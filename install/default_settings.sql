INSERT INTO `articles` (`id`, `title`, `slug`, `content`, `tags`, `category_id`, `active`, `date_created`, `user_id`) VALUES
(1, 'Many People Selling Online 2', 'many-people-selling-online-2', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus eligendi nobis ea maiores sapiente veritatis reprehenderit suscipit quaerat rerum voluptatibus a eius.', NULL, NULL, 1, '2019-09-02 04:20:26', 0),
(2, 'Many People Selling Online 2', 'many-people-selling-online-2', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus eligendi nobis ea maiores sapiente veritatis reprehenderit suscipit quaerat rerum voluptatibus a eius.', 'tag1, tag2, tag3', NULL, 1, '2019-09-02 04:20:26', 0),
(3, 'Many People Selling Online 3', 'many-people-selling-online', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus eligendi nobis ea maiores sapiente veritatis reprehenderit suscipit quaerat rerum voluptatibus a eius.\r\n\r\nLorem ipsum dolor sit amet consectetur adipisicing elit. Natus eligendi nobis ea maiores sapiente veritatis reprehenderit suscipit quaerat rerum voluptatibus a eius.\r\n\r\nLorem ipsum dolor sit amet consectetur adipisicing elit. Natus eligendi nobis ea maiores sapiente veritatis reprehenderit suscipit quaerat rerum voluptatibus a eius.', 'tag1, tag2, tag3', 1, 1, '2019-09-02 04:20:26', 0);

INSERT INTO `bookmarks` (`id`, `listing_id`, `label`, `type`, `user_id`) VALUES
(12, 4, NULL, 'listing', 3),
(11, 13, NULL, 'listing', 3),
(10, 12, NULL, 'listing', 3),
(9, 11, NULL, 'listing', 3);

INSERT INTO `categories` (`id`, `name`, `description`, `parent_id`, `icon`, `slug`, `active`, `total`) VALUES
(1, 'Cars & Vehicles', 'Cars & Vehicles', 0, 'flaticon-car', 'cars-vehicles', 1, 1),
(2, 'Furnitures', 'Furnitures', 0, 'flaticon-closet', 'furnitures', 1, 1),
(3, 'Real Estate', 'Real Estate', 0, 'flaticon-home', 'real-estate', 1, 0),
(4, 'Books & Magazines', 'Books & Magazines', 0, 'flaticon-open-book', 'books-magazines', 1, 3),
(5, 'Electronics', 'Electronics', 0, 'flaticon-tv', 'electronics', 1, 1),
(6, 'Others', 'Others', 0, 'flaticon-pizza', 'others', 1, 1);

INSERT INTO `listings` (`id`, `title`, `slug`, `address`, `category_id`, `location_id`, `description`, `rating`, `user_id`, `active`, `featured`, `date_created`) VALUES
(1, 'Red Luxury Car', 'red-luxury-car', 'Don St, Brooklyn, New York', 4, 1, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error repellat architecto maiores vero, quasi dolor, accusantium autem aliquam, ullam sequi saepe illum eaque aperiam eius amet! Necessitatibus nam sapiente obcaecati sit, fugit omnis non sunt distinctio aliquid, quibusdam excepturi hic?\r\n\r\nNisi, error. Molestias, quidem eaque sequi aut perspiciatis assumenda obcaecati ut quod eius reprehenderit. Iure rem numquam totam odio dignissimos aspernatur soluta. Corporis suscipit modi iste consequatur, repellat nihil omnis molestias optio. Dolorem ullam eius officia, eum ratione dolorum assumenda.\r\n\r\nSoluta corporis blanditiis cupiditate debitis eveniet, temporibus ut cumque sint repudiandae quidem tenetur commodi id, quam. Sapiente corrupti magnam quis nulla, asperiores neque tenetur labore aperiam provident nostrum sequi delectus voluptatem fuga officiis repellat, ratione aspernatur eius repellendus modi reprehenderit.\r\n\r\nSapiente molestias voluptate cupiditate blanditiis quasi qui aperiam accusamus aspernatur ipsam velit nihil quaerat voluptatum soluta laboriosam ipsum veritatis at reiciendis quod voluptates, saepe harum dignissimos placeat dolorum aliquid! Quod quasi praesentium optio ratione non et sit quos excepturi cum?', 0, 0, 1, 0, '2019-09-02 09:39:18'),
(2, 'Red Luxury Car', 'red-luxury-car-b4d11cdd68', 'Brgy. Sto. Nino, Davao CIty', 6, 1, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error repellat architecto maiores vero, quasi dolor, accusantium autem aliquam, ullam sequi saepe illum eaque aperiam eius amet! Necessitatibus nam sapiente obcaecati sit, fugit omnis non sunt distinctio aliquid, quibusdam excepturi hic?\r\n\r\nNisi, error. Molestias, quidem eaque sequi aut perspiciatis assumenda obcaecati ut quod eius reprehenderit. Iure rem numquam totam odio dignissimos aspernatur soluta. Corporis suscipit modi iste consequatur, repellat nihil omnis molestias optio. Dolorem ullam eius officia, eum ratione dolorum assumenda.\r\n\r\nSoluta corporis blanditiis cupiditate debitis eveniet, temporibus ut cumque sint repudiandae quidem tenetur commodi id, quam. Sapiente corrupti magnam quis nulla, asperiores neque tenetur labore aperiam provident nostrum sequi delectus voluptatem fuga officiis repellat, ratione aspernatur eius repellendus modi reprehenderit.\r\n\r\nSapiente molestias voluptate cupiditate blanditiis quasi qui aperiam accusamus aspernatur ipsam velit nihil quaerat voluptatum soluta laboriosam ipsum veritatis at reiciendis quod voluptates, saepe harum dignissimos placeat dolorum aliquid! Quod quasi praesentium optio ratione non et sit quos excepturi cum?', 0, 1, 1, 0, '2019-09-02 09:39:18'),
(3, 'Red Luxury Car', 'red-luxury-car-b3922a3b4c', 'Brgy. Sto. Nino, Davao CIty', 3, 1, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error repellat architecto maiores vero, quasi dolor, accusantium autem aliquam, ullam sequi saepe illum eaque aperiam eius amet! Necessitatibus nam sapiente obcaecati sit, fugit omnis non sunt distinctio aliquid, quibusdam excepturi hic?\r\n\r\nNisi, error. Molestias, quidem eaque sequi aut perspiciatis assumenda obcaecati ut quod eius reprehenderit. Iure rem numquam totam odio dignissimos aspernatur soluta. Corporis suscipit modi iste consequatur, repellat nihil omnis molestias optio. Dolorem ullam eius officia, eum ratione dolorum assumenda.\r\n\r\nSoluta corporis blanditiis cupiditate debitis eveniet, temporibus ut cumque sint repudiandae quidem tenetur commodi id, quam. Sapiente corrupti magnam quis nulla, asperiores neque tenetur labore aperiam provident nostrum sequi delectus voluptatem fuga officiis repellat, ratione aspernatur eius repellendus modi reprehenderit.\r\n\r\nSapiente molestias voluptate cupiditate blanditiis quasi qui aperiam accusamus aspernatur ipsam velit nihil quaerat voluptatum soluta laboriosam ipsum veritatis at reiciendis quod voluptates, saepe harum dignissimos placeat dolorum aliquid! Quod quasi praesentium optio ratione non et sit quos excepturi cum?', 0, 1, 1, 0, '2019-09-02 09:39:18'),
(4, 'Red Luxury Car', 'red-luxury-car-9fe604a6ef', 'Brgy. Sto. Nino, Davao CIty', 1, 1, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error repellat architecto maiores vero, quasi dolor, accusantium autem aliquam, ullam sequi saepe illum eaque aperiam eius amet! Necessitatibus nam sapiente obcaecati sit, fugit omnis non sunt distinctio aliquid, quibusdam excepturi hic?\r\n\r\nNisi, error. Molestias, quidem eaque sequi aut perspiciatis assumenda obcaecati ut quod eius reprehenderit. Iure rem numquam totam odio dignissimos aspernatur soluta. Corporis suscipit modi iste consequatur, repellat nihil omnis molestias optio. Dolorem ullam eius officia, eum ratione dolorum assumenda.\r\n\r\nSoluta corporis blanditiis cupiditate debitis eveniet, temporibus ut cumque sint repudiandae quidem tenetur commodi id, quam. Sapiente corrupti magnam quis nulla, asperiores neque tenetur labore aperiam provident nostrum sequi delectus voluptatem fuga officiis repellat, ratione aspernatur eius repellendus modi reprehenderit.\r\n\r\nSapiente molestias voluptate cupiditate blanditiis quasi qui aperiam accusamus aspernatur ipsam velit nihil quaerat voluptatum soluta laboriosam ipsum veritatis at reiciendis quod voluptates, saepe harum dignissimos placeat dolorum aliquid! Quod quasi praesentium optio ratione non et sit quos excepturi cum?', 0, 1, 1, 0, '2019-09-02 09:39:18'),
(5, 'Red Luxury Car', 'red-luxury-car-073298a121', 'Brgy. Sto. Nino, Davao CIty', 2, 2, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error repellat architecto maiores vero, quasi dolor, accusantium autem aliquam, ullam sequi saepe illum eaque aperiam eius amet! Necessitatibus nam sapiente obcaecati sit, fugit omnis non sunt distinctio aliquid, quibusdam excepturi hic?\r\n\r\nNisi, error. Molestias, quidem eaque sequi aut perspiciatis assumenda obcaecati ut quod eius reprehenderit. Iure rem numquam totam odio dignissimos aspernatur soluta. Corporis suscipit modi iste consequatur, repellat nihil omnis molestias optio. Dolorem ullam eius officia, eum ratione dolorum assumenda.\r\n\r\nSoluta corporis blanditiis cupiditate debitis eveniet, temporibus ut cumque sint repudiandae quidem tenetur commodi id, quam. Sapiente corrupti magnam quis nulla, asperiores neque tenetur labore aperiam provident nostrum sequi delectus voluptatem fuga officiis repellat, ratione aspernatur eius repellendus modi reprehenderit.\r\n\r\nSapiente molestias voluptate cupiditate blanditiis quasi qui aperiam accusamus aspernatur ipsam velit nihil quaerat voluptatum soluta laboriosam ipsum veritatis at reiciendis quod voluptates, saepe harum dignissimos placeat dolorum aliquid! Quod quasi praesentium optio ratione non et sit quos excepturi cum?', 0, 1, 1, 0, '2019-09-02 09:39:18'),
(6, 'Own New House', 'own-new-house-d942ee4f17', 'Don St, Brooklyn, New York', 4, 1, '<a href=\"test\">Lorem</a> ipsum dolor sit amet, consectetur adipisicing elit. Error repellat architecto maiores vero, quasi dolor, accusantium autem aliquam, ullam sequi saepe illum eaque aperiam eius amet! Necessitatibus nam sapiente obcaecati sit, fugit omnis non sunt distinctio aliquid, quibusdam excepturi hic?\r\n\r\nNisi, error. Molestias, quidem eaque sequi aut perspiciatis assumenda obcaecati ut quod eius reprehenderit. Iure rem numquam totam odio dignissimos aspernatur soluta. Corporis suscipit modi iste consequatur, repellat nihil omnis molestias optio. Dolorem ullam eius officia, eum ratione dolorum assumenda.\r\n\r\nSoluta corporis blanditiis cupiditate debitis eveniet, temporibus ut cumque sint repudiandae quidem tenetur commodi id, quam. Sapiente corrupti magnam quis nulla, asperiores neque tenetur labore aperiam provident nostrum sequi delectus voluptatem fuga officiis repellat, ratione aspernatur eius repellendus modi reprehenderit.\r\n\r\nSapiente molestias voluptate cupiditate blanditiis quasi qui aperiam accusamus aspernatur ipsam velit nihil quaerat voluptatum soluta laboriosam ipsum veritatis at reiciendis quod voluptates, saepe harum dignissimos placeat dolorum aliquid! Quod quasi praesentium optio ratione non et sit quos excepturi cum?', 0, 1, 1, 1, '2019-09-02 09:39:18'),
(7, 'Many People Selling Online', 'many-people-selling-online-3478f8ac2b', 'Don St, Brooklyn, New York', 4, 1, 'test', 0, 1, 1, 0, '2019-09-02 09:39:18'),
(8, 'Many People Selling Online 2', 'many-people-selling-online-2-01fd665fc7', 'Lorenzville Homes, Ulas, Brgy. Talomo, Davao City', 4, 1, 'test 2', 0, 1, 1, 1, '2019-09-02 09:39:54'),
(9, 'Red Luxury Car', 'red-luxury-car-03da2cfefe', 'Brgy. Sto. Nino, Davao CIty', 4, 1, 'test', 0, 1, 1, 0, '2019-09-02 09:49:57'),
(10, 'Many People Selling Online 3', 'many-people-selling-online-3-d3ea7e8296', 'Lorenzville Homes, Ulas, Brgy. Talomo, Davao City', 4, 1, 'asdf asdf asdf asdf asdf asdf', 0, 1, 1, 0, '2019-09-02 09:58:25'),
(11, 'Red Luxury Car 2', 'red-luxury-car-2-d428b2b9e3', 'Brgy. Sto. Nino, Davao CIty 2', 4, 1, 'test te se asdf asdf asdf asdf', 0, 1, 1, 0, '2019-09-02 10:07:41'),
(12, 'Red Luxury Car 2', 'red-luxury-car-2-841198c4a7', 'Brgy. Sto. Nino, Davao CIty 2', 4, 2, 'test te se asdf asdf asdf asdf test', 0, 1, 1, 0, '2019-09-02 10:07:54'),
(13, 'Announcements 2', 'announcements-2-e91b2501d1', 'Don St, Brooklyn, New York 2', 1, 2, 'asdf asdf asdf', 0, 1, 1, 1, '2019-09-02 10:09:44');

INSERT INTO `settings` (`id`, `module`, `skey`, `value`) VALUES
(4, 'site_settings', 'site_description', 'Directory Ads Directory System'),
(3, 'site_settings', 'site_name', 'Directory Ads 3'),
(5, 'site_settings', 'site_keywords', 'DirectoryAds, Directory, System, Alchie'),
(6, 'homepage', 'heading_title', 'Welcome To Alchie'),
(7, 'homepage', 'heading_description', 'heading_description'),
(27, 'homepage', 'newsletter_title', 'Newsletter '),
(11, 'homepage', 'heading_trending', 'iPhone,Cars,Flowers,House'),
(30, 'homepage', 'blog_title', 'Our Blog 2'),
(18, 'homepage', 'featured_ads_title', 'Featured Ads Title'),
(19, 'homepage', 'categories_title', 'Popular Categories 2'),
(20, 'homepage', 'categories_description', 'Popular Categories 3'),
(35, 'homepage', 'heading_search', '1'),
(23, 'homepage', 'trending_today_title', 'Trending Today 1'),
(28, 'homepage', 'newsletter_description', 'Newsletter 2'),
(31, 'homepage', 'blog_description', 'See Our Daily News & Updates 2'),
(32, 'homepage', 'testimonials_show', '1'),
(33, 'homepage', 'testimonials_title', 'testi'),
(34, 'homepage', 'testimonials_description', 'test 2'),
(36, 'homepage', 'heading_trending_show', '1'),
(37, 'homepage', 'featured_ads_show', '1'),
(38, 'homepage', 'categories_show', '1'),
(39, 'homepage', 'trending_today_show', '1'),
(40, 'homepage', 'blog_show', '1'),
(41, 'homepage', 'newsletter_show', '1'),
(42, 'listings', 'listings_title', 'Ads Listings 2'),
(43, 'listings', 'listings_description', 'Choose product you want 2'),
(44, 'listings', 'testimonials_show', '1'),
(45, 'listings', 'trending_today_show', '1'),
(46, 'listings', 'trending_today_title', 'test'),
(47, 'listings', 'testimonials_title', 'test'),
(48, 'listings', 'testimonials_description', 'test');

INSERT INTO `testimonials` (`id`, `name`, `message`, `pic`, `active`) VALUES
(1, 'Christian Aguilar', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur unde reprehenderit aperiam quaerat fugiat repudiandae explicabo animi minima fuga beatae illum eligendi incidunt consequatur. Amet dolores excepturi earum unde iusto.', NULL, 1),
(2, 'Christian Aguilar', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur unde reprehenderit aperiam quaerat fugiat repudiandae explicabo animi minima fuga beatae illum eligendi incidunt consequatur. Amet dolores excepturi earum unde iusto.', NULL, 1),
(3, 'Christian Aguilar 2', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur unde reprehenderit aperiam quaerat fugiat repudiandae explicabo animi minima fuga beatae illum eligendi incidunt consequatur. Amet dolores excepturi earum unde iusto.', NULL, 1);

INSERT INTO `users` (`id`, `display_name`, `email`, `password`, `username`, `is_admin`, `admin_password`, `last_login`, `date_registered`, `description`) VALUES
(1, 'Admin', 'admin@admin.com', 'f865b53623b121fd34ee5426c792e5c33af8c227', 'admin', 1, NULL, '2019-09-10 04:48:54', '2019-08-31 15:24:30', 'Admin');

