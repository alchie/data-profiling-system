CREATE TABLE `account_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  KEY `account_sessions_timestamp` (`timestamp`)
);

-- Table structure for table `account_sessions` 

CREATE TABLE `activities_attendance` (
  `activity_id` int(20) NOT NULL,
  `name_id` int(20) NOT NULL
);

-- Table structure for table `activities_attendance` 

CREATE TABLE `activities_list` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `notes` text,
  `benefeciaries` int(1) NOT NULL DEFAULT '0',
  `parents` int(1) NOT NULL DEFAULT '0',
  `active` int(1) NOT NULL DEFAULT '1',
  `trash` int(1) DEFAULT '0',
  `service_id` int(20) DEFAULT NULL,
  `center_id` int(20) DEFAULT NULL,
  `activity_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
);

-- Table structure for table `activities_list` 

CREATE TABLE `benefeciaries` (
  `name_id` int(20) NOT NULL,
  `center_id` int(20) NOT NULL,
  `group_id` int(20) DEFAULT NULL,
  `area_id` int(20) DEFAULT NULL,
  `graduated` date DEFAULT NULL,
  `status` int(20) DEFAULT NULL,
  `notes` text,
  `trash` int(1) NOT NULL DEFAULT '0',
  `benefeciary_id` varchar(20) DEFAULT NULL,
  `accepted` date DEFAULT NULL,
  PRIMARY KEY (`name_id`),
  KEY `name_id` (`name_id`),
  KEY `group_id` (`group_id`),
  KEY `area_id` (`area_id`),
  KEY `center_id` (`center_id`)
);

-- Table structure for table `benefeciaries` 

CREATE TABLE `benefeciaries_areas` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `center_id` int(20) NOT NULL,
  `trash` int(1) NOT NULL DEFAULT '0',
  `notes` text,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `center_id` (`center_id`)
);

-- Table structure for table `benefeciaries_areas` 

CREATE TABLE `benefeciaries_groups` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `center_id` int(20) NOT NULL,
  `name` varchar(200) NOT NULL,
  `notes` text,
  `trash` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `center_id` (`center_id`)
);

-- Table structure for table `benefeciaries_groups` 

CREATE TABLE `benefeciaries_hw` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name_id` int(20) NOT NULL,
  `date_hw` date NOT NULL,
  `height` decimal(10,5) NOT NULL,
  `weight` decimal(10,5) NOT NULL,
  `notes` text,
  PRIMARY KEY (`id`)
);

-- Table structure for table `benefeciaries_hw` 

CREATE TABLE `benefeciaries_services` (
  `name_id` int(20) NOT NULL,
  `service_id` int(20) NOT NULL
);

-- Table structure for table `benefeciaries_services` 

CREATE TABLE `centers_list` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `theme` varchar(50) DEFAULT NULL,
  `name` varchar(200) NOT NULL,
  `address` varchar(200) DEFAULT NULL,
  `phone` varchar(200) DEFAULT NULL,
  `notes` text,
  `default` int(1) DEFAULT '0',
  `trash` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
);

-- Table structure for table `centers_list` 

CREATE TABLE `names_info` (
  `name_id` int(20) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `middlename` varchar(100) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `birthplace` varchar(50) DEFAULT NULL,
  `gender` varchar(50) DEFAULT NULL,
  `civil_status` varchar(50) DEFAULT NULL,
  `prefix` varchar(50) DEFAULT NULL,
  `suffix` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`name_id`),
  KEY `name_id` (`name_id`)
);

-- Table structure for table `names_info` 

CREATE TABLE `names_list` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(200) NOT NULL,
  `address` varchar(200) DEFAULT NULL,
  `contact_number` varchar(200) DEFAULT NULL,
  `trash` int(1) NOT NULL DEFAULT '0',
  `type_benefeciary` int(1) NOT NULL DEFAULT '0',
  `type_parent` int(1) NOT NULL DEFAULT '0',
  `type_sponsor` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `full_name` (`full_name`)
);

-- Table structure for table `names_list` 

CREATE TABLE `names_meta` (
  `meta_id` int(20) NOT NULL AUTO_INCREMENT,
  `name_id` int(20) NOT NULL,
  `meta_key` varchar(200) NOT NULL,
  `meta_value` text,
  PRIMARY KEY (`meta_id`),
  KEY `name_id` (`name_id`)
);

-- Table structure for table `names_meta` 

CREATE TABLE `parents` (
  `name_id` int(20) NOT NULL,
  `center_id` int(20) NOT NULL,
  `group_id` int(20) DEFAULT NULL,
  `area_id` int(20) DEFAULT NULL,
  `status` int(20) DEFAULT NULL,
  `notes` text,
  `trash` int(1) NOT NULL DEFAULT '0',
  `parent_id` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`name_id`),
  KEY `name_id` (`name_id`),
  KEY `group_id` (`group_id`),
  KEY `area_id` (`area_id`),
  KEY `center_id` (`center_id`)
);

-- Table structure for table `parents` 

CREATE TABLE `parents_areas` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `center_id` int(20) NOT NULL,
  `trash` int(1) NOT NULL DEFAULT '0',
  `notes` text,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `center_id` (`center_id`)
);

-- Table structure for table `parents_areas` 

CREATE TABLE `parents_groups` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `center_id` int(20) NOT NULL,
  `name` varchar(200) NOT NULL,
  `notes` text,
  `trash` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `center_id` (`center_id`)
);

-- Table structure for table `parents_groups` 

CREATE TABLE `services_list` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `notes` text,
  `benefeciaries` int(1) NOT NULL DEFAULT '0',
  `parents` int(1) NOT NULL DEFAULT '0',
  `active` int(1) NOT NULL DEFAULT '1',
  `trash` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
);

-- Table structure for table `services_list` 

CREATE TABLE `system_audit` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(20) NOT NULL,
  `dept` varchar(200) NOT NULL,
  `sect` varchar(200) NOT NULL,
  `action` varchar(200) NOT NULL,
  `company_id` int(20) DEFAULT NULL,
  `name_id` int(20) DEFAULT '0',
  `notes` text,
  `date_accessed` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
);

-- Table structure for table `system_audit` 

CREATE TABLE `terms_list` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `notes` text,
  `type` varchar(50) DEFAULT NULL,
  `trash` int(1) DEFAULT '0',
  `priority` int(3) DEFAULT '0',
  PRIMARY KEY (`id`)
);

-- Table structure for table `terms_list` 

CREATE TABLE `user_accounts` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  `last_login` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `id` (`id`)
);

-- Table structure for table `user_accounts` 

CREATE TABLE `user_accounts_centers` (
  `uid` int(20) NOT NULL,
  `center_id` int(20) NOT NULL,
  KEY `uid` (`uid`,`center_id`)
);

-- Table structure for table `user_accounts_centers` 

CREATE TABLE `user_accounts_companies` (
  `uid` int(20) NOT NULL,
  `company_id` int(20) NOT NULL,
  KEY `uid` (`uid`,`company_id`)
);

-- Table structure for table `user_accounts_companies` 

CREATE TABLE `user_accounts_options` (
  `uid` int(20) NOT NULL,
  `department` varchar(200) NOT NULL,
  `section` varchar(200) NOT NULL,
  `key` varchar(200) NOT NULL,
  `value` text NOT NULL,
  KEY `uid` (`uid`)
);

-- Table structure for table `user_accounts_options` 

CREATE TABLE `user_accounts_restrictions` (
  `uid` int(20) NOT NULL,
  `department` varchar(50) NOT NULL,
  `section` varchar(50) NOT NULL,
  `view` int(1) NOT NULL DEFAULT '0',
  `add` int(1) NOT NULL DEFAULT '0',
  `edit` int(1) NOT NULL DEFAULT '0',
  `delete` int(1) NOT NULL DEFAULT '0',
  KEY `uid` (`uid`)
);

-- Table structure for table `user_accounts_restrictions` 

