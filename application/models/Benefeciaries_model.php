<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Benefeciaries_model Class
 *
 * Manipulates `benefeciaries` table on database

CREATE TABLE `benefeciaries` (
  `name_id` int(20) NOT NULL,
  `center_id` int(20) NOT NULL,
  `group_id` int(20) DEFAULT NULL,
  `area_id` int(20) DEFAULT NULL,
  `graduated` date DEFAULT NULL,
  `status` int(20) DEFAULT NULL,
  `notes` text,
  `trash` int(1) NOT NULL DEFAULT '0',
  `benefeciary_id` varchar(20) DEFAULT NULL,
  `accepted` date DEFAULT NULL,
  PRIMARY KEY (`name_id`),
  KEY `name_id` (`name_id`),
  KEY `group_id` (`group_id`),
  KEY `area_id` (`area_id`),
  KEY `center_id` (`center_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `benefeciaries` ADD  `name_id` int(20) NOT NULL   PRIMARY KEY;
ALTER TABLE  `benefeciaries` ADD  `center_id` int(20) NOT NULL   ;
ALTER TABLE  `benefeciaries` ADD  `group_id` int(20) NULL   ;
ALTER TABLE  `benefeciaries` ADD  `area_id` int(20) NULL   ;
ALTER TABLE  `benefeciaries` ADD  `graduated` date NULL   ;
ALTER TABLE  `benefeciaries` ADD  `status` int(20) NULL   ;
ALTER TABLE  `benefeciaries` ADD  `notes` text NULL   ;
ALTER TABLE  `benefeciaries` ADD  `trash` int(1) NOT NULL   DEFAULT '0';
ALTER TABLE  `benefeciaries` ADD  `benefeciary_id` varchar(20) NULL   ;
ALTER TABLE  `benefeciaries` ADD  `accepted` date NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Benefeciaries_model extends MY_Model {

	protected $name_id;
	protected $center_id;
	protected $group_id;
	protected $area_id;
	protected $graduated;
	protected $status;
	protected $notes;
	protected $trash;
	protected $benefeciary_id;
	protected $accepted;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'benefeciaries';
		$this->_short_name = 'benefeciaries';
		$this->_fields = array("name_id","center_id","group_id","area_id","graduated","status","notes","trash","benefeciary_id","accepted");
		$this->_required = array("center_id","trash");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: name_id -------------------------------------- 

	/** 
	* Sets a value to `name_id` variable
	* @access public
	*/

	public function setNameId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('name_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_name_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('name_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `name_id` variable
	* @access public
	*/

	public function getNameId() {
		return $this->name_id;
	}

	public function get_name_id() {
		return $this->name_id;
	}

	
// ------------------------------ End Field: name_id --------------------------------------


// ---------------------------- Start Field: center_id -------------------------------------- 

	/** 
	* Sets a value to `center_id` variable
	* @access public
	*/

	public function setCenterId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('center_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_center_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('center_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `center_id` variable
	* @access public
	*/

	public function getCenterId() {
		return $this->center_id;
	}

	public function get_center_id() {
		return $this->center_id;
	}

	
// ------------------------------ End Field: center_id --------------------------------------


// ---------------------------- Start Field: group_id -------------------------------------- 

	/** 
	* Sets a value to `group_id` variable
	* @access public
	*/

	public function setGroupId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('group_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_group_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('group_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `group_id` variable
	* @access public
	*/

	public function getGroupId() {
		return $this->group_id;
	}

	public function get_group_id() {
		return $this->group_id;
	}

	
// ------------------------------ End Field: group_id --------------------------------------


// ---------------------------- Start Field: area_id -------------------------------------- 

	/** 
	* Sets a value to `area_id` variable
	* @access public
	*/

	public function setAreaId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('area_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_area_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('area_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `area_id` variable
	* @access public
	*/

	public function getAreaId() {
		return $this->area_id;
	}

	public function get_area_id() {
		return $this->area_id;
	}

	
// ------------------------------ End Field: area_id --------------------------------------


// ---------------------------- Start Field: graduated -------------------------------------- 

	/** 
	* Sets a value to `graduated` variable
	* @access public
	*/

	public function setGraduated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('graduated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_graduated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('graduated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `graduated` variable
	* @access public
	*/

	public function getGraduated() {
		return $this->graduated;
	}

	public function get_graduated() {
		return $this->graduated;
	}

	
// ------------------------------ End Field: graduated --------------------------------------


// ---------------------------- Start Field: status -------------------------------------- 

	/** 
	* Sets a value to `status` variable
	* @access public
	*/

	public function setStatus($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('status', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_status($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('status', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `status` variable
	* @access public
	*/

	public function getStatus() {
		return $this->status;
	}

	public function get_status() {
		return $this->status;
	}

	
// ------------------------------ End Field: status --------------------------------------


// ---------------------------- Start Field: notes -------------------------------------- 

	/** 
	* Sets a value to `notes` variable
	* @access public
	*/

	public function setNotes($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('notes', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_notes($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('notes', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `notes` variable
	* @access public
	*/

	public function getNotes() {
		return $this->notes;
	}

	public function get_notes() {
		return $this->notes;
	}

	
// ------------------------------ End Field: notes --------------------------------------


// ---------------------------- Start Field: trash -------------------------------------- 

	/** 
	* Sets a value to `trash` variable
	* @access public
	*/

	public function setTrash($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('trash', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_trash($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('trash', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `trash` variable
	* @access public
	*/

	public function getTrash() {
		return $this->trash;
	}

	public function get_trash() {
		return $this->trash;
	}

	
// ------------------------------ End Field: trash --------------------------------------


// ---------------------------- Start Field: benefeciary_id -------------------------------------- 

	/** 
	* Sets a value to `benefeciary_id` variable
	* @access public
	*/

	public function setBenefeciaryId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('benefeciary_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_benefeciary_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('benefeciary_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `benefeciary_id` variable
	* @access public
	*/

	public function getBenefeciaryId() {
		return $this->benefeciary_id;
	}

	public function get_benefeciary_id() {
		return $this->benefeciary_id;
	}

	
// ------------------------------ End Field: benefeciary_id --------------------------------------


// ---------------------------- Start Field: accepted -------------------------------------- 

	/** 
	* Sets a value to `accepted` variable
	* @access public
	*/

	public function setAccepted($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('accepted', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_accepted($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('accepted', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `accepted` variable
	* @access public
	*/

	public function getAccepted() {
		return $this->accepted;
	}

	public function get_accepted() {
		return $this->accepted;
	}

	
// ------------------------------ End Field: accepted --------------------------------------



	
	public function get_table_options() {
		return array(
			'name_id' => (object) array(
										'Field'=>'name_id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>''
									),

			'center_id' => (object) array(
										'Field'=>'center_id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'group_id' => (object) array(
										'Field'=>'group_id',
										'Type'=>'int(20)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'area_id' => (object) array(
										'Field'=>'area_id',
										'Type'=>'int(20)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'graduated' => (object) array(
										'Field'=>'graduated',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'status' => (object) array(
										'Field'=>'status',
										'Type'=>'int(20)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'notes' => (object) array(
										'Field'=>'notes',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'trash' => (object) array(
										'Field'=>'trash',
										'Type'=>'int(1)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'benefeciary_id' => (object) array(
										'Field'=>'benefeciary_id',
										'Type'=>'varchar(20)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'accepted' => (object) array(
										'Field'=>'accepted',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'name_id' => "ALTER TABLE  `benefeciaries` ADD  `name_id` int(20) NOT NULL   PRIMARY KEY;",
			'center_id' => "ALTER TABLE  `benefeciaries` ADD  `center_id` int(20) NOT NULL   ;",
			'group_id' => "ALTER TABLE  `benefeciaries` ADD  `group_id` int(20) NULL   ;",
			'area_id' => "ALTER TABLE  `benefeciaries` ADD  `area_id` int(20) NULL   ;",
			'graduated' => "ALTER TABLE  `benefeciaries` ADD  `graduated` date NULL   ;",
			'status' => "ALTER TABLE  `benefeciaries` ADD  `status` int(20) NULL   ;",
			'notes' => "ALTER TABLE  `benefeciaries` ADD  `notes` text NULL   ;",
			'trash' => "ALTER TABLE  `benefeciaries` ADD  `trash` int(1) NOT NULL   DEFAULT '0';",
			'benefeciary_id' => "ALTER TABLE  `benefeciaries` ADD  `benefeciary_id` varchar(20) NULL   ;",
			'accepted' => "ALTER TABLE  `benefeciaries` ADD  `accepted` date NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setNameId() - name_id
//setCenterId() - center_id
//setGroupId() - group_id
//setAreaId() - area_id
//setGraduated() - graduated
//setStatus() - status
//setNotes() - notes
//setTrash() - trash
//setBenefeciaryId() - benefeciary_id
//setAccepted() - accepted

--------------------------------------

//set_name_id() - name_id
//set_center_id() - center_id
//set_group_id() - group_id
//set_area_id() - area_id
//set_graduated() - graduated
//set_status() - status
//set_notes() - notes
//set_trash() - trash
//set_benefeciary_id() - benefeciary_id
//set_accepted() - accepted

*/
/* End of file Benefeciaries_model.php */
/* Location: ./application/models/Benefeciaries_model.php */
