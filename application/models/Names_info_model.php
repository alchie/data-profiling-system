<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Names_info_model Class
 *
 * Manipulates `names_info` table on database

CREATE TABLE `names_info` (
  `name_id` int(20) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `middlename` varchar(100) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `birthplace` varchar(50) DEFAULT NULL,
  `gender` varchar(50) DEFAULT NULL,
  `civil_status` varchar(50) DEFAULT NULL,
  `prefix` varchar(50) DEFAULT NULL,
  `suffix` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`name_id`),
  KEY `name_id` (`name_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `names_info` ADD  `name_id` int(20) NOT NULL   PRIMARY KEY;
ALTER TABLE  `names_info` ADD  `lastname` varchar(100) NOT NULL   ;
ALTER TABLE  `names_info` ADD  `firstname` varchar(100) NOT NULL   ;
ALTER TABLE  `names_info` ADD  `middlename` varchar(100) NULL   ;
ALTER TABLE  `names_info` ADD  `birthday` date NULL   ;
ALTER TABLE  `names_info` ADD  `birthplace` varchar(50) NULL   ;
ALTER TABLE  `names_info` ADD  `gender` varchar(50) NULL   ;
ALTER TABLE  `names_info` ADD  `civil_status` varchar(50) NULL   ;
ALTER TABLE  `names_info` ADD  `prefix` varchar(50) NULL   ;
ALTER TABLE  `names_info` ADD  `suffix` varchar(50) NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Names_info_model extends MY_Model {

	protected $name_id;
	protected $lastname;
	protected $firstname;
	protected $middlename;
	protected $birthday;
	protected $birthplace;
	protected $gender;
	protected $civil_status;
	protected $prefix;
	protected $suffix;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'names_info';
		$this->_short_name = 'names_info';
		$this->_fields = array("name_id","lastname","firstname","middlename","birthday","birthplace","gender","civil_status","prefix","suffix");
		$this->_required = array("lastname","firstname");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: name_id -------------------------------------- 

	/** 
	* Sets a value to `name_id` variable
	* @access public
	*/

	public function setNameId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('name_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_name_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('name_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `name_id` variable
	* @access public
	*/

	public function getNameId() {
		return $this->name_id;
	}

	public function get_name_id() {
		return $this->name_id;
	}

	
// ------------------------------ End Field: name_id --------------------------------------


// ---------------------------- Start Field: lastname -------------------------------------- 

	/** 
	* Sets a value to `lastname` variable
	* @access public
	*/

	public function setLastname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('lastname', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_lastname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('lastname', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `lastname` variable
	* @access public
	*/

	public function getLastname() {
		return $this->lastname;
	}

	public function get_lastname() {
		return $this->lastname;
	}

	
// ------------------------------ End Field: lastname --------------------------------------


// ---------------------------- Start Field: firstname -------------------------------------- 

	/** 
	* Sets a value to `firstname` variable
	* @access public
	*/

	public function setFirstname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('firstname', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_firstname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('firstname', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `firstname` variable
	* @access public
	*/

	public function getFirstname() {
		return $this->firstname;
	}

	public function get_firstname() {
		return $this->firstname;
	}

	
// ------------------------------ End Field: firstname --------------------------------------


// ---------------------------- Start Field: middlename -------------------------------------- 

	/** 
	* Sets a value to `middlename` variable
	* @access public
	*/

	public function setMiddlename($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('middlename', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_middlename($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('middlename', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `middlename` variable
	* @access public
	*/

	public function getMiddlename() {
		return $this->middlename;
	}

	public function get_middlename() {
		return $this->middlename;
	}

	
// ------------------------------ End Field: middlename --------------------------------------


// ---------------------------- Start Field: birthday -------------------------------------- 

	/** 
	* Sets a value to `birthday` variable
	* @access public
	*/

	public function setBirthday($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('birthday', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_birthday($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('birthday', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `birthday` variable
	* @access public
	*/

	public function getBirthday() {
		return $this->birthday;
	}

	public function get_birthday() {
		return $this->birthday;
	}

	
// ------------------------------ End Field: birthday --------------------------------------


// ---------------------------- Start Field: birthplace -------------------------------------- 

	/** 
	* Sets a value to `birthplace` variable
	* @access public
	*/

	public function setBirthplace($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('birthplace', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_birthplace($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('birthplace', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `birthplace` variable
	* @access public
	*/

	public function getBirthplace() {
		return $this->birthplace;
	}

	public function get_birthplace() {
		return $this->birthplace;
	}

	
// ------------------------------ End Field: birthplace --------------------------------------


// ---------------------------- Start Field: gender -------------------------------------- 

	/** 
	* Sets a value to `gender` variable
	* @access public
	*/

	public function setGender($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('gender', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_gender($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('gender', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `gender` variable
	* @access public
	*/

	public function getGender() {
		return $this->gender;
	}

	public function get_gender() {
		return $this->gender;
	}

	
// ------------------------------ End Field: gender --------------------------------------


// ---------------------------- Start Field: civil_status -------------------------------------- 

	/** 
	* Sets a value to `civil_status` variable
	* @access public
	*/

	public function setCivilStatus($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('civil_status', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_civil_status($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('civil_status', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `civil_status` variable
	* @access public
	*/

	public function getCivilStatus() {
		return $this->civil_status;
	}

	public function get_civil_status() {
		return $this->civil_status;
	}

	
// ------------------------------ End Field: civil_status --------------------------------------


// ---------------------------- Start Field: prefix -------------------------------------- 

	/** 
	* Sets a value to `prefix` variable
	* @access public
	*/

	public function setPrefix($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('prefix', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_prefix($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('prefix', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `prefix` variable
	* @access public
	*/

	public function getPrefix() {
		return $this->prefix;
	}

	public function get_prefix() {
		return $this->prefix;
	}

	
// ------------------------------ End Field: prefix --------------------------------------


// ---------------------------- Start Field: suffix -------------------------------------- 

	/** 
	* Sets a value to `suffix` variable
	* @access public
	*/

	public function setSuffix($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('suffix', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_suffix($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('suffix', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `suffix` variable
	* @access public
	*/

	public function getSuffix() {
		return $this->suffix;
	}

	public function get_suffix() {
		return $this->suffix;
	}

	
// ------------------------------ End Field: suffix --------------------------------------



	
	public function get_table_options() {
		return array(
			'name_id' => (object) array(
										'Field'=>'name_id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>''
									),

			'lastname' => (object) array(
										'Field'=>'lastname',
										'Type'=>'varchar(100)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'firstname' => (object) array(
										'Field'=>'firstname',
										'Type'=>'varchar(100)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'middlename' => (object) array(
										'Field'=>'middlename',
										'Type'=>'varchar(100)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'birthday' => (object) array(
										'Field'=>'birthday',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'birthplace' => (object) array(
										'Field'=>'birthplace',
										'Type'=>'varchar(50)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'gender' => (object) array(
										'Field'=>'gender',
										'Type'=>'varchar(50)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'civil_status' => (object) array(
										'Field'=>'civil_status',
										'Type'=>'varchar(50)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'prefix' => (object) array(
										'Field'=>'prefix',
										'Type'=>'varchar(50)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'suffix' => (object) array(
										'Field'=>'suffix',
										'Type'=>'varchar(50)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'name_id' => "ALTER TABLE  `names_info` ADD  `name_id` int(20) NOT NULL   PRIMARY KEY;",
			'lastname' => "ALTER TABLE  `names_info` ADD  `lastname` varchar(100) NOT NULL   ;",
			'firstname' => "ALTER TABLE  `names_info` ADD  `firstname` varchar(100) NOT NULL   ;",
			'middlename' => "ALTER TABLE  `names_info` ADD  `middlename` varchar(100) NULL   ;",
			'birthday' => "ALTER TABLE  `names_info` ADD  `birthday` date NULL   ;",
			'birthplace' => "ALTER TABLE  `names_info` ADD  `birthplace` varchar(50) NULL   ;",
			'gender' => "ALTER TABLE  `names_info` ADD  `gender` varchar(50) NULL   ;",
			'civil_status' => "ALTER TABLE  `names_info` ADD  `civil_status` varchar(50) NULL   ;",
			'prefix' => "ALTER TABLE  `names_info` ADD  `prefix` varchar(50) NULL   ;",
			'suffix' => "ALTER TABLE  `names_info` ADD  `suffix` varchar(50) NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setNameId() - name_id
//setLastname() - lastname
//setFirstname() - firstname
//setMiddlename() - middlename
//setBirthday() - birthday
//setBirthplace() - birthplace
//setGender() - gender
//setCivilStatus() - civil_status
//setPrefix() - prefix
//setSuffix() - suffix

--------------------------------------

//set_name_id() - name_id
//set_lastname() - lastname
//set_firstname() - firstname
//set_middlename() - middlename
//set_birthday() - birthday
//set_birthplace() - birthplace
//set_gender() - gender
//set_civil_status() - civil_status
//set_prefix() - prefix
//set_suffix() - suffix

*/
/* End of file Names_info_model.php */
/* Location: ./application/models/Names_info_model.php */
