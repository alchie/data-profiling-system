<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Benefeciaries_hw_model Class
 *
 * Manipulates `benefeciaries_hw` table on database

CREATE TABLE `benefeciaries_hw` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name_id` int(20) NOT NULL,
  `date_hw` date NOT NULL,
  `height` decimal(10,5) NOT NULL,
  `weight` decimal(10,5) NOT NULL,
  `notes` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `benefeciaries_hw` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `benefeciaries_hw` ADD  `name_id` int(20) NOT NULL   ;
ALTER TABLE  `benefeciaries_hw` ADD  `date_hw` date NOT NULL   ;
ALTER TABLE  `benefeciaries_hw` ADD  `height` decimal(10,5) NOT NULL   ;
ALTER TABLE  `benefeciaries_hw` ADD  `weight` decimal(10,5) NOT NULL   ;
ALTER TABLE  `benefeciaries_hw` ADD  `notes` text NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Benefeciaries_hw_model extends MY_Model {

	protected $id;
	protected $name_id;
	protected $date_hw;
	protected $height;
	protected $weight;
	protected $notes;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'benefeciaries_hw';
		$this->_short_name = 'benefeciaries_hw';
		$this->_fields = array("id","name_id","date_hw","height","weight","notes");
		$this->_required = array("name_id","date_hw","height","weight");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

	public function getId() {
		return $this->id;
	}

	public function get_id() {
		return $this->id;
	}

	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: name_id -------------------------------------- 

	/** 
	* Sets a value to `name_id` variable
	* @access public
	*/

	public function setNameId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('name_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_name_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('name_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `name_id` variable
	* @access public
	*/

	public function getNameId() {
		return $this->name_id;
	}

	public function get_name_id() {
		return $this->name_id;
	}

	
// ------------------------------ End Field: name_id --------------------------------------


// ---------------------------- Start Field: date_hw -------------------------------------- 

	/** 
	* Sets a value to `date_hw` variable
	* @access public
	*/

	public function setDateHw($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('date_hw', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_date_hw($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('date_hw', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `date_hw` variable
	* @access public
	*/

	public function getDateHw() {
		return $this->date_hw;
	}

	public function get_date_hw() {
		return $this->date_hw;
	}

	
// ------------------------------ End Field: date_hw --------------------------------------


// ---------------------------- Start Field: height -------------------------------------- 

	/** 
	* Sets a value to `height` variable
	* @access public
	*/

	public function setHeight($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('height', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_height($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('height', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `height` variable
	* @access public
	*/

	public function getHeight() {
		return $this->height;
	}

	public function get_height() {
		return $this->height;
	}

	
// ------------------------------ End Field: height --------------------------------------


// ---------------------------- Start Field: weight -------------------------------------- 

	/** 
	* Sets a value to `weight` variable
	* @access public
	*/

	public function setWeight($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('weight', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_weight($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('weight', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `weight` variable
	* @access public
	*/

	public function getWeight() {
		return $this->weight;
	}

	public function get_weight() {
		return $this->weight;
	}

	
// ------------------------------ End Field: weight --------------------------------------


// ---------------------------- Start Field: notes -------------------------------------- 

	/** 
	* Sets a value to `notes` variable
	* @access public
	*/

	public function setNotes($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('notes', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_notes($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('notes', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `notes` variable
	* @access public
	*/

	public function getNotes() {
		return $this->notes;
	}

	public function get_notes() {
		return $this->notes;
	}

	
// ------------------------------ End Field: notes --------------------------------------



	
	public function get_table_options() {
		return array(
			'id' => (object) array(
										'Field'=>'id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'name_id' => (object) array(
										'Field'=>'name_id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'date_hw' => (object) array(
										'Field'=>'date_hw',
										'Type'=>'date',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'height' => (object) array(
										'Field'=>'height',
										'Type'=>'decimal(10,5)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'weight' => (object) array(
										'Field'=>'weight',
										'Type'=>'decimal(10,5)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'notes' => (object) array(
										'Field'=>'notes',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'id' => "ALTER TABLE  `benefeciaries_hw` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'name_id' => "ALTER TABLE  `benefeciaries_hw` ADD  `name_id` int(20) NOT NULL   ;",
			'date_hw' => "ALTER TABLE  `benefeciaries_hw` ADD  `date_hw` date NOT NULL   ;",
			'height' => "ALTER TABLE  `benefeciaries_hw` ADD  `height` decimal(10,5) NOT NULL   ;",
			'weight' => "ALTER TABLE  `benefeciaries_hw` ADD  `weight` decimal(10,5) NOT NULL   ;",
			'notes' => "ALTER TABLE  `benefeciaries_hw` ADD  `notes` text NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setId() - id
//setNameId() - name_id
//setDateHw() - date_hw
//setHeight() - height
//setWeight() - weight
//setNotes() - notes

--------------------------------------

//set_id() - id
//set_name_id() - name_id
//set_date_hw() - date_hw
//set_height() - height
//set_weight() - weight
//set_notes() - notes

*/
/* End of file Benefeciaries_hw_model.php */
/* Location: ./application/models/Benefeciaries_hw_model.php */
