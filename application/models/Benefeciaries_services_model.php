<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Benefeciaries_services_model Class
 *
 * Manipulates `benefeciaries_services` table on database

CREATE TABLE `benefeciaries_services` (
  `name_id` int(20) NOT NULL,
  `service_id` int(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `benefeciaries_services` ADD  `name_id` int(20) NOT NULL   ;
ALTER TABLE  `benefeciaries_services` ADD  `service_id` int(20) NOT NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Benefeciaries_services_model extends MY_Model {

	protected $name_id;
	protected $service_id;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'benefeciaries_services';
		$this->_short_name = 'benefeciaries_services';
		$this->_fields = array("name_id","service_id");
		$this->_required = array("name_id","service_id");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: name_id -------------------------------------- 

	/** 
	* Sets a value to `name_id` variable
	* @access public
	*/

	public function setNameId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('name_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_name_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('name_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `name_id` variable
	* @access public
	*/

	public function getNameId() {
		return $this->name_id;
	}

	public function get_name_id() {
		return $this->name_id;
	}

	
// ------------------------------ End Field: name_id --------------------------------------


// ---------------------------- Start Field: service_id -------------------------------------- 

	/** 
	* Sets a value to `service_id` variable
	* @access public
	*/

	public function setServiceId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('service_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_service_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('service_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `service_id` variable
	* @access public
	*/

	public function getServiceId() {
		return $this->service_id;
	}

	public function get_service_id() {
		return $this->service_id;
	}

	
// ------------------------------ End Field: service_id --------------------------------------



	
	public function get_table_options() {
		return array(
			'name_id' => (object) array(
										'Field'=>'name_id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'service_id' => (object) array(
										'Field'=>'service_id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'name_id' => "ALTER TABLE  `benefeciaries_services` ADD  `name_id` int(20) NOT NULL   ;",
			'service_id' => "ALTER TABLE  `benefeciaries_services` ADD  `service_id` int(20) NOT NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setNameId() - name_id
//setServiceId() - service_id

--------------------------------------

//set_name_id() - name_id
//set_service_id() - service_id

*/
/* End of file Benefeciaries_services_model.php */
/* Location: ./application/models/Benefeciaries_services_model.php */
