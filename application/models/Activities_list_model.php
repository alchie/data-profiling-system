<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Activities_list_model Class
 *
 * Manipulates `activities_list` table on database

CREATE TABLE `activities_list` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `notes` text,
  `benefeciaries` int(1) NOT NULL DEFAULT '0',
  `parents` int(1) NOT NULL DEFAULT '0',
  `active` int(1) NOT NULL DEFAULT '1',
  `trash` int(1) DEFAULT '0',
  `service_id` int(20) DEFAULT NULL,
  `center_id` int(20) DEFAULT NULL,
  `activity_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin;

ALTER TABLE  `activities_list` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `activities_list` ADD  `name` varchar(200) NOT NULL   ;
ALTER TABLE  `activities_list` ADD  `notes` text NULL   ;
ALTER TABLE  `activities_list` ADD  `benefeciaries` int(1) NOT NULL   DEFAULT '0';
ALTER TABLE  `activities_list` ADD  `parents` int(1) NOT NULL   DEFAULT '0';
ALTER TABLE  `activities_list` ADD  `active` int(1) NOT NULL   DEFAULT '1';
ALTER TABLE  `activities_list` ADD  `trash` int(1) NULL   DEFAULT '0';
ALTER TABLE  `activities_list` ADD  `service_id` int(20) NULL   ;
ALTER TABLE  `activities_list` ADD  `center_id` int(20) NULL   ;
ALTER TABLE  `activities_list` ADD  `activity_date` date NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Activities_list_model extends MY_Model {

	protected $id;
	protected $name;
	protected $notes;
	protected $benefeciaries;
	protected $parents;
	protected $active;
	protected $trash;
	protected $service_id;
	protected $center_id;
	protected $activity_date;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'activities_list';
		$this->_short_name = 'activities_list';
		$this->_fields = array("id","name","notes","benefeciaries","parents","active","trash","service_id","center_id","activity_date");
		$this->_required = array("name","benefeciaries","parents","active");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

	public function getId() {
		return $this->id;
	}

	public function get_id() {
		return $this->id;
	}

	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: name -------------------------------------- 

	/** 
	* Sets a value to `name` variable
	* @access public
	*/

	public function setName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_name($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `name` variable
	* @access public
	*/

	public function getName() {
		return $this->name;
	}

	public function get_name() {
		return $this->name;
	}

	
// ------------------------------ End Field: name --------------------------------------


// ---------------------------- Start Field: notes -------------------------------------- 

	/** 
	* Sets a value to `notes` variable
	* @access public
	*/

	public function setNotes($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('notes', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_notes($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('notes', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `notes` variable
	* @access public
	*/

	public function getNotes() {
		return $this->notes;
	}

	public function get_notes() {
		return $this->notes;
	}

	
// ------------------------------ End Field: notes --------------------------------------


// ---------------------------- Start Field: benefeciaries -------------------------------------- 

	/** 
	* Sets a value to `benefeciaries` variable
	* @access public
	*/

	public function setBenefeciaries($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('benefeciaries', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_benefeciaries($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('benefeciaries', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `benefeciaries` variable
	* @access public
	*/

	public function getBenefeciaries() {
		return $this->benefeciaries;
	}

	public function get_benefeciaries() {
		return $this->benefeciaries;
	}

	
// ------------------------------ End Field: benefeciaries --------------------------------------


// ---------------------------- Start Field: parents -------------------------------------- 

	/** 
	* Sets a value to `parents` variable
	* @access public
	*/

	public function setParents($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('parents', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_parents($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('parents', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `parents` variable
	* @access public
	*/

	public function getParents() {
		return $this->parents;
	}

	public function get_parents() {
		return $this->parents;
	}

	
// ------------------------------ End Field: parents --------------------------------------


// ---------------------------- Start Field: active -------------------------------------- 

	/** 
	* Sets a value to `active` variable
	* @access public
	*/

	public function setActive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('active', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_active($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('active', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `active` variable
	* @access public
	*/

	public function getActive() {
		return $this->active;
	}

	public function get_active() {
		return $this->active;
	}

	
// ------------------------------ End Field: active --------------------------------------


// ---------------------------- Start Field: trash -------------------------------------- 

	/** 
	* Sets a value to `trash` variable
	* @access public
	*/

	public function setTrash($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('trash', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_trash($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('trash', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `trash` variable
	* @access public
	*/

	public function getTrash() {
		return $this->trash;
	}

	public function get_trash() {
		return $this->trash;
	}

	
// ------------------------------ End Field: trash --------------------------------------


// ---------------------------- Start Field: service_id -------------------------------------- 

	/** 
	* Sets a value to `service_id` variable
	* @access public
	*/

	public function setServiceId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('service_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_service_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('service_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `service_id` variable
	* @access public
	*/

	public function getServiceId() {
		return $this->service_id;
	}

	public function get_service_id() {
		return $this->service_id;
	}

	
// ------------------------------ End Field: service_id --------------------------------------


// ---------------------------- Start Field: center_id -------------------------------------- 

	/** 
	* Sets a value to `center_id` variable
	* @access public
	*/

	public function setCenterId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('center_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_center_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('center_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `center_id` variable
	* @access public
	*/

	public function getCenterId() {
		return $this->center_id;
	}

	public function get_center_id() {
		return $this->center_id;
	}

	
// ------------------------------ End Field: center_id --------------------------------------


// ---------------------------- Start Field: activity_date -------------------------------------- 

	/** 
	* Sets a value to `activity_date` variable
	* @access public
	*/

	public function setActivityDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('activity_date', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_activity_date($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('activity_date', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `activity_date` variable
	* @access public
	*/

	public function getActivityDate() {
		return $this->activity_date;
	}

	public function get_activity_date() {
		return $this->activity_date;
	}

	
// ------------------------------ End Field: activity_date --------------------------------------



	
	public function get_table_options() {
		return array(
			'id' => (object) array(
										'Field'=>'id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'name' => (object) array(
										'Field'=>'name',
										'Type'=>'varchar(200)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'notes' => (object) array(
										'Field'=>'notes',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'benefeciaries' => (object) array(
										'Field'=>'benefeciaries',
										'Type'=>'int(1)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'parents' => (object) array(
										'Field'=>'parents',
										'Type'=>'int(1)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'active' => (object) array(
										'Field'=>'active',
										'Type'=>'int(1)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'1',
										'Extra'=>''
									),

			'trash' => (object) array(
										'Field'=>'trash',
										'Type'=>'int(1)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'service_id' => (object) array(
										'Field'=>'service_id',
										'Type'=>'int(20)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'center_id' => (object) array(
										'Field'=>'center_id',
										'Type'=>'int(20)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'activity_date' => (object) array(
										'Field'=>'activity_date',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'id' => "ALTER TABLE  `activities_list` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'name' => "ALTER TABLE  `activities_list` ADD  `name` varchar(200) NOT NULL   ;",
			'notes' => "ALTER TABLE  `activities_list` ADD  `notes` text NULL   ;",
			'benefeciaries' => "ALTER TABLE  `activities_list` ADD  `benefeciaries` int(1) NOT NULL   DEFAULT '0';",
			'parents' => "ALTER TABLE  `activities_list` ADD  `parents` int(1) NOT NULL   DEFAULT '0';",
			'active' => "ALTER TABLE  `activities_list` ADD  `active` int(1) NOT NULL   DEFAULT '1';",
			'trash' => "ALTER TABLE  `activities_list` ADD  `trash` int(1) NULL   DEFAULT '0';",
			'service_id' => "ALTER TABLE  `activities_list` ADD  `service_id` int(20) NULL   ;",
			'center_id' => "ALTER TABLE  `activities_list` ADD  `center_id` int(20) NULL   ;",
			'activity_date' => "ALTER TABLE  `activities_list` ADD  `activity_date` date NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setId() - id
//setName() - name
//setNotes() - notes
//setBenefeciaries() - benefeciaries
//setParents() - parents
//setActive() - active
//setTrash() - trash
//setServiceId() - service_id
//setCenterId() - center_id
//setActivityDate() - activity_date

--------------------------------------

//set_id() - id
//set_name() - name
//set_notes() - notes
//set_benefeciaries() - benefeciaries
//set_parents() - parents
//set_active() - active
//set_trash() - trash
//set_service_id() - service_id
//set_center_id() - center_id
//set_activity_date() - activity_date

*/
/* End of file Activities_list_model.php */
/* Location: ./application/models/Activities_list_model.php */
