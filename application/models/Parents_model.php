<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Parents_model Class
 *
 * Manipulates `parents` table on database

CREATE TABLE `parents` (
  `name_id` int(20) NOT NULL,
  `center_id` int(20) NOT NULL,
  `group_id` int(20) DEFAULT NULL,
  `area_id` int(20) DEFAULT NULL,
  `status` int(20) DEFAULT NULL,
  `notes` text,
  `trash` int(1) NOT NULL DEFAULT '0',
  `parent_id` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`name_id`),
  KEY `name_id` (`name_id`),
  KEY `group_id` (`group_id`),
  KEY `area_id` (`area_id`),
  KEY `center_id` (`center_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `parents` ADD  `name_id` int(20) NOT NULL   PRIMARY KEY;
ALTER TABLE  `parents` ADD  `center_id` int(20) NOT NULL   ;
ALTER TABLE  `parents` ADD  `group_id` int(20) NULL   ;
ALTER TABLE  `parents` ADD  `area_id` int(20) NULL   ;
ALTER TABLE  `parents` ADD  `status` int(20) NULL   ;
ALTER TABLE  `parents` ADD  `notes` text NULL   ;
ALTER TABLE  `parents` ADD  `trash` int(1) NOT NULL   DEFAULT '0';
ALTER TABLE  `parents` ADD  `parent_id` varchar(20) NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Parents_model extends MY_Model {

	protected $name_id;
	protected $center_id;
	protected $group_id;
	protected $area_id;
	protected $status;
	protected $notes;
	protected $trash;
	protected $parent_id;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'parents';
		$this->_short_name = 'parents';
		$this->_fields = array("name_id","center_id","group_id","area_id","status","notes","trash","parent_id");
		$this->_required = array("center_id","trash");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: name_id -------------------------------------- 

	/** 
	* Sets a value to `name_id` variable
	* @access public
	*/

	public function setNameId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('name_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_name_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('name_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `name_id` variable
	* @access public
	*/

	public function getNameId() {
		return $this->name_id;
	}

	public function get_name_id() {
		return $this->name_id;
	}

	
// ------------------------------ End Field: name_id --------------------------------------


// ---------------------------- Start Field: center_id -------------------------------------- 

	/** 
	* Sets a value to `center_id` variable
	* @access public
	*/

	public function setCenterId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('center_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_center_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('center_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `center_id` variable
	* @access public
	*/

	public function getCenterId() {
		return $this->center_id;
	}

	public function get_center_id() {
		return $this->center_id;
	}

	
// ------------------------------ End Field: center_id --------------------------------------


// ---------------------------- Start Field: group_id -------------------------------------- 

	/** 
	* Sets a value to `group_id` variable
	* @access public
	*/

	public function setGroupId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('group_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_group_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('group_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `group_id` variable
	* @access public
	*/

	public function getGroupId() {
		return $this->group_id;
	}

	public function get_group_id() {
		return $this->group_id;
	}

	
// ------------------------------ End Field: group_id --------------------------------------


// ---------------------------- Start Field: area_id -------------------------------------- 

	/** 
	* Sets a value to `area_id` variable
	* @access public
	*/

	public function setAreaId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('area_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_area_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('area_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `area_id` variable
	* @access public
	*/

	public function getAreaId() {
		return $this->area_id;
	}

	public function get_area_id() {
		return $this->area_id;
	}

	
// ------------------------------ End Field: area_id --------------------------------------


// ---------------------------- Start Field: status -------------------------------------- 

	/** 
	* Sets a value to `status` variable
	* @access public
	*/

	public function setStatus($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('status', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_status($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('status', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `status` variable
	* @access public
	*/

	public function getStatus() {
		return $this->status;
	}

	public function get_status() {
		return $this->status;
	}

	
// ------------------------------ End Field: status --------------------------------------


// ---------------------------- Start Field: notes -------------------------------------- 

	/** 
	* Sets a value to `notes` variable
	* @access public
	*/

	public function setNotes($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('notes', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_notes($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('notes', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `notes` variable
	* @access public
	*/

	public function getNotes() {
		return $this->notes;
	}

	public function get_notes() {
		return $this->notes;
	}

	
// ------------------------------ End Field: notes --------------------------------------


// ---------------------------- Start Field: trash -------------------------------------- 

	/** 
	* Sets a value to `trash` variable
	* @access public
	*/

	public function setTrash($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('trash', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_trash($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('trash', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `trash` variable
	* @access public
	*/

	public function getTrash() {
		return $this->trash;
	}

	public function get_trash() {
		return $this->trash;
	}

	
// ------------------------------ End Field: trash --------------------------------------


// ---------------------------- Start Field: parent_id -------------------------------------- 

	/** 
	* Sets a value to `parent_id` variable
	* @access public
	*/

	public function setParentId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('parent_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_parent_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('parent_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `parent_id` variable
	* @access public
	*/

	public function getParentId() {
		return $this->parent_id;
	}

	public function get_parent_id() {
		return $this->parent_id;
	}

	
// ------------------------------ End Field: parent_id --------------------------------------



	
	public function get_table_options() {
		return array(
			'name_id' => (object) array(
										'Field'=>'name_id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>''
									),

			'center_id' => (object) array(
										'Field'=>'center_id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'group_id' => (object) array(
										'Field'=>'group_id',
										'Type'=>'int(20)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'area_id' => (object) array(
										'Field'=>'area_id',
										'Type'=>'int(20)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'status' => (object) array(
										'Field'=>'status',
										'Type'=>'int(20)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'notes' => (object) array(
										'Field'=>'notes',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'trash' => (object) array(
										'Field'=>'trash',
										'Type'=>'int(1)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'parent_id' => (object) array(
										'Field'=>'parent_id',
										'Type'=>'varchar(20)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'name_id' => "ALTER TABLE  `parents` ADD  `name_id` int(20) NOT NULL   PRIMARY KEY;",
			'center_id' => "ALTER TABLE  `parents` ADD  `center_id` int(20) NOT NULL   ;",
			'group_id' => "ALTER TABLE  `parents` ADD  `group_id` int(20) NULL   ;",
			'area_id' => "ALTER TABLE  `parents` ADD  `area_id` int(20) NULL   ;",
			'status' => "ALTER TABLE  `parents` ADD  `status` int(20) NULL   ;",
			'notes' => "ALTER TABLE  `parents` ADD  `notes` text NULL   ;",
			'trash' => "ALTER TABLE  `parents` ADD  `trash` int(1) NOT NULL   DEFAULT '0';",
			'parent_id' => "ALTER TABLE  `parents` ADD  `parent_id` varchar(20) NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setNameId() - name_id
//setCenterId() - center_id
//setGroupId() - group_id
//setAreaId() - area_id
//setStatus() - status
//setNotes() - notes
//setTrash() - trash
//setParentId() - parent_id

--------------------------------------

//set_name_id() - name_id
//set_center_id() - center_id
//set_group_id() - group_id
//set_area_id() - area_id
//set_status() - status
//set_notes() - notes
//set_trash() - trash
//set_parent_id() - parent_id

*/
/* End of file Parents_model.php */
/* Location: ./application/models/Parents_model.php */
