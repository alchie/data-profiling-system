<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Activities_attendance_model Class
 *
 * Manipulates `activities_attendance` table on database

CREATE TABLE `activities_attendance` (
  `activity_id` int(20) NOT NULL,
  `name_id` int(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `activities_attendance` ADD  `activity_id` int(20) NOT NULL   ;
ALTER TABLE  `activities_attendance` ADD  `name_id` int(20) NOT NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Activities_attendance_model extends MY_Model {

	protected $activity_id;
	protected $name_id;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'activities_attendance';
		$this->_short_name = 'activities_attendance';
		$this->_fields = array("activity_id","name_id");
		$this->_required = array("activity_id","name_id");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: activity_id -------------------------------------- 

	/** 
	* Sets a value to `activity_id` variable
	* @access public
	*/

	public function setActivityId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('activity_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_activity_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('activity_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `activity_id` variable
	* @access public
	*/

	public function getActivityId() {
		return $this->activity_id;
	}

	public function get_activity_id() {
		return $this->activity_id;
	}

	
// ------------------------------ End Field: activity_id --------------------------------------


// ---------------------------- Start Field: name_id -------------------------------------- 

	/** 
	* Sets a value to `name_id` variable
	* @access public
	*/

	public function setNameId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('name_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_name_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('name_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `name_id` variable
	* @access public
	*/

	public function getNameId() {
		return $this->name_id;
	}

	public function get_name_id() {
		return $this->name_id;
	}

	
// ------------------------------ End Field: name_id --------------------------------------



	
	public function get_table_options() {
		return array(
			'activity_id' => (object) array(
										'Field'=>'activity_id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'name_id' => (object) array(
										'Field'=>'name_id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'activity_id' => "ALTER TABLE  `activities_attendance` ADD  `activity_id` int(20) NOT NULL   ;",
			'name_id' => "ALTER TABLE  `activities_attendance` ADD  `name_id` int(20) NOT NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setActivityId() - activity_id
//setNameId() - name_id

--------------------------------------

//set_activity_id() - activity_id
//set_name_id() - name_id

*/
/* End of file Activities_attendance_model.php */
/* Location: ./application/models/Activities_attendance_model.php */
