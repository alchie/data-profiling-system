<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Parents_groups extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->template_data->set('current_page', 'Parent Groups');
		$this->template_data->set('current_uri', 'parents_groups');
		$this->template_data->set('navbar_search', true);

		$this->_isAuth('parents', 'parents', 'edit');

	}

	public function index($start=0) {
		
		$groups = new $this->Parents_groups_model;
		if( $this->input->get('q') ) {
			$groups->set_where('name LIKE "%' . $this->input->get('q') . '%"', NULL, 99);
		}
		$groups->setCenterId($this->session->userdata('current_center_id'),true);
		if( $this->input->get('filter') == 'trash' ) {
			$groups->setTrash(1, true);
		} else {
			$groups->setTrash(0, true);
		}
		$groups->set_select("*");
		$groups->set_select("(SELECT COUNT(*) FROM `parents` WHERE group_id=parents_groups.id) as parents_count");
		$groups->set_order('name', 'ASC');
		$groups->set_start($start);
		$this->template_data->set('groups', $groups->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => base_url($this->config->item('index_page') . '/parents_groups/index/'),
			'total_rows' => $groups->count_all_results(),
			'per_page' => $groups->get_limit(),
			'ajax'=>true,
		)));
		
		$this->load->view('parents/groups/groups_list', $this->template_data->get_data());
	}

	public function add($output='') {

		$this->_isAuth('parents', 'groups', 'add');

		if( $this->input->post() ) {
			$this->form_validation->set_rules('group_name', 'Group Name', 'trim|required');
			$this->form_validation->set_rules('notes', 'Notes', 'trim');
			if( $this->form_validation->run() ) {
				$groups = new $this->Parents_groups_model;
				$groups->setName($this->input->post('group_name'));
				$groups->setNotes($this->input->post('notes'));
				$groups->setCenterId($this->session->userdata('current_center_id'));
				if( $groups->insert() ) {
					record_system_audit($this->session->userdata('user_id'), 'parents', 'groups', 'add', $this->session->userdata('current_center_id'), "Parent Group Added : {$this->input->post('group_name')}");
				}
				$this->getNext("parents_groups");
			}
		}

		$this->template_data->set('output', $output);
		$this->load->view('parents/groups/groups_add', $this->template_data->get_data());
	}

	public function edit($id,$output='') {

		$this->_isAuth('parents', 'groups', 'edit');

		$groups = new $this->Parents_groups_model;
		$groups->setId($id,true);
		if( $groups->nonEmpty() ) {
			$group_data = $groups->getResults();
			if( $this->input->post() ) {
				$this->form_validation->set_rules('group_name', 'Group Name', 'trim|required');
				$this->form_validation->set_rules('notes', 'Notes', 'trim');
				if( $this->form_validation->run() ) {
					$groups->setName($this->input->post('group_name'),false,true);
					$groups->setNotes($this->input->post('notes'),false,true);
					if( $groups->update() ) {
						record_system_audit($this->session->userdata('user_id'), 'parents', 'groups', 'edit', $this->session->userdata('current_center_id'), "Parent Group Updated : {$group_data->name}");
					}
				}
				$this->postNext();
			}
		}

		$groups->set_select("*");
		$groups->set_select("(SELECT COUNT(*) FROM `parents` WHERE group_id=parents_groups.id) as parents_count");
		$this->template_data->set('group', $groups->get());

		$this->template_data->set('output', $output);
		$this->load->view('parents/groups/groups_edit', $this->template_data->get_data());
	}

	public function deactivate($id) {
		
		$this->_isAuth('parents', 'groups', 'delete');

		$groups = new $this->Parents_groups_model;
		$groups->setId($id,true,false);
		$groups->setTrash(1,false,true);

		$group_data = $groups->get();

		if( $groups->update() ) {
			record_system_audit($this->session->userdata('user_id'), 'parents', 'groups', 'deactivate', $this->session->userdata('current_center_id'), "Parent Group Deactivate : {$group_data->name}");
		}

		$this->getNext("parents_groups");
	}

	public function restore($id) {
		
		$this->_isAuth('parents', 'groups', 'delete');

		$groups = new $this->Parents_groups_model;
		$groups->setId($id,true,false);
		$groups->setTrash(0,false,true);

		$group_data = $groups->get();

		if( $groups->update() ) {
			record_system_audit($this->session->userdata('user_id'), 'parents', 'groups', 'restore', $this->session->userdata('current_center_id'), "Parent Group Restore : {$group_data->name}");
		}

		$this->getNext("parents_groups");
	}
}
