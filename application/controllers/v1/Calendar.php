<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Calendar extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->template_data->set('current_page', 'Calendar');
		$this->template_data->set('current_uri', 'calendar');
		$this->template_data->set('navbar_search', false);

		$this->_isAuth('calendar', 'calendar', 'view');

	}

	public function index($month=0,$year=0,$output='') {

		$current_month = ($month) ? $month : date('m');
		$current_year = ($year) ? $year : date('Y');
		$this->template_data->set('current_month', $current_month);
		$this->template_data->set('current_year', $current_year);

		$activities = new $this->Activities_list_model('c');
		$activities->set_select("c.*");
		$activities->setCenterId($this->session->userdata('current_center_id'),true);
		$activities->set_where("(MONTH(c.activity_date)='{$current_month}')");
		$activities->set_where("(YEAR(c.activity_date)='{$current_year}')");
		$activities->set_limit(0);
		$this->template_data->set('activities', $activities->populate());

		$birthdays = new $this->Names_info_model('ni');
		$birthdays->set_where('(MONTH(ni.birthday)='.$current_month.')');
		$birthdays->set_where('e.center_id', $this->session->userdata( 'current_center_id'));
		$birthdays->set_join('benefeciaries e', 'e.name_id=ni.name_id');
		$birthdays->set_order('(DAY(ni.birthday))', 'ASC');
		//$birthdays->set_select("ni.*");
		$birthdays->set_select("COUNT(*) as bdays");
		$birthdays->set_select("DAY(ni.birthday) as birth_day");
		//$birthdays->set_select("(TIMESTAMPDIFF(YEAR, ni.birthday, CURDATE())) as age");
		$birthdays->set_limit(0);
		$birthdays->set_group_by('DAY(ni.birthday)');
		$this->template_data->set('birthdays', $birthdays->populate());

		$this->template_data->set('output', $output);
		$this->load->view('calendar/calendar_list', $this->template_data->get_data());
	}

	public function edit($list_date, $output='') {

		$current = new $this->Calendar_model;
		$current->setCalendarDate($list_date,true);
		$current->setCompanyId($this->session->userdata('current_center_id'),true);

		if( $this->input->post() ) {
			if( $current->nonEmpty() ) {
				$current->setIsHoliday( (($this->input->post('is_holiday')) ? 1 : 0), false, true);
				$current->setHolidayType($this->input->post('holiday_type'), false, true);
				$current->setPremium($this->input->post('holiday_premium'), false, true);
				$current->setNotes($this->input->post('holiday_notes'), false, true);
				$current->update();
			} else {
				$current = new $this->Calendar_model;
				$current->setCalendarDate($list_date,false,true);
				$current->setCompanyId($this->session->userdata('current_center_id'),false,true);
				$current->setCompanyId($this->session->userdata('current_center_id'),true);
				$current->setIsHoliday( (($this->input->post('is_holiday')) ? 1 : 0), false, true);
				$current->setHolidayType($this->input->post('holiday_type'), false, true);
				$current->setPremium($this->input->post('holiday_premium'), false, true);
				$current->setNotes($this->input->post('holiday_notes'), false, true);
				$current->insert();
			}
			$this->postNext("system_calendar");
		}

		$this->template_data->set('current_date', $current->get());

		$terms = new $this->Terms_list_model;
		$terms->set_select("*");
		$terms->set_order('priority', 'ASC');
		$terms->set_order('name', 'ASC');
		$terms->set_start(0);
		$terms->setTrash('0',true);
		$terms->setType('holiday',true);
		$this->template_data->set('holidays', $terms->populate());

		$this->template_data->set('output', $output);
		$this->load->view('calendar/calendar_edit', $this->template_data->get_data());
	}

}
