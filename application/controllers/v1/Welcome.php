<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
	}

	public function index() {

		//redirect("calendar");

		$current_month = date('m');
		$current_year = date('Y');
		$this->template_data->set('current_month', $current_month);
		$this->template_data->set('current_year', $current_year);

		$activities = new $this->Activities_list_model('c');
		$activities->set_select("c.*");
		$activities->setCenterId($this->session->userdata('current_center_id'),true);
		$activities->set_where("(MONTH(c.activity_date)='{$current_month}')");
		$activities->set_where("(YEAR(c.activity_date)='{$current_year}')");
		$this->template_data->set('activities', $activities->populate());
		
		if( $this->config->item('multi_company') ) {
			$centers = new $this->User_accounts_centers_model('uc');
			$centers->setUid($this->session->userdata('user_id'),true);
			$centers->set_join('centers_list cl', 'uc.center_id=cl.id');
			$centers->set_select('cl.*');
			$centers->set_where('cl.id !=' . $this->session->userdata( 'current_center_id') );
			$this->template_data->set('centers', $centers->populate());
		}

		$birthdays = new $this->Names_info_model('ni');
		$birthdays->set_where('(MONTH(ni.birthday)='.$current_month.')');
		$birthdays->set_where('e.center_id', $this->session->userdata( 'current_center_id'));
		$birthdays->set_join('benefeciaries e', 'e.name_id=ni.name_id');
		$birthdays->set_order('(DAY(ni.birthday))', 'ASC');
		//$birthdays->set_select("ni.*");
		$birthdays->set_select("COUNT(*) as bdays");
		$birthdays->set_select("DAY(ni.birthday) as birth_day");
		//$birthdays->set_select("(TIMESTAMPDIFF(YEAR, ni.birthday, CURDATE())) as age");
		$birthdays->set_limit(0);
		$birthdays->set_group_by('DAY(ni.birthday)');
		$this->template_data->set('birthdays', $birthdays->populate());

		$this->load->view('welcome/welcome', $this->template_data->get_data());
	}

	public function select_company($id=NULL,$output='') {

		$companies = new $this->User_accounts_centers_model('uc');
		$companies->setUid($this->session->userdata('user_id'),true);
		$companies->set_join('centers_list cl', 'uc.center_id=cl.id');
		$companies->set_select('cl.*');
		if($id) {
			$companies->set_where('cl.id', $id);
			if( $companies->nonEmpty() ) {
				$company =  $companies->getResults();
				$this->session->set_userdata( 'current_company', $company->name );
				$this->session->set_userdata( 'current_center_id', $company->id );
				$this->session->set_userdata( 'current_company_theme', $company->theme );
				$this->session->set_userdata('current_payroll', false );
				$this->session->set_userdata('employees_status', false);
				redirect(site_url('welcome'));
			} else {
				redirect(site_url('welcome/select_company'));
			}
		}
		$companies->set_where('cl.id !=' . $this->session->userdata( 'current_center_id') );
		$this->template_data->set('companies', $companies->populate());

		$this->template_data->set( 'output', $output );
		$this->load->view('welcome/select_company', $this->template_data->get_data());
	}

	public function select_center($id=NULL,$output='') {

		$centers = new $this->User_accounts_centers_model('uc');
		$centers->setUid($this->session->userdata('user_id'),true);
		$centers->set_join('centers_list cl', 'uc.center_id=cl.id');
		$centers->set_select('cl.*');
		if($id) {
			$centers->set_where('cl.id', $id);
			if( $centers->nonEmpty() ) {
				$center =  $centers->getResults();
				$this->session->set_userdata( 'current_center', $center->name );
				$this->session->set_userdata( 'current_company', $center->name );
				$this->session->set_userdata( 'current_center_id', $center->id );
				$this->session->set_userdata( 'current_center_theme', $center->theme );
				$this->session->set_userdata( 'current_company_theme', $center->theme );
				$this->session->set_userdata('current_payroll', false );
				$this->session->set_userdata('employees_status', false);
				redirect(site_url('welcome'));
			} else {
				redirect(site_url('welcome/select_company'));
			}
		}
		$centers->set_where('cl.id !=' . $this->session->userdata( 'current_center_id') );
		$this->template_data->set('centers', $centers->populate());

		$this->template_data->set( 'output', $output );
		$this->load->view('welcome/select_center', $this->template_data->get_data());
	}

	public function change_company($center_id) {
		$default_company = new $this->Companies_list_model;
		$default_company->setId($center_id,true);
		if( $default_company->nonEmpty() ) {
				$company = $default_company->getResults();
				$this->session->set_userdata( 'current_company', $company->name );
				$this->session->set_userdata( 'current_center_id', $company->id );
				$this->session->set_userdata( 'current_company_theme', $company->theme );
				$this->session->set_userdata('employees_status', false);
				$this->session->set_userdata('current_employee', false);
		}
		redirect("welcome");
	}

	public function change_current_theme($theme) {
		$this->session->set_userdata( 'current_company_theme', $theme );
		redirect( $this->input->get('uri') );
	}

	public function filter_benefeciary($name_id) {
		$benefeciary = new $this->Benefeciaries_model('e');
		$benefeciary->setNameId($name_id,true);
		$benefeciary->set_join('names_info ni', 'ni.name_id=e.name_id');
		$this->session->set_userdata('current_employee', $benefeciary->get() );
		redirect( site_url( $this->input->get('next') ) . "?error_code=101" );
	}
	
	public function ajax($action='') {
		$results = array();
		switch($action) {
			case 'search_benefeciaries':

				if( ! $this->_isAuth('benefeciaries', 'benefeciaries', 'view', 'welcome', true) ) {
					break;
				}

				$benefeciaries = new $this->Benefeciaries_model('e');
				$benefeciaries->set_join('names_info ni', 'ni.name_id=e.name_id');

				if( $this->input->get('term') ) {
					$benefeciaries->set_where('ni.lastname LIKE "%' . $this->input->get('term') . '%"');
					$benefeciaries->set_where_or('ni.firstname LIKE "%' . $this->input->get('term') . '%"');
					$benefeciaries->set_where_or('ni.middlename LIKE "%' . $this->input->get('term') . '%"');
				}

				$benefeciaries->set_order('ni.lastname', 'ASC');
				$benefeciaries->set_order('ni.firstname', 'ASC');
				$benefeciaries->set_order('ni.middlename', 'ASC');
				$benefeciaries->set_limit(0); 
				$benefeciaries->setCenterId($this->session->userdata('current_center_id'),true);
				
				$ctrl = 'lists_names/profile/';
				if( strpos($this->input->get('uri_string'), 'employees_earnings') ) {
					$ctrl = 'employees_earnings/view/';
				}
				elseif( strpos($this->input->get('uri_string'), 'employees_benefits') ) {
					$ctrl = 'employees_benefits/view/';
				}
				elseif( strpos($this->input->get('uri_string'), 'employees_deductions') ) {
					$ctrl = 'employees_deductions/view/';
				}
				elseif( strpos($this->input->get('uri_string'), 'employees_salaries') ) {
					$ctrl = 'employees_salaries/view/';
				}
				
				foreach($benefeciaries->populate() as $benefeciary) {
					$results[] = array(
						'label' => $benefeciary->lastname . ", " . $benefeciary->firstname. " " . substr($benefeciary->middlename,0,1).".",
						'id' => $benefeciary->name_id,
						'redirect'=> site_url( "welcome/filter_benefeciary/{$benefeciary->name_id}" ) . "?next=" . $ctrl . $benefeciary->name_id,
						);
				}
			break;
		}
		$this->output
        ->set_content_type('application/json')
        ->set_output(json_encode( $results ));
	}

	public function change_password($output='')
	{

		$this->template_data->set('page_title', 'Change Password');

		if( count($this->input->post()) > 0 ) {
			$this->form_validation->set_rules('current_password', 'Current Password', 'trim|required');
			$this->form_validation->set_rules('new_password', 'New Password', 'trim|required');
			$this->form_validation->set_rules('repeat_password', 'Repeat Password', 'trim|required|matches[new_password]');
			if( $this->form_validation->run() != FALSE) {
				$account = new $this->User_accounts_model;
				$account->setId($this->session->user_id,true);
				$account->setPassword(sha1($this->input->post('new_password'))); 
				$account->set_where('password LIKE', sha1($this->input->post('current_password')));
				if( $account->nonEmpty() ) {
					$account->set_exclude( array('id', 'username', 'name') );
					if( $account->update() ) {
						record_system_audit($this->session->userdata('user_id'), 'welcome', 'change_password', 'update', $this->session->userdata('current_center_id'), "Password Changed!");
					}
					//redirect(site_url('account/change_password') . "?success=true");
				} else {
					//redirect(site_url('account/change_password') . "?error=true");
				}
			}
			$this->postNext(NULL, $output);
		}

		$this->template_data->set( 'output', $output );
		$this->load->view('account/change_password', $this->template_data->get_data());
	}

	public function settings($output='')
	{

		$this->template_data->set('page_title', 'Account Settings');

		$user_account = new $this->User_accounts_model;
		$user_account->setId($this->session->user_id,true,false);

		if( $this->input->post() ) {

			if( $this->input->post('full_name') ) {
				$user_account->setName($this->input->post('full_name'),false,true);
				$user_account->update();
				$this->session->set_userdata( 'name', $this->input->post('full_name') );
			}

			if( $this->input->post('setting') ) {
				if( $this->input->post('setting') ) foreach($this->input->post('setting') as $key=>$value) {
					$option = new $this->User_accounts_options_model;
					$option->setUid($this->session->user_id,true,false);
					$option->setKey($key,true,false);
					$option->setDepartment('my',true,false);
					$option->setSection('settings',true,false);
					$option->setValue($value);
					if( $option->nonEmpty() ) {
						if( $option->update() ) {
							record_system_audit($this->session->userdata('user_id'), 'welcome', 'settings', 'update', $this->session->userdata('current_center_id'), "Settings Changed!");
						}
					} else {
						if( $option->insert() ) {
							record_system_audit($this->session->userdata('user_id'), 'welcome', 'settings', 'update', $this->session->userdata('current_center_id'), "Settings Changed!");
						}
					}

					if( $key == 'theme') {
						$user_settings = $this->session->user_settings;
						$user_settings['theme'] = $value;
						$this->session->set_userdata( 'user_settings', $user_settings );
					}
				}
				$this->postNext(NULL, $output);
			}
		}

		$this->template_data->set( 'user_account', $user_account->get() );

		$options = new $this->User_accounts_options_model;
		$options->setUid($this->session->user_id,true,false);
		$options->setDepartment('my',true,false);
		$options->setSection('settings',true,false);
		$options->setKey('theme',true,false);
		$this->template_data->set( 'user_theme', $options->get() );

		$centers = new $this->User_accounts_centers_model('uc');
		$centers->setUid($this->session->userdata('user_id'),true);
		$centers->set_join('centers_list cl', 'uc.center_id=cl.id');
		$centers->set_select('cl.*');
		$centers->set_order('cl.name');
		$this->template_data->set( 'centers', $centers->populate() );

		$options = new $this->User_accounts_options_model;
		$options->setUid($this->session->user_id,true,false);
		$options->setDepartment('my',true,false);
		$options->setSection('settings',true,false);
		$options->setKey('default_center',true,false);
		$this->template_data->set( 'default_center', $options->get() );

		$this->template_data->set( 'output', $output );
		$this->load->view('account/settings', $this->template_data->get_data());
	}

}
