<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lists_services extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->template_data->set('current_page', 'Services');
		$this->template_data->set('current_uri', 'lists_services');
		$this->template_data->set('navbar_search', false);

		$this->_isAuth('lists', 'services', 'view');

	}

	public function index($start=0) {
		
		$services = new $this->Services_list_model;
		if( $this->input->get('q') ) {
			$services->set_where('(name LIKE "%' . $this->input->get('q') . '%"', NULL, 99);
			$services->set_where_or('notes LIKE "%' . $this->input->get('q') . '%")', NULL, 99);
		}
		$services->set_select("*");
		$services->set_order('name', 'ASC');
		$services->set_start($start);
		$services->setTrash('0',true);
		$services->setActive('1',true);
		$this->template_data->set('services', $services->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => base_url($this->config->item('index_page') . '/lists_services/index/'),
			'total_rows' => $services->count_all_results(),
			'per_page' => $services->get_limit(),
			'ajax'=>true,
		)));
		
		$this->load->view('lists/services/services_list', $this->template_data->get_data());
	}

	public function trash($start=0) {
		
		$services = new $this->Services_list_model;
		if( $this->input->get('q') ) {
			$services->set_where('(name LIKE "%' . $this->input->get('q') . '%"', NULL, 99);
			$services->set_where_or('notes LIKE "%' . $this->input->get('q') . '%")', NULL, 99);
		}
		$services->set_select("*");
		$services->set_order('name', 'ASC');
		$services->set_start($start);
		$services->setTrash('1',true);
		$services->setActive('0',true);
		$this->template_data->set('services', $services->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => base_url($this->config->item('index_page') . '/lists_services/index/'),
			'total_rows' => $services->count_all_results(),
			'per_page' => $services->get_limit(),
			'ajax'=>true,
		)));
		
		$this->load->view('lists/services/services_trash', $this->template_data->get_data());
	}

	public function add($output='') {

		$this->_isAuth('lists', 'services', 'add');

		if( $this->input->post() ) {
			$this->form_validation->set_rules('service_name', 'Service Name', 'trim|required');
			$this->form_validation->set_rules('notes', 'Notes', 'trim');
			if( $this->form_validation->run() ) {
				$services = new $this->Services_list_model;
				$services->setName($this->input->post('service_name'));
				$services->setNotes($this->input->post('notes'));
				$services->setBenefeciaries( ( $this->input->post('benefeciaries') ) ? 1 : 0 );
				$services->setParents( ( $this->input->post('parents') ) ? 1 : 0 );
				if( $services->insert() ) {
					
					record_system_audit($this->session->userdata('user_id'), 'lists', 'services', 'add', $this->session->userdata('current_company_id'), "Service Added: {$this->input->post('service_name')}");

					$this->getNext("lists_services");
				}
			}
		}

		$this->template_data->set('output', $output);
		$this->load->view('lists/services/services_add', $this->template_data->get_data());
	}

	public function edit($id,$output='') {

		$this->_isAuth('lists', 'services', 'edit');

		$services = new $this->Services_list_model;
		$services->setId($id,true,false);

		if( $services->nonEmpty() ) {
			if( $this->input->post() ) {
				$this->form_validation->set_rules('service_name', 'Service Name', 'trim|required');
				$this->form_validation->set_rules('notes', 'Notes', 'trim');
				if( $this->form_validation->run() ) {
					$services->setName($this->input->post('service_name'),false,true);
					$services->setNotes($this->input->post('notes'),false,true);
					$services->setBenefeciaries( ( ( $this->input->post('benefeciaries') ) ? 1 : 0) ,false,true );
					$services->setParents( ( ( $this->input->post('parents') ) ? 1 : 0 ) ,false,true );
					if( $services->update() ) {
						record_system_audit($this->session->userdata('user_id'), 'lists', 'services', 'edit', $this->session->userdata('current_company_id'), "Service Edited: {$this->input->post('service_name')}");
					}
				}
				$this->postNext();
			}
		}

		$services->set_select("*");
		$this->template_data->set('service', $services->get());

		$this->template_data->set('output', $output);
		$this->load->view('lists/services/services_edit', $this->template_data->get_data());
	}

	public function delete($id) {
		
		$this->_isAuth('lists', 'services', 'delete');

		$services = new $this->Services_list_model;
		$services->setId($id,true,false);
		$services->setActive('0',false,true);
		$services->setTrash('1',false,true);
		if( $services->update() ) {
			record_system_audit($this->session->userdata('user_id'), 'lists', 'services', 'deactivate', $this->session->userdata('current_company_id'), "Service Deactivated!");
		}

		$this->getNext("lists_services");
	}

	public function deactivate($id) {
		
		$this->_isAuth('lists', 'services', 'delete');

		$services = new $this->Services_list_model;
		$services->setId($id,true,false);
		$services->setActive('0',false,true);
		$services->setTrash('1',false,true);
		if( $services->update() ) {
			record_system_audit($this->session->userdata('user_id'), 'lists', 'services', 'deactivate', $this->session->userdata('current_company_id'), "Service Deactivated!");
		}

		$this->getNext("lists_services");
	}

	public function activities($service_id, $start=0) {
		
		$services = new $this->Services_list_model;
		$services->setId($service_id,true);
		$services->set_select("*");
		$this->template_data->set('service', $services->get());

		$activities = new $this->Activities_list_model('aa');
		$activities->setServiceId($service_id,true);
		$activities->set_select('aa.*');
		if( $this->input->get('q') ) {
			$activities->set_where('(aa.name LIKE "%' . $this->input->get('q') . '%"', NULL, 99);
			$activities->set_where_or('aa.notes LIKE "%' . $this->input->get('q') . '%")', NULL, 99);
		}
		$this->template_data->set('activities', $activities->populate());
		$this->template_data->set('activities_count', $activities->count_all_results());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => base_url($this->config->item('index_page') . '/lists_services/activities/' . $service_id),
			'total_rows' => $activities->count_all_results(),
			'per_page' => $activities->get_limit(),
			'ajax'=>true,
		)));
		
		$this->load->view('lists/services/services_activities', $this->template_data->get_data());
	}

}
