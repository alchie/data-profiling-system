<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Activities extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->template_data->set('current_page', 'Activities');
		$this->template_data->set('current_uri', 'activities');
		$this->template_data->set('navbar_search', false);

		$this->_isAuth('activities', 'activities', 'view');

	}

	public function index($current_day=NULL, $start=0) {
		
		if( $current_day === NULL ) {
			$current_day = date('Y-m-d');
		}
		$this->template_data->set('current_day', $current_day);

		$activities = new $this->Activities_list_model;
		if( $this->input->get('q') ) {
			$activities->set_where('(name LIKE "%' . $this->input->get('q') . '%"', NULL, 99);
			$activities->set_where_or('notes LIKE "%' . $this->input->get('q') . '%")', NULL, 99);
		}
		$activities->set_select("*");
		$activities->set_order('name', 'ASC');
		$activities->set_start($start);
		$activities->setTrash('0',true);
		$activities->setActive('1',true);
		$activities->setCenterId($this->session->userdata('current_center_id'),true);
		$activities->setActivityDate( date('Y-m-d', strtotime($current_day)), true );
		$this->template_data->set('activities', $activities->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => base_url($this->config->item('index_page') . '/activities/index/'),
			'total_rows' => $activities->count_all_results(),
			'per_page' => $activities->get_limit(),
			'ajax'=>true,
		)));

		$birthdays = new $this->Names_info_model('ni');
		$birthdays->set_where('(MONTH(ni.birthday)='.date('m', strtotime($current_day)).')');
		$birthdays->set_where('(DAY(ni.birthday)='.date('d', strtotime($current_day)).')');
		$birthdays->set_where('e.center_id', $this->session->userdata( 'current_center_id'));
		$birthdays->set_join('benefeciaries e', 'e.name_id=ni.name_id');
		$birthdays->set_order('(DAY(ni.birthday))', 'ASC');
		$birthdays->set_select("ni.*");
		$birthdays->set_select("(TIMESTAMPDIFF(YEAR, ni.birthday, CURDATE())) as age");
		$birthdays->set_limit(0);
		$this->template_data->set('birthdays', $birthdays->populate());

		$this->load->view('activities/activities_list', $this->template_data->get_data());
	}

	public function trash($start=0) {
		
		$activities = new $this->Activities_list_model;
		if( $this->input->get('q') ) {
			$activities->set_where('(name LIKE "%' . $this->input->get('q') . '%"', NULL, 99);
			$activities->set_where_or('notes LIKE "%' . $this->input->get('q') . '%")', NULL, 99);
		}
		$activities->set_select("*");
		$activities->set_order('name', 'ASC');
		$activities->set_start($start);
		$activities->setTrash('1',true);
		$activities->setActive('0',true);
		$this->template_data->set('activities', $activities->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => base_url($this->config->item('index_page') . '/activities/index/'),
			'total_rows' => $activities->count_all_results(),
			'per_page' => $activities->get_limit(),
			'ajax'=>true,
		)));
		
		$this->load->view('activities/activities_trash', $this->template_data->get_data());
	}

	public function add($current_day, $output='') {

		$this->_isAuth('activities', 'activities', 'add');

		if( $this->input->post() ) {
			$this->form_validation->set_rules('activity_name', 'Activity Name', 'trim|required');
			$this->form_validation->set_rules('notes', 'Notes', 'trim');
			if( $this->form_validation->run() ) {
				$activities = new $this->Activities_list_model;
				$activities->setName($this->input->post('activity_name'));
				$activities->setNotes($this->input->post('notes'));
				$activities->setServiceId($this->input->post('service_id'));
				$activities->setBenefeciaries( ( $this->input->post('benefeciaries') ) ? 1 : 0 );
				$activities->setParents( ( $this->input->post('parents') ) ? 1 : 0 );
				$activities->setCenterId($this->session->userdata('current_center_id'));
				$activities->setActivityDate( date('Y-m-d', strtotime($current_day)) );
				if( $activities->insert() ) {
					
					record_system_audit($this->session->userdata('user_id'), 'activities', 'activities', 'add', $this->session->userdata('current_company_id'), "Activity Added: {$this->input->post('activity_name')}");

					$this->getNext("activities");
				}
			}
		}

		$services = new $this->Services_list_model;
		$services->set_select("*");
		$services->set_order('name', 'ASC');
		$services->setTrash('0',true);
		$services->setActive('1',true);
		$this->template_data->set('services', $services->populate());

		$this->template_data->set('output', $output);
		$this->load->view('activities/activities_add', $this->template_data->get_data());
	}

	public function edit($id,$output='') {

		$this->_isAuth('activities', 'activities', 'edit');

		$activities = new $this->Activities_list_model;
		$activities->setId($id,true,false);

		if( $activities->nonEmpty() ) {
			if( $this->input->post() ) {
				$this->form_validation->set_rules('activity_name', 'Activity Name', 'trim|required');
				$this->form_validation->set_rules('notes', 'Notes', 'trim');
				if( $this->form_validation->run() ) {
					$activities->setName($this->input->post('activity_name'),false,true);
					$activities->setNotes($this->input->post('notes'),false,true);
					$activities->setServiceId($this->input->post('service_id'),false,true);
					$activities->setBenefeciaries( ( ( $this->input->post('benefeciaries') ) ? 1 : 0) ,false,true );
					$activities->setParents( ( ( $this->input->post('parents') ) ? 1 : 0 ) ,false,true );
					if( $activities->update() ) {
						record_system_audit($this->session->userdata('user_id'), 'activities', 'activities', 'edit', $this->session->userdata('current_company_id'), "Activity Edited: {$this->input->post('activity_name')}");
					}
				}
				$this->postNext();
			}
		}

		$activities->set_select("*");
		$this->template_data->set('activity', $activities->get());

		$services = new $this->Services_list_model;
		$services->set_select("*");
		$services->set_order('name', 'ASC');
		$services->setTrash('0',true);
		$services->setActive('1',true);
		$this->template_data->set('services', $services->populate());

		$this->template_data->set('output', $output);
		$this->load->view('activities/activities_edit', $this->template_data->get_data());
	}

	public function delete($id) {
		
		$this->_isAuth('activities', 'activities', 'delete');

		$activities = new $this->Activities_list_model;
		$activities->setId($id,true,false);
		$activities->setActive('0',false,true);
		$activities->setTrash('1',false,true);
		if( $activities->update() ) {
			record_system_audit($this->session->userdata('user_id'), 'activities', 'activities', 'deactivate', $this->session->userdata('current_company_id'), "Activity Deactivated!");
		}

		$this->getNext("activities");
	}

	public function deactivate($id) {
		
		$this->_isAuth('activities', 'activities', 'delete');

		$activities = new $this->Activities_list_model;
		$activities->setId($id,true,false);
		$activities->setActive('0',false,true);
		$activities->setTrash('1',false,true);
		if( $activities->update() ) {
			record_system_audit($this->session->userdata('user_id'), 'activities', 'activities', 'deactivate', $this->session->userdata('current_company_id'), "Activity Deactivated!");
		}

		$this->getNext("activities");
	}

	public function select_attendance($id, $start=0) {
		$this->session->set_userdata('current_activity', $id);
		redirect("activities/attendance/{$id}");
	}

	public function add_activity($ids, $ouput='') {

		if( $this->input->post('activity_id') ) {
			$ids = implode("-", $this->input->post('activity_id') );
			redirect( "activities/attendance/" . $ids );
		}

		$this->template_data->set('ids', $ids);

		$idr = explode("-", $ids);
		$id = $idr[0];

		$activity = new $this->Activities_list_model;
		$activity->setId($id,true);
		$activity->set_select('*');
		$activity->set_select('MONTH(activity_date) as month');
		$activity->set_select('YEAR(activity_date) as year');
		$activity_data = $activity->get();
		$this->template_data->set('activity', $activity_data);

		$activities = new $this->Activities_list_model;
		$activities->set_where('MONTH(activity_date)', $activity_data->month);
		$activities->set_where('YEAR(activity_date)', $activity_data->year);
		$activities->set_where('benefeciaries', $activity_data->benefeciaries);
		$activities->set_order('activity_date', 'ASC');
		$activities->set_limit(0);
		$activities_data = $activities->populate();
		$this->template_data->set('activities', $activities_data);

		$this->load->view('activities/add_activity', $this->template_data->get_data());
	}

	public function attendance($ids, $start=0) {

		$this->template_data->set('ids', $ids);

		$idr = explode("-", $ids);
		$id = $idr[0];

		$activity = new $this->Activities_list_model;
		$activity->set_where_in('id', $idr);
		$activity->set_limit(0);
		$activities = $activity->populate();
		$this->template_data->set('activities', $activities);

		$benefeciaries = new $this->Benefeciaries_model('e');
		$benefeciaries->setCenterId($this->session->userdata('current_center_id'),true);
		$benefeciaries->set_select('e.*');
		$benefeciaries->set_join('names_info ni','ni.name_id=e.name_id');
		$benefeciaries->set_select('ni.lastname as lastname');
		$benefeciaries->set_select('ni.firstname as firstname');
		$benefeciaries->set_select('ni.middlename as middlename');

		foreach( $activities as $activity ) {
			$benefeciaries->set_select('(SELECT COUNT(*) FROM activities_attendance aa WHERE aa.activity_id='.$activity->id.' AND aa.name_id=e.name_id) as is_present_' . $activity->id);
		}
		
		$benefeciaries->set_order('ni.lastname', 'ASC');
		$benefeciaries->set_order('ni.firstname', 'ASC');
		$benefeciaries->set_start($start);
		//$benefeciaries->set_limit(0);
		$benefeciaries->setTrash(0,true);

		if( $this->input->get('filter') == 'present' ) {
			$benefeciaries->set_where('((SELECT COUNT(*) FROM activities_attendance aa WHERE aa.activity_id='.$id.' AND aa.name_id=e.name_id) = 1)');
		} elseif( $this->input->get('filter') == 'absent' ) {
			$benefeciaries->set_where('((SELECT COUNT(*) FROM activities_attendance aa WHERE aa.activity_id='.$id.' AND aa.name_id=e.name_id) = 0)');
		}

		if( $this->input->get('q') ) {
			$benefeciaries->set_where('(ni.lastname LIKE "%' . $this->input->get('q') . '%"', NULL, 99);
			$benefeciaries->set_where_or('ni.firstname LIKE "%' . $this->input->get('q') . '%"', NULL, 99);
			$benefeciaries->set_where_or('ni.middlename LIKE "%' . $this->input->get('q') . '%")', NULL, 99);
		}

		$this->template_data->set('benefeciaries', $benefeciaries->populate());
		$this->template_data->set('benefeciaries_count', $benefeciaries->count_all_results());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 4,
			'base_url' => base_url( $this->config->item('index_page') . "/activities/attendance/{$ids}"),
			'total_rows' => $benefeciaries->count_all_results(),
			'per_page' => $benefeciaries->get_limit(),
			'ajax'=>true
		)));

		$this->load->view('activities/activities_attendance', $this->template_data->get_data());
	}

	public function update_attendace($activity_id, $name_id, $output='') {
		$attendance = new $this->Activities_attendance_model();
		$attendance->setActivityId($activity_id, true);
		$attendance->setNameId($name_id, true);
		if( $attendance->nonEmpty() ) {
			$attendance->delete();
			if( $output == 'ajax') {
				$json = json_encode(array(
					'activity_id' => $activity_id,
					'name_id' => $name_id,
					'value' => 0,
				));
				$this->output->set_content_type('text/json')->set_output($json);
			}
		} else {
			$attendance->insert();
			if( $output == 'ajax') {
				$json = json_encode(array(
					'activity_id' => $activity_id,
					'name_id' => $name_id,
					'value' => 1,
				));
				$this->output->set_content_type('text/json')->set_output($json);
			}
		}
		if( $output != 'ajax') {
			redirect("activities/attendance/{$activity_id}");
		}
	}

}
