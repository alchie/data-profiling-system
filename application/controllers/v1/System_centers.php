<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class System_centers extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->template_data->set('current_page', lang_term('centers_title_plural', 'Companies'));
		$this->template_data->set('current_uri', 'system_centers');
		$this->template_data->set('navbar_search', false);

		$this->_isAuth('system', 'centers', 'view');

		if( !$this->config->item('multi_company') ) {
			redirect("welcome");
		}

	}

	public function index($start=0) {
		
		$centers = new $this->Centers_list_model;
		$centers->set_select("*");
		$centers->set_order('name', 'ASC');
		$centers->set_start($start);
		if( $this->input->get('filter') == 'trash') {
			$centers->setTrash('1',true);
		} else {
			$centers->setTrash('0',true);
		}
		$this->template_data->set('centers', $centers->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => base_url($this->config->item('index_page') . '/system_centers/index/'),
			'total_rows' => $centers->count_all_results(),
			'per_page' => $centers->get_limit(),
			'ajax'=>true,
		)));
		
		$this->load->view('system/centers/centers_list', $this->template_data->get_data());
	}

	public function add($output='') {

		$this->_isAuth('system', 'centers', 'add');

		if( $this->input->post() ) {
			$this->form_validation->set_rules('name', 'Center Name', 'trim|required');
			$this->form_validation->set_rules('address', 'Address', 'trim');
			$this->form_validation->set_rules('phone', 'phone', 'trim');
			$this->form_validation->set_rules('notes', 'Notes', 'trim');
			if( $this->form_validation->run() ) {
				$centers = new $this->Centers_list_model;
				$centers->setName($this->input->post('name'));
				$centers->setAddress($this->input->post('address'));
				$centers->setPhone($this->input->post('phone'));
				$centers->setNotes($this->input->post('notes'));
				$bootstrap_theme = ( isset($this->session->user_settings['theme']) && $this->session->user_settings['theme'] ) ? $this->session->user_settings['theme'] : 'yeti';
				$centers->setTheme($bootstrap_theme);
				if( $centers->insert() ) {
					$company_id = $centers->get_inserted_id();
					record_system_audit($this->session->userdata('user_id'), 'system', 'centers', 'add', $company_id, "Center Added: {$this->input->post('name')}", 0);
					redirect("system_centers");
				}
			}
		}

		$this->template_data->set('output', $output);
		$this->load->view('system/centers/centers_add', $this->template_data->get_data());
	}

	public function edit($id,$output='') {

		$this->_isAuth('system', 'centers', 'edit');

		$centers = new $this->Centers_list_model;
		$centers->setId($id,true);

		if( $centers->nonEmpty() ) {
			if( $this->input->post() ) { 
				$this->form_validation->set_rules('name', 'Center Name', 'trim|required');
				$this->form_validation->set_rules('address', 'Address', 'trim');
				$this->form_validation->set_rules('phone', 'phone', 'trim');
				$this->form_validation->set_rules('notes', 'Notes', 'trim');
				if( $this->form_validation->run() ) {
					$centers->setName($this->input->post('name'),false,true);
					$centers->setAddress($this->input->post('address'),false,true);
					$centers->setPhone($this->input->post('phone'),false,true);
					$centers->setNotes($this->input->post('notes'),false,true);
					$centers->setTheme($this->input->post('theme'),false,true);
					$centers->setDefault(($this->input->post('default')?1:0),false,true);
					if( $centers->update() ) {
						record_system_audit($this->session->userdata('user_id'), 'system', 'centers', 'edit', $id, "Center Updated: {$this->input->post('name')}", 0);
					}
				}
				if( $this->input->post('default') ) {
					$old_company = new $this->Centers_list_model;
					$old_company->setDefault(0,false,true);
					$old_company->set_where('default = 1');
					$old_company->set_where('id!=' . $id);
					$old_company->update();
				}
				$this->postNext();
			}
		}

		$centers->set_select("*");
		$this->template_data->set('company', $centers->get());

		$this->template_data->set('output', $output);
		$this->load->view('system/centers/centers_edit', $this->template_data->get_data());
	}

	public function delete($id) {
		
		$this->_isAuth('system', 'centers', 'delete');

		$centers = new $this->Centers_list_model;
		$centers->setId($id,true);
		$centers->setTrash('1',false,true);
		$centers->update();

		$this->getNext("system_centers");
	}

	public function deactivate($id) {
		
		$this->_isAuth('system', 'centers', 'delete');

		$centers = new $this->Centers_list_model;
		$centers->setId($id,true);
		$centers->setTrash('1',false,true);
		if( $centers->update() ) {
			record_system_audit($this->session->userdata('user_id'), 'system', 'centers', 'deactivate', $id, "Center Deactivated", 0);
		}

		$this->getNext("system_centers");
	}

	public function restore($id) {
		
		$this->_isAuth('system', 'centers', 'delete');

		$centers = new $this->Centers_list_model;
		$centers->setId($id,true);
		$centers->setTrash('0',false,true);
		if( $centers->update() ) {
			record_system_audit($this->session->userdata('user_id'), 'system', 'centers', 'restore', $id, "Center Restored", 0);
		}

		$this->getNext("system_centers");
	}

	private function _save_option($id, $name) {

		$options = new $this->Centers_options_model;
		$options->setCenterId($id,true);
		$options->setKey($name,true);

		if( $this->input->post() ) {
			if( $this->input->post($name) ) {
				if( $options->nonEmpty() ) {
					$options->setValue( serialize($this->input->post($name)), false,true);
					if( $options->update() ) {
						record_system_audit($this->session->userdata('user_id'), 'system', 'centers', '_save_option', $id, "Center Option Updated: {$name}", 0);
					}
				} else {
					$options->setValue( serialize( $this->input->post($name) ) );
					if( $options->insert() ) {
						record_system_audit($this->session->userdata('user_id'), 'system', 'centers', '_save_option', $id, "Center Option Inserted: {$name}", 0);
					}
				}
			} else {
				if( $options->nonEmpty() ) {
					if( $options->delete() ) {
						record_system_audit($this->session->userdata('user_id'), 'system', 'centers', '_save_option', $id, "Center Option Delete: {$name}", 0);
					}
				}
			}
		}

		$options->set_select("value");
		return $options->get();

	}

	public function print_css($id,$output='') {

		$this->_isAuth('system', 'centers', 'edit');

		$this->template_data->set('css', $this->_save_option($id, 'print_css'));
		$this->postNext();

		$this->template_data->set('output', $output);
		$this->load->view('system/centers/centers_print_css', $this->template_data->get_data());

	}

	public function print_group($id,$output='') {

		$this->_isAuth('system', 'centers', 'edit');

		$this->template_data->set('option', $this->_save_option($id, 'print_group'));
		$this->template_data->set('sort', $this->_save_option($id, 'print_group_sort'));
		$this->postNext();
		
		$print_groups = new $this->Terms_list_model;
		$print_groups->set_select("*");
		$print_groups->set_order('priority', 'ASC');
		$print_groups->set_order('name', 'ASC');
		$print_groups->set_limit(0);
		$print_groups->setTrash('0',true);
		$print_groups->setType('print_group',true);
		$this->template_data->set('print_groups', $print_groups->populate());

		$this->template_data->set('output', $output);
		$this->load->view('system/centers/centers_print_group', $this->template_data->get_data());
	}

	public function column_group($id,$output='') {

		$this->_isAuth('system', 'centers', 'edit');

		$this->template_data->set('column_group_employees', $this->_save_option($id, 'column_group_employees'));
		$this->template_data->set('column_group_dtr', $this->_save_option($id, 'column_group_dtr'));
		$this->template_data->set('column_group_salaries', $this->_save_option($id, 'column_group_salaries'));
		$this->template_data->set('column_group_earnings', $this->_save_option($id, 'column_group_earnings'));
		$this->template_data->set('column_group_benefits', $this->_save_option($id, 'column_group_benefits'));
		$this->template_data->set('column_group_deductions', $this->_save_option($id, 'column_group_deductions'));
		$this->template_data->set('column_group_summary', $this->_save_option($id, 'column_group_summary'));
		$this->template_data->set('sort', $this->_save_option($id, 'column_group_sort'));
		$this->postNext();
		
		$this->template_data->set('output', $output);
		$this->load->view('system/centers/centers_column_group', $this->template_data->get_data());
	}

}
