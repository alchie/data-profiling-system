<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Benefeciaries extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->template_data->set('current_page', 'Benefeciaries');
		$this->template_data->set('current_uri', 'benefeciaries');
		$this->template_data->set('navbar_search', true);

		$this->_isCompanyId();
		
		$this->_isAuth('benefeciaries', 'benefeciaries', 'view');

	}

	public function index($limit=10, $start=0) {

		$benefeciaries = new $this->Benefeciaries_model('e');
		if( $this->input->get('q') ) {
			$benefeciaries->set_where('(ni.lastname LIKE "%' . $this->input->get('q') . '%"', NULL, 99);
			$benefeciaries->set_where_or('ni.firstname LIKE "%' . $this->input->get('q') . '%"', NULL, 99);
			$benefeciaries->set_where_or('ni.middlename LIKE "%' . $this->input->get('q') . '%")', NULL, 99);
		}

		if( $this->input->get('filter') == 'trash' ) {
			$benefeciaries->setTrash(1,true);
		} else {
			$benefeciaries->setTrash(0,true);
		}

		$benefeciaries->setCenterId($this->session->userdata('current_center_id'),true);
		
		$benefeciaries->set_select('e.*');
		$benefeciaries->set_select('(SELECT name FROM benefeciaries_groups WHERE id=e.group_id) as group_name');
		$benefeciaries->set_select('(SELECT name FROM benefeciaries_areas WHERE id=e.area_id) as area_name');
		$benefeciaries->set_select('(SELECT name FROM terms_list WHERE id=e.status) as status_name');

		$benefeciaries->set_join('names_info ni','ni.name_id=e.name_id');
		$benefeciaries->set_select('ni.lastname as lastname');
		$benefeciaries->set_select('ni.firstname as firstname');
		$benefeciaries->set_select('ni.middlename as middlename');

		$benefeciaries->set_order('ni.lastname', 'ASC');
		$benefeciaries->set_order('ni.firstname', 'ASC');
		$benefeciaries->set_order('ni.middlename', 'ASC');

		$benefeciaries->set_start($start);
		$benefeciaries->set_limit($limit);

		$this->template_data->set('benefeciaries', $benefeciaries->populate());
		$this->template_data->set('benefeciaries_count', $benefeciaries->count_all_results());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 4,
			'base_url' => base_url( $this->config->item('index_page') . "/benefeciaries/index/{$limit}"),
			'total_rows' => $benefeciaries->count_all_results(),
			'per_page' => $benefeciaries->get_limit(),
			'ajax'=>false
		), "?filter=" . $this->input->get('filter') . "&back=" . $this->input->get('back') ));

		$services = new $this->Services_list_model;
		$services->setBenefeciaries(1,true);
		$services->setActive(1,true);
		$services->setTrash(0,true);
		$services->set_limit(0);
		$services->set_order('name');
		$this->template_data->set('services', $services->populate());

		if( $this->input->get('filter') == 'trash' ) {
			$this->load->view('benefeciaries/benefeciaries/benefeciaries_list_trash', $this->template_data->get_data());
		} else {
			$this->load->view('benefeciaries/benefeciaries/benefeciaries_list', $this->template_data->get_data());
		}
	}	

	public function height_weight_all($current_year=0, $month=0, $limit=10, $start=0) {

		$this->template_data->set('current_uri', 'benefeciaries_height_weight');
		$this->template_data->set('current_month', $month);
		$this->template_data->set('limit', $limit);
		$this->template_data->set('start', $start);

		$current_cgs = ($this->input->get('cgs')) ? $this->input->get('cgs') : 'weight_for_age';
		$this->template_data->set('current_cgs', $current_cgs);

		$current_year = ($current_year) ? $current_year : date('Y');
		$this->template_data->set('current_year', $current_year);

		$benefeciaries = new $this->Benefeciaries_model('e');
		if( $this->input->get('q') ) {
			$benefeciaries->set_where('(ni.lastname LIKE "%' . $this->input->get('q') . '%"', NULL, 99);
			$benefeciaries->set_where_or('ni.firstname LIKE "%' . $this->input->get('q') . '%"', NULL, 99);
			$benefeciaries->set_where_or('ni.middlename LIKE "%' . $this->input->get('q') . '%")', NULL, 99);
		}

		if( $this->input->get('filter') == 'trash' ) {
			$benefeciaries->setTrash(1,true);
		} else {
			$benefeciaries->setTrash(0,true);
		}

		//$benefeciaries->setCenterId($this->session->userdata('current_center_id'),true);
		
		$benefeciaries->set_select('e.*');
		$benefeciaries->set_select('(SELECT name FROM benefeciaries_groups WHERE id=e.group_id) as group_name');
		$benefeciaries->set_select('(SELECT name FROM benefeciaries_areas WHERE id=e.area_id) as area_name');
		$benefeciaries->set_select('(SELECT name FROM terms_list WHERE id=e.status) as status_name');

		$benefeciaries->set_join('names_info ni','ni.name_id=e.name_id');
		$benefeciaries->set_select('ni.lastname as lastname');
		$benefeciaries->set_select('ni.firstname as firstname');
		$benefeciaries->set_select('ni.middlename as middlename');
		$benefeciaries->set_select('ni.gender');
		$benefeciaries->set_select('ni.birthday');

		$benefeciaries->set_order('ni.lastname', 'ASC');
		$benefeciaries->set_start($start);
		$benefeciaries->set_limit($limit);

		if( $this->input->get('center') == 'all' ) {
			$benefeciaries->set_limit(0);
		} else {
			$benefeciaries->setCenterId($this->session->userdata('current_center_id'),true);
		}

		for($i=1;$i<=12;$i++) {

			if( ($month) && ($month<>$i) ) {
			  continue;
			}

			$month1 = new $this->Benefeciaries_hw_model('hw');
			$month1->set_select('hw.id');
			$month1->set_limit(1);
			$month1->set_order('hw.date_hw','DESC');
			$month1->set_where("(MONTH(hw.date_hw)={$i})");
			$month1->set_where("(YEAR(hw.date_hw)={$current_year})");
			$month1->set_where("(hw.name_id=e.name_id)");
			$benefeciaries->set_select('('.$month1->get_compiled_select().') as month_'.$i);

			if($month) {
				switch($this->input->get('show')) {
					case 'with_hw':
						$benefeciaries->set_where('(('.$month1->get_compiled_select().') IS NOT NULL)');
					break;
					case 'without_hw':
						$benefeciaries->set_where('(('.$month1->get_compiled_select().') IS NULL)');
					break;
					default:
					break;
				}
				//if( $this->input->get('output') == 'print') {
					$month1->set_select('hw.height', NULL, TRUE);
					$benefeciaries->set_select('('.$month1->get_compiled_select().') as height');

					$month1->set_select('hw.weight', NULL, TRUE);
					$benefeciaries->set_select('('.$month1->get_compiled_select().') as weight');
				//}
			}			
		}

		$this->template_data->set('benefeciaries', $benefeciaries->populate());
		$this->template_data->set('benefeciaries_count', $benefeciaries->count_all_results());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 6,
			'base_url' => base_url( $this->config->item('index_page') . "/benefeciaries/height_weight_all/{$current_year}/{$month}/{$limit}"),
			'total_rows' => $benefeciaries->count_all_results(),
			'per_page' => $benefeciaries->get_limit(),
			'ajax'=>true
		), '?show=' . $this->input->get('show')));

		$services = new $this->Services_list_model;
		$services->setBenefeciaries(1,true);
		$services->setActive(1,true);
		$services->setTrash(0,true);
		$services->set_limit(0);
		$services->set_order('name');
		$this->template_data->set('services', $services->populate());

		$years = new $this->Benefeciaries_hw_model();
		$years->set_select("YEAR(date_hw) as year");
		$years->set_group_by("YEAR(date_hw)");
		$years->set_order("YEAR(date_hw)", "DESC");
		$this->template_data->set('years', $years->populate());

		if( $this->input->get('output') == 'print') {
			$this->load->view('benefeciaries/benefeciaries/height_weight_print/' . $current_cgs, $this->template_data->get_data());
		} else {
			$this->load->view('benefeciaries/benefeciaries/benefeciaries_height_weight', $this->template_data->get_data());
		}

	}

	public function monthly_attendance($current_year=0, $month=0, $limit=10, $start=0) {

		$month = (!$month) ? date('m') : $month;
		
		$this->template_data->set('current_uri', 'benefeciaries_attendance');
		$this->template_data->set('current_month', $month);
		$this->template_data->set('limit', $limit);
		$this->template_data->set('start', $start);

		

		$current_cgs = ($this->input->get('cgs')) ? $this->input->get('cgs') : 'weight_for_age';
		$this->template_data->set('current_cgs', $current_cgs);

		$current_year = ($current_year) ? $current_year : date('Y');
		$this->template_data->set('current_year', $current_year);

		$benefeciaries = new $this->Benefeciaries_model('e');
		if( $this->input->get('q') ) {
			$benefeciaries->set_where('(ni.lastname LIKE "%' . $this->input->get('q') . '%"', NULL, 99);
			$benefeciaries->set_where_or('ni.firstname LIKE "%' . $this->input->get('q') . '%"', NULL, 99);
			$benefeciaries->set_where_or('ni.middlename LIKE "%' . $this->input->get('q') . '%")', NULL, 99);
		}

		if( $this->input->get('filter') == 'trash' ) {
			$benefeciaries->setTrash(1,true);
		} else {
			$benefeciaries->setTrash(0,true);
		}

		//$benefeciaries->setCenterId($this->session->userdata('current_center_id'),true);
		
		$benefeciaries->set_select('e.*');
		$benefeciaries->set_select('(SELECT name FROM benefeciaries_groups WHERE id=e.group_id) as group_name');
		$benefeciaries->set_select('(SELECT name FROM benefeciaries_areas WHERE id=e.area_id) as area_name');
		$benefeciaries->set_select('(SELECT name FROM terms_list WHERE id=e.status) as status_name');

		$benefeciaries->set_join('names_info ni','ni.name_id=e.name_id');
		$benefeciaries->set_select('ni.lastname as lastname');
		$benefeciaries->set_select('ni.firstname as firstname');
		$benefeciaries->set_select('ni.middlename as middlename');
		$benefeciaries->set_select('ni.gender');
		$benefeciaries->set_select('ni.birthday');

		$benefeciaries->set_order('ni.lastname', 'ASC');
		$benefeciaries->set_start($start);
		$benefeciaries->set_limit($limit);

		if( $this->input->get('center') == 'all' ) {
			$benefeciaries->set_limit(0);
		} else {
			$benefeciaries->setCenterId($this->session->userdata('current_center_id'),true);
		}

		for($i=1;$i<=12;$i++) {

			if( ($month) && ($month<>$i) ) {
			  continue;
			}

			$month2 = new $this->Activities_attendance_model('aa');
			$month2->set_join('activities_list al', 'al.id=aa.activity_id');
			$month2->set_select('COUNT(*)');
			$month2->set_where("(MONTH(al.activity_date)={$i})");
			$month2->set_where("(YEAR(al.activity_date)={$current_year})");
			$month2->set_where("(aa.name_id=e.name_id)");
			$benefeciaries->set_select('('.$month2->get_compiled_select().') as month_'.$i);
		
		}

		$this->template_data->set('benefeciaries', $benefeciaries->populate());
		$this->template_data->set('benefeciaries_count', $benefeciaries->count_all_results());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 6,
			'base_url' => base_url( $this->config->item('index_page') . "/benefeciaries/monthly_attendance/{$current_year}/{$month}/{$limit}"),
			'total_rows' => $benefeciaries->count_all_results(),
			'per_page' => $benefeciaries->get_limit(),
			'ajax'=>true
		), '?show=' . $this->input->get('show')));

		$services = new $this->Services_list_model;
		$services->setBenefeciaries(1,true);
		$services->setActive(1,true);
		$services->setTrash(0,true);
		$services->set_limit(0);
		$services->set_order('name');
		$this->template_data->set('services', $services->populate());

		$years = new $this->Activities_attendance_model('aa');
		$years->set_join('activities_list al', 'al.id=aa.activity_id');
		$years->set_select("YEAR(al.activity_date) as year");
		$years->set_group_by("YEAR(al.activity_date)");
		$years->set_order("YEAR(al.activity_date)", "DESC");
		$this->template_data->set('years', $years->populate());

		$this->load->view('benefeciaries/benefeciaries/benefeciaries_monthly_attendance', $this->template_data->get_data());
	}
	
	public function download_height_weight_template($current_year=0, $current_month=0, $output="") {

		$benefeciaries = new $this->Benefeciaries_model('e');
		$benefeciaries->setTrash(0,true);
		$benefeciaries->setCenterId($this->session->userdata('current_center_id'),true);
		$benefeciaries->set_select('e.*');
		$benefeciaries->set_join('names_info ni','ni.name_id=e.name_id');
		$benefeciaries->set_select('ni.lastname as lastname');
		$benefeciaries->set_select('ni.firstname as firstname');
		$benefeciaries->set_limit(0);
		$benefeciaries->set_order('ni.lastname', 'ASC');
		$benefeciaries->set_order('ni.firstname', 'ASC');

		$csv = "ID,NAME,DATE,HEIGHT,WEIGHT,NOTES\n";

		foreach( $benefeciaries->populate() as $ben ) {
			$csv .= "{$ben->name_id},\"{$ben->lastname}, {$ben->firstname}\",{$current_month}/1/{$current_year},\"\",\"\",\"\"\n";
		}		

		$this->load->helper('download');

		force_download("height_weight_template_{$current_month}_{$current_year}.csv", $csv);

	}

	public function import_height_weight($current_year=0, $current_month=0, $output="") {

				$this->template_data->set('current_uri', 'benefeciaries_height_weight');
				$this->template_data->set('current_year', $current_year);
				$this->template_data->set('current_month', $current_month);
				$this->template_data->set('output', $output);

				$config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'csv';
                $config['max_size']             = 100;
                
                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('userfile') )
                {
                		if( $this->upload->display_errors() ) {
                        	$this->template_data->set('upload_error', $this->upload->display_errors());
                        }
                        
                        $this->load->view('benefeciaries/benefeciaries/benefeciaries_import_height_weight', $this->template_data->get_data());
                }
                else
                {
                        $data = $this->upload->data();

                        $file = fopen($data['full_path'],"r");
                        $csv_data = array();
                        while($row = fgetcsv($file)) {

                        	$name_id = (isset($row[0])) ? $row[0] : false;
                        	$date_hw = (isset($row[2])) ? $row[2] : false;
                        	$height = (isset($row[3])) ? $row[3] : false;
                        	$weight = (isset($row[4])) ? $row[4] : false;
                        	$notes = (isset($row[5])) ? $row[5] : false;

                        	if( ($name_id) && ($date_hw) ) {
	                        	$hw = new $this->Benefeciaries_hw_model('hw');
								$hw->setNameId($name_id,true);
								$hw->setDateHw(date('Y-m-d', strtotime($date_hw)),true);
								$hw->setHeight($height);
								$hw->setWeight($weight);
								$hw->setNotes($notes);
								if( !$hw->nonEmpty() ) {
									$hw->insert();
								}
							}
                        }

						fclose($file);
						redirect( $this->input->get('next') );
                }

	}

	public function service($service_id, $start=0) {

		$this->template_data->set('current_uri', 'service_' . $service_id);
		$this->template_data->set('service_id', $service_id);
		
		$service = new $this->Services_list_model;
		$service->setId($service_id,true);
		$this->template_data->set('current_service', $service->get());

		$benefeciaries = new $this->Benefeciaries_model('e');
		if( $this->input->get('q') ) {
			$benefeciaries->set_where('(ni.lastname LIKE "%' . $this->input->get('q') . '%"', NULL, 99);
			$benefeciaries->set_where_or('ni.firstname LIKE "%' . $this->input->get('q') . '%"', NULL, 99);
			$benefeciaries->set_where_or('ni.middlename LIKE "%' . $this->input->get('q') . '%")', NULL, 99);
		}

		if( $this->input->get('filter') == 'trash' ) {
			$benefeciaries->setTrash(1,true);
		} else {
			$benefeciaries->setTrash(0,true);
		}

		$benefeciaries->setCenterId($this->session->userdata('current_center_id'),true);
		
		$benefeciaries->set_select('e.*');
		$benefeciaries->set_select('(SELECT name FROM benefeciaries_groups WHERE id=e.group_id) as group_name');
		$benefeciaries->set_select('(SELECT name FROM benefeciaries_areas WHERE id=e.area_id) as area_name');
		$benefeciaries->set_select('(SELECT name FROM terms_list WHERE id=e.status) as status_name');

		$benefeciaries->set_join('names_info ni','ni.name_id=e.name_id');
		$benefeciaries->set_select('ni.lastname as lastname');
		$benefeciaries->set_select('ni.firstname as firstname');
		$benefeciaries->set_select('ni.middlename as middlename');

		$benefeciaries->set_order('ni.lastname', 'ASC');
		$benefeciaries->set_start($start);
		$benefeciaries->set_where('( (SELECT COUNT(*) FROM benefeciaries_services bs WHERE bs.service_id='.$service_id.' AND bs.name_id=e.name_id) > 0 )');

		$this->template_data->set('benefeciaries', $benefeciaries->populate());
		$this->template_data->set('benefeciaries_count', $benefeciaries->count_all_results());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 3,
			'base_url' => base_url( $this->config->item('index_page') . "/benefeciaries/index"),
			'total_rows' => $benefeciaries->count_all_results(),
			'per_page' => $benefeciaries->get_limit(),
			'ajax'=>true
		)));

		$services = new $this->Services_list_model;
		$services->setBenefeciaries(1,true);
		$services->setActive(1,true);
		$services->setTrash(0,true);
		$services->set_limit(0);
		$services->set_order('name');
		$this->template_data->set('services', $services->populate());
		
		if( $this->input->get('filter') == 'trash' ) {
			$this->load->view('benefeciaries/benefeciaries/benefeciaries_list_trash', $this->template_data->get_data());
		} else {
			$this->load->view('benefeciaries/benefeciaries/benefeciaries_list', $this->template_data->get_data());
		}
	}

	public function group($id, $start=0) {

		$groups = new $this->Benefeciaries_groups_model;
		$groups->setId($id,true);
		$this->template_data->set('group', $groups->get());

		$benefeciaries = new $this->Benefeciaries_model('e');
		if( $this->input->get('q') ) {
			$benefeciaries->set_where('(ni.lastname LIKE "%' . $this->input->get('q') . '%"', NULL, 99);
			$benefeciaries->set_where_or('ni.firstname LIKE "%' . $this->input->get('q') . '%"', NULL, 99);
			$benefeciaries->set_where_or('ni.middlename LIKE "%' . $this->input->get('q') . '%")', NULL, 99);
		}

		if( $this->input->get('filter') == 'trash' ) {
			$benefeciaries->setTrash(1,true);
		} else {
			$benefeciaries->setTrash(0,true);
		}

		$benefeciaries->setCenterId($this->session->userdata('current_center_id'),true);
		$benefeciaries->setGroupId($id,true);
		$benefeciaries->set_select('e.*');
		$benefeciaries->set_select('(SELECT name FROM benefeciaries_groups WHERE id=e.group_id) as group_name');
		$benefeciaries->set_select('(SELECT name FROM benefeciaries_areas WHERE id=e.area_id) as area_name');
		$benefeciaries->set_select('(SELECT name FROM terms_list WHERE id=e.status) as status_name');
		
		$benefeciaries->set_join('names_info ni','ni.name_id=e.name_id');
		$benefeciaries->set_select('ni.lastname as lastname');
		$benefeciaries->set_select('ni.firstname as firstname');
		$benefeciaries->set_select('ni.middlename as middlename');

		$benefeciaries->set_order('ni.lastname', 'ASC');

		$benefeciaries->set_start($start);
		$this->template_data->set('benefeciaries', $benefeciaries->populate());
		$this->template_data->set('benefeciaries_count', $benefeciaries->count_all_results());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 4,
			'base_url' => base_url($this->config->item('index_page') . "/benefeciaries/group/{$id}"),
			'total_rows' => $benefeciaries->count_all_results(),
			'per_page' => $benefeciaries->get_limit(),
			'ajax'=>true
		)));

		$this->load->view('benefeciaries/benefeciaries/benefeciaries_list', $this->template_data->get_data());
	}

	public function area($id, $start=0) {

		$area = new $this->Benefeciaries_areas_model;
		$area->setId($id,true);
		$this->template_data->set('area', $area->get());

		$benefeciaries = new $this->Benefeciaries_model('e');
		if( $this->input->get('q') ) {
			$benefeciaries->set_where('(lastname LIKE "%' . $this->input->get('q') . '%"', NULL, 99);
			$benefeciaries->set_where_or('firstname LIKE "%' . $this->input->get('q') . '%"', NULL, 99);
			$benefeciaries->set_where_or('middlename LIKE "%' . $this->input->get('q') . '%")', NULL, 99);
		}
		$benefeciaries->setCenterId($this->session->userdata('current_center_id'),true);
		$benefeciaries->setAreaId($id,true);
		$benefeciaries->set_select('e.*');
		$benefeciaries->set_select('(SELECT name FROM benefeciaries_groups WHERE id=e.group_id) as group_name');
		$benefeciaries->set_select('(SELECT name FROM benefeciaries_areas WHERE id=e.area_id) as area_name');
		$benefeciaries->set_select('(SELECT name FROM terms_list WHERE id=e.status) as status_name');
		$benefeciaries->set_join('names_info ni','ni.name_id=e.name_id');
		$benefeciaries->set_select('ni.lastname as lastname');
		$benefeciaries->set_select('ni.firstname as firstname');
		$benefeciaries->set_select('ni.middlename as middlename');
		$benefeciaries->set_order('ni.lastname', 'ASC');
		$benefeciaries->set_start($start);
		$this->template_data->set('benefeciaries', $benefeciaries->populate());
		$this->template_data->set('benefeciaries_count', $benefeciaries->count_all_results());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 4,
			'base_url' => base_url($this->config->item('index_page') . "/benefeciaries/area/{$id}"),
			'total_rows' => $benefeciaries->count_all_results(),
			'per_page' => $benefeciaries->get_limit(),
			'ajax'=>true
		)));

		$this->load->view('benefeciaries/benefeciaries/benefeciaries_list', $this->template_data->get_data());
	}

	public function add($id, $output='') {
		$this->_isAuth('benefeciaries', 'benefeciaries', 'add');

		$this->template_data->set('output', $output);

		$names = new $this->Names_list_model;
		$names->setId($id, true);
		$names->setTrash(0,true);
		$names->setTypeBenefeciary(1,true);
		$names->set_join('names_info', 'names_info.name_id=names_list.id');
		$name_data = $names->get();
		$this->template_data->set('name', $name_data);

		if( $names->nonEmpty() ) {
			if( $this->input->post('action') == 'add' ) {
				$benefeciary = new $this->Benefeciaries_model;
				$benefeciary->setNameId($id);
				$benefeciary->setGroupId($this->input->post('group_id'));
				$benefeciary->setAreaId($this->input->post('area_id'));
				$benefeciary->setCenterId($this->session->userdata('current_center_id'));
				$benefeciary->setTrash(0);
				if( $benefeciary->insert() ) {
					record_system_audit($this->session->userdata('user_id'), 'benefeciaries', 'benefeciaries', 'add', $this->session->userdata('current_center_id'), "Benefeciary Added: {$name_data->full_name}", $id);
				}
				$this->postNext();
			}
		}


		$groups = new $this->Benefeciaries_groups_model;
		$groups->setCenterId($this->session->userdata('current_center_id'),true);
		$groups->set_limit(0);
		$groups->set_order('name', 'ASC');
		$this->template_data->set('groups', $groups->populate());

		$areas = new $this->Benefeciaries_areas_model;
		$areas->setCenterId($this->session->userdata('current_center_id'),true);
		$areas->set_limit(0);
		$areas->set_order('name', 'ASC');
		$this->template_data->set('areas', $areas->populate());

		$this->load->view('benefeciaries/benefeciaries/benefeciaries_add', $this->template_data->get_data());

	}

	public function add_multiple($output='0', $start=0) {
		$this->_isAuth('benefeciaries', 'benefeciaries', 'add');

		if( $this->input->post('benefeciary') ) {
			foreach($this->input->post('benefeciary') as $name_id => $benefeciary) {
				if( isset($benefeciary['add']) ) {
					$benefeciary1 = new $this->Benefeciaries_model;
					$benefeciary1->setNameId( $name_id, true);
					$benefeciary1->setGroupId( $benefeciary['group_id'] );
					$benefeciary1->setAreaId( $benefeciary['area_id'] );
					$benefeciary1->setCenterId( $this->session->userdata('current_center_id'), true);
					$benefeciary1->setTrash(0);
					if( $benefeciary1->nonEmpty()===FALSE ) {
						if( $benefeciary1->insert() ) {
							record_system_audit($this->session->userdata('user_id'), 'benefeciaries', 'benefeciaries', 'add_multiple', $this->session->userdata('current_center_id'), "Added Multiple Benefeciaries");
						}
					}
					
				}
			}
			$this->postNext();
		}

		$names = new $this->Names_list_model;
		$names->set_start($start);
		$names->set_limit(10);
		$names->setTrash(0,true);
		$names->set_order('names_list.full_name', 'ASC');
		$names->set_where('((SELECT COUNT(*) FROM `benefeciaries` WHERE name_id=names_list.id) = 0)');
		$names->set_where('((SELECT COUNT(*) FROM `names_info` WHERE name_id=names_list.id) = 1)');

		$this->template_data->set('names', $names->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 4,
			'base_url' => base_url($this->config->item('index_page') . "/benefeciaries/add_multiple/{$output}"),
			'total_rows' => $names->count_all_results(),
			'per_page' => $names->get_limit()
		), '?next=' . $this->input->get('next') ));

		$groups = new $this->Benefeciaries_groups_model;
		$groups->setCenterId($this->session->userdata('current_center_id'),true);
		$groups->set_limit(0);
		$groups->set_order('name', 'ASC');
		$this->template_data->set('groups', $groups->populate());

		$areas = new $this->Benefeciaries_areas_model;
		$areas->setCenterId($this->session->userdata('current_center_id'),true);
		$areas->set_limit(0);
		$areas->set_order('name', 'ASC');
		$this->template_data->set('areas', $areas->populate());

		$this->template_data->set('output', $output);
		$this->load->view('benefeciaries/benefeciaries/benefeciaries_add_multiple', $this->template_data->get_data());

	}

	public function search_name($output='', $start=0) {

		$this->_isAuth('benefeciaries', 'benefeciaries', 'add');

		$names = new $this->Names_list_model;
		$names->set_start($start);
		$names->set_limit(5);
		$names->setTrash(0,true);
		$names->setTypeBenefeciary(1,true);
		$names->set_order('names_list.full_name', 'ASC');
		$names->set_where('((SELECT COUNT(*) FROM `benefeciaries` WHERE name_id=names_list.id) = 0)');
		$names->set_where('((SELECT COUNT(*) FROM `names_info` WHERE name_id=names_list.id) = 1)');
		
		
		if( $output == 'autocomplete') {
			
			if( $this->input->get('term') ) {
				$names->set_where('names_list.full_name LIKE "%' . $this->input->get('term') . '%"', NULL, 99);
			}

			$results = array();

			foreach($names->populate() as $name) {
					$results[] = array(
						'label' => $name->full_name,
						'id' => $name->id,
						'redirect'=> site_url( "benefeciaries/add/{$name->id}" ) . "?next=benefeciaries/search_name",
						'innerBody'=> site_url( "benefeciaries/add/{$name->id}/ajax" ) . "?next=benefeciaries",
						);
				}

			$this->output->set_content_type('application/json')->set_output(json_encode( $results ));

		} else {

			$this->template_data->set('names', $names->populate());

			$this->template_data->set('pagination', bootstrap_pagination(array(
				'uri_segment' => 4,
				'base_url' => base_url($this->config->item('index_page') . "/benefeciaries/search_name/{$output}"),
				'total_rows' => $names->count_all_results(),
				'per_page' => $names->get_limit(),
				'attributes' => array(
					'class' => 'btn btn-default ajax-modal-inner',
					'data-hide_footer' => 1
					)
			), '?next=' . $this->input->get('next') ));
			
			$this->template_data->set('output', $output);
			$this->load->view('benefeciaries/benefeciaries/benefeciaries_add_search', $this->template_data->get_data());
		}
		
	}

	public function edit_personal($id,$output='') {
		$this->_isAuth('benefeciaries', 'benefeciaries', 'edit');

		$benefeciary = new $this->Benefeciaries_model;
		$benefeciary->setNameId($id,true);
		$benefeciary_data = $benefeciary->get();

		if( $benefeciary->nonEmpty() ) {
			if( $this->input->post() ) {
				$this->form_validation->set_rules('lastname', 'Last Name', 'trim|required');
				$this->form_validation->set_rules('firstname', 'First Name', 'trim|required');
				$this->form_validation->set_rules('middlename', 'Middle Name', 'trim');
				if( $this->form_validation->run() ) {
					$benefeciary->setLastname($this->input->post('lastname'),false,true);
					$benefeciary->setFirstname($this->input->post('firstname'),false,true);
					$benefeciary->setMiddlename($this->input->post('middlename'),false,true);
					$benefeciary->setBirthday( date("Y-m-d", strtotime($this->input->post('birthday'))),false,true);
					$benefeciary->setBirthplace($this->input->post('birthplace'),false,true);
					$benefeciary->setGender($this->input->post('gender'),false,true);
					$benefeciary->setCivilStatus($this->input->post('civil_status'),false,true);
					if( $benefeciary->update() ) {
						record_system_audit($this->session->userdata('user_id'), 'benefeciaries', 'benefeciaries', 'edit', $this->session->userdata('current_center_id'), "Edit Personal Info: {$benefeciary_data->lastname}, {$benefeciary_data->firstname}", $id);
					}
				}
				$this->postNext();
			}
		}

		$this->template_data->set('benefeciary', $benefeciary->get());

		$this->template_data->set('output', $output);
		$this->load->view('benefeciaries/benefeciaries/benefeciaries_edit_personal', $this->template_data->get_data());
	}

	public function edit_benefeciary($id,$output='') {
		$this->_isAuth('benefeciaries', 'benefeciaries', 'edit');

		$benefeciary = new $this->Benefeciaries_model;
		$benefeciary->setNameId($id,true,false);

		if( $benefeciary->nonEmpty() ) {
			if( $this->input->post() ) {
				$this->form_validation->set_rules('center_id', 'Center', 'trim');
				$this->form_validation->set_rules('group_id', 'Group', 'trim');
				$this->form_validation->set_rules('area_id', 'Area', 'trim');
				$this->form_validation->set_rules('date_accepted', 'Accepted', 'trim');
				$this->form_validation->set_rules('status', 'Status', 'trim');
				$this->form_validation->set_rules('notes', 'Notes', 'trim');
				if( $this->form_validation->run() ) {
					$benefeciary->setCenterId($this->input->post('center_id'),false,true);
					$benefeciary->setGroupId($this->input->post('group_id'),false,true);
					$benefeciary->setAreaId($this->input->post('area_id'),false,true);
					$benefeciary->setAccepted( date('Y-m-d', strtotime($this->input->post('date_accepted'))),false,true);
					$benefeciary->setStatus($this->input->post('status'),false,true);
					$benefeciary->setNotes($this->input->post('notes'),false,true);
					$benefeciary->setBenefeciaryId($this->input->post('benefeciary_id'),false,true);
					if( $benefeciary->update() ) {
						record_system_audit($this->session->userdata('user_id'), 'benefeciaries', 'benefeciaries', 'edit', $this->session->userdata('current_center_id'), "Edit Employment", $id);
					}

					if( $this->input->post('benefeciary_service') ) {
						$services = new $this->Benefeciaries_services_model;
						$services->setNameId($id,true,true);
						$services->delete();
						foreach( $this->input->post('benefeciary_service') as $service_id ) {
							$services->setServiceId($service_id,false,true);
							$services->insert();
						}
					}
				}
				$this->postNext("active=benefeciary");
			}
		}
		$benefeciary_data = $benefeciary->get();
		$this->template_data->set('benefeciary', $benefeciary_data);

		$center_id = ($benefeciary_data->center_id > 0) ? $benefeciary_data->center_id : $this->session->userdata('current_center_id');

		$groups = new $this->Benefeciaries_groups_model;
		$groups->setCenterId($center_id,true);
		$groups->set_limit(0);
		$groups->set_order('name', 'ASC');
		$this->template_data->set('groups', $groups->populate());

		$areas = new $this->Benefeciaries_areas_model;
		$areas->setCenterId($center_id,true);
		$areas->set_limit(0);
		$areas->set_order('name', 'ASC');
		$this->template_data->set('areas', $areas->populate());

		$centers = new $this->Centers_list_model;
		$centers->setTrash(0,true);
		$centers->set_order('name', 'ASC');
		$centers->set_limit(0);
		$this->template_data->set('centers', $centers->populate());

		$terms = new $this->Terms_list_model;
		$terms->set_select("*");
		$terms->set_order('priority', 'ASC');
		$terms->set_order('name', 'ASC');
		$terms->set_start(0);
		$terms->setTrash('0',true);
		$terms->setType('benefeciary_status',true);
		$this->template_data->set('benefeciary_status', $terms->populate());

		$services = new $this->Services_list_model('s');
		$services->setBenefeciaries(1,true);
		$services->setActive(1,true);
		$services->setTrash(0,true);
		$services->set_limit(0);
		$services->set_order('s.name');
		$services->set_select('s.*');
		$services->set_select('(SELECT COUNT(*) FROM benefeciaries_services bs WHERE bs.name_id='.$id.' AND bs.service_id=s.id) as checked');
		$this->template_data->set('services', $services->populate());

		$this->template_data->set('output', $output);
		$this->load->view('benefeciaries/benefeciaries/benefeciaries_edit_benefeciary', $this->template_data->get_data());
	}

	public function edit_address($id,$output='') {

		$this->_isAuth('benefeciaries', 'benefeciaries', 'edit');

		$benefeciary = new $this->Benefeciaries_contacts_model;
		$benefeciary->setNameId($id,true);
		$benefeciary_data = $benefeciary->get();

		if( $this->input->post() ) {
			$this->form_validation->set_rules('phone_number', 'Phone Number', 'trim');
			$this->form_validation->set_rules('cell_number', 'Cellphone Number', 'trim');
			$this->form_validation->set_rules('address', 'Address', 'trim');
			if( $this->form_validation->run() ) {
				$benefeciary->setPhoneNumber($this->input->post('phone_number'),false,true);
				$benefeciary->setCellNumber($this->input->post('cell_number'),false,true);
				$benefeciary->setAddress($this->input->post('address'),false,true);
				if($benefeciary->nonEmpty()) {
					if( $benefeciary->update() ) {
						record_system_audit($this->session->userdata('user_id'), 'benefeciaries', 'benefeciaries', 'edit', $this->session->userdata('current_center_id'), "Edit Address: {$benefeciary_data->full_name}", $id);
					}
				} else {
					$benefeciary->setNameId($id,true,true);
					if( $benefeciary->insert() ) {
						record_system_audit($this->session->userdata('user_id'), 'benefeciaries', 'benefeciaries', 'edit', $this->session->userdata('current_center_id'), "Added Address: {$benefeciary_data->full_name}", $id);
					}
				}
			}
			$this->postNext();
		}
		

		$this->template_data->set('benefeciary', $benefeciary->get());

		$this->template_data->set('output', $output);
		$this->load->view('benefeciaries/benefeciaries/benefeciaries_edit_address', $this->template_data->get_data());
	}

	public function restore($id) {
		$this->_isAuth('benefeciaries', 'benefeciaries', 'delete');

		$benefeciary = new $this->Benefeciaries_model;
		$benefeciary->setNameId($id,true,false);
		$benefeciary->setTrash(0,false,true);
		if( $benefeciary->update() ) {
			record_system_audit($this->session->userdata('user_id'), 'benefeciaries', 'benefeciaries', 'restore', $this->session->userdata('current_center_id'), "Benefeciary Restored", $id);
		}
		
		$this->getNext("benefeciaries");
	}

	public function delete($id) {
		$this->_isAuth('benefeciaries', 'benefeciaries', 'delete');

		$benefeciary = new $this->Benefeciaries_model;
		$benefeciary->setNameId($id,true);
		$benefeciary->setTrash(1,true);
		if( $benefeciary->delete() ) {
			record_system_audit($this->session->userdata('user_id'), 'benefeciaries', 'benefeciaries', 'delete', $this->session->userdata('current_center_id'), "Benefeciary Deactivated", $id);
		}
		
		$this->getNext("benefeciaries");
	}

	public function deactivate($id) {
		$this->_isAuth('benefeciaries', 'benefeciaries', 'delete');

		$benefeciary = new $this->Benefeciaries_model;
		$benefeciary->setNameId($id,true,false);
		$benefeciary->setTrash(1,false,true);
		if( $benefeciary->update() ) {
			record_system_audit($this->session->userdata('user_id'), 'benefeciaries', 'benefeciaries', 'deactivate', $this->session->userdata('current_center_id'), "Benefeciary Deactivated", $id);
		}
		
		$this->getNext("benefeciaries");
	}

	public function config($id, $output='') {

		$benefeciary = new $this->Benefeciaries_model;
		$benefeciary->setNameId($id,true);
		$this->template_data->set('benefeciary', $benefeciary->get());
		
		$this->template_data->set('output', $output);

		$this->load->view('benefeciaries/benefeciaries/benefeciaries_config', $this->template_data->get_data());

	}

	public function edit_leave_benefits($id,$selected_year=NULL,$output='') {

		$this->template_data->set('selected_year', $selected_year);

		$benefeciary = new $this->Benefeciaries_model;
		$benefeciary->setNameId($id,true);
		$benefeciary_data = $benefeciary->get();
		$this->template_data->set('benefeciary', $benefeciary_data);

		if( $selected_year ) {
			if( $this->input->post('leave') ) {
				foreach($this->input->post('leave') as $benefit_id=>$days) {
					$leave_b = new $this->Benefeciaries_leave_benefits_model('elb');
					$leave_b->setCenterId($benefeciary_data->center_id,true);
					$leave_b->setNameId($benefeciary_data->name_id,true);
					$leave_b->setBenefitId($benefit_id,true);
					$leave_b->setYear($selected_year,true);

					if( $leave_b->nonEmpty() ) {
						$leave_b->setDays($days,false,true);
						if( $leave_b->update() ) {
							record_system_audit($this->session->userdata('user_id'), 'benefeciaries', 'benefeciaries', 'edit_leave_benefits', $this->session->userdata('current_center_id'), "Leave Benefits Updated", $id);
						}
					} else {
						$leave_b->setDays($days);
						if( $leave_b->insert() ) {
							record_system_audit($this->session->userdata('user_id'), 'benefeciaries', 'benefeciaries', 'edit_leave_benefits', $this->session->userdata('current_center_id'), "Leave Benefits Inserted", $id);
						}
					}
				}
				$this->postNext();
			}
			
			$leave = new $this->Benefits_list_model('b');
			$leave->setLeave(1,true);
			$leave->setTrash(0,true);
			$leave->set_select("*");
			$leave->set_select("(SELECT elb.days FROM benefeciaries_leave_benefits elb WHERE elb.name_id={$benefeciary_data->name_id} AND elb.center_id={$benefeciary_data->center_id} AND b.id=elb.benefit_id AND elb.year='{$selected_year}' LIMIT 1) as days");
			$this->template_data->set('leaves', $leave->populate());
		}

		$payroll_years = new $this->Payroll_model;
		$payroll_years->setCenterId($this->session->userdata('current_center_id'),true);
		$payroll_years->set_select('year');
		$payroll_years->set_group_by('year');
		$payroll_years->set_order('year', 'DESC');
		$payroll_years->set_limit(0);
		$this->template_data->set('payroll_years', $payroll_years->populate());

		$this->template_data->set('output', $output);
		$this->load->view('benefeciaries/benefeciaries/benefeciaries_edit_leave_benefits', $this->template_data->get_data());
	}

	private function _employee_columns($benefeciaries, $columns) {
					
					$columns = ($columns) ? $columns : array();
					// personal info
					$benefeciaries->set_select('ni.*');
					if( in_array('age', $columns)) {
						$benefeciaries->set_select("(TIMESTAMPDIFF(YEAR, ni.birthday, CURDATE())) as age");
					}

					// employment info
					if( in_array('emp_group', $columns)) {
						$benefeciaries->set_select('(SELECT name FROM benefeciaries_groups WHERE id=e.group_id) as group_name');
					}
					if( in_array('emp_area', $columns)) {
						$benefeciaries->set_select('(SELECT name FROM benefeciaries_areas WHERE id=e.area_id) as area_name');
					}
					if( in_array('emp_status', $columns)) {
						$benefeciaries->set_select('(SELECT name FROM terms_list WHERE id=e.status) as status_name');
					}

					if( in_array('contact_address', $columns)) {
						$benefeciaries->set_select('(SELECT nm.meta_value FROM  names_meta nm WHERE nm.name_id=e.name_id AND nm.meta_key="address") as address');
					}
					if( in_array('contact_email', $columns)) {
						$benefeciaries->set_select('(SELECT nm.meta_value FROM  names_meta nm WHERE nm.name_id=e.name_id AND nm.meta_key="email") as email');
					}
					if( in_array('contact_phone', $columns)) {
						$benefeciaries->set_select('(SELECT nm.meta_value FROM  names_meta nm WHERE nm.name_id=e.name_id AND nm.meta_key="phone_number") as phone_number');
					}
					if( in_array('contact_smart', $columns)) {
						$benefeciaries->set_select('(SELECT nm.meta_value FROM  names_meta nm WHERE nm.name_id=e.name_id AND nm.meta_key="cell_smart") as cell_smart');
					}
					if( in_array('contact_globe', $columns)) {
						$benefeciaries->set_select('(SELECT nm.meta_value FROM  names_meta nm WHERE nm.name_id=e.name_id AND nm.meta_key="cell_globe") as cell_globe');
					}
					if( in_array('contact_sun', $columns)) {
						$benefeciaries->set_select('(SELECT nm.meta_value FROM  names_meta nm WHERE nm.name_id=e.name_id AND nm.meta_key="cell_sun") as cell_sun');
					}

					if( in_array('sm_facebook', $columns)) {
						$benefeciaries->set_select('(SELECT nm.meta_value FROM  names_meta nm WHERE nm.name_id=e.name_id AND nm.meta_key="facebook_id") as facebook_id');
					}
					if( in_array('sm_twitter', $columns)) {
						$benefeciaries->set_select('(SELECT nm.meta_value FROM  names_meta nm WHERE nm.name_id=e.name_id AND nm.meta_key="twitter_id") as twitter_id');
					}
					if( in_array('sm_instagram', $columns)) {
						$benefeciaries->set_select('(SELECT nm.meta_value FROM  names_meta nm WHERE nm.name_id=e.name_id AND nm.meta_key="instagram_id") as instagram_id');
					}
					if( in_array('sm_skype', $columns)) {
						$benefeciaries->set_select('(SELECT nm.meta_value FROM  names_meta nm WHERE nm.name_id=e.name_id AND nm.meta_key="skype_id") as skype_id');
					}
					if( in_array('sm_yahoo', $columns)) {
						$benefeciaries->set_select('(SELECT nm.meta_value FROM  names_meta nm WHERE nm.name_id=e.name_id AND nm.meta_key="yahoo_id") as yahoo_id');
					}
					if( in_array('sm_google', $columns)) {
						$benefeciaries->set_select('(SELECT nm.meta_value FROM  names_meta nm WHERE nm.name_id=e.name_id AND nm.meta_key="google_id") as google_id');
					}

					if( in_array('idn_tin', $columns)) {
						$benefeciaries->set_select('(SELECT nm.meta_value FROM  names_meta nm WHERE nm.name_id=e.name_id AND nm.meta_key="tin") as tin');
					}
					if( in_array('idn_sss', $columns)) {
						$benefeciaries->set_select('(SELECT nm.meta_value FROM  names_meta nm WHERE nm.name_id=e.name_id AND nm.meta_key="sss") as sss');
					}
					if( in_array('idn_hdmf', $columns)) {
						$benefeciaries->set_select('(SELECT nm.meta_value FROM  names_meta nm WHERE nm.name_id=e.name_id AND nm.meta_key="hdmf") as hdmf');
					}
					if( in_array('idn_phic', $columns)) {
						$benefeciaries->set_select('(SELECT nm.meta_value FROM  names_meta nm WHERE nm.name_id=e.name_id AND nm.meta_key="phic") as phic');
					}
					if( in_array('idn_driver', $columns)) {
						$benefeciaries->set_select('(SELECT nm.meta_value FROM  names_meta nm WHERE nm.name_id=e.name_id AND nm.meta_key="drivers_license") as drivers_license');
					}
					if( in_array('idn_voter', $columns)) {
						$benefeciaries->set_select('(SELECT nm.meta_value FROM  names_meta nm WHERE nm.name_id=e.name_id AND nm.meta_key="voters_number") as voters_number');
					}

					if( in_array('emergency_name', $columns)) {
						$benefeciaries->set_select('(SELECT nm.meta_value FROM  names_meta nm WHERE nm.name_id=e.name_id AND nm.meta_key="emergency_name") as emergency_name');
					}
					if( in_array('emergency_address', $columns)) {
						$benefeciaries->set_select('(SELECT nm.meta_value FROM  names_meta nm WHERE nm.name_id=e.name_id AND nm.meta_key="emergency_address") as emergency_address');
					}
					if( in_array('emergency_number', $columns)) {
						$benefeciaries->set_select('(SELECT nm.meta_value FROM  names_meta nm WHERE nm.name_id=e.name_id AND nm.meta_key="emergency_contact") as emergency_contact');
					}
					if( in_array('emergency_rel', $columns)) {
						$benefeciaries->set_select('(SELECT nm.meta_value FROM  names_meta nm WHERE nm.name_id=e.name_id AND nm.meta_key="emergency_relationship") as emergency_relationship');
					}
					return $benefeciaries;
	}

	public function report($page='config', $output='') {
	
		$this->template_data->set('page', $page);
		$this->template_data->set('output', $output);

		$services = new $this->Services_list_model;
		$services->setBenefeciaries(1,true);
		$services->setActive(1,true);
		$services->setTrash(0,true);
		$services->set_limit(0);
		$services->set_order('name');
		$this->template_data->set('services', $services->populate());

		$benefeciaries = new $this->Benefeciaries_model('e');
		$benefeciaries->setCenterId($this->session->userdata('current_center_id'),true);
		$benefeciaries->setTrash(0,true);
		
				$benefeciaries->set_join('names_info ni','ni.name_id=e.name_id');
				$benefeciaries->set_select('ni.lastname as lastname');
				$benefeciaries->set_select('ni.firstname as firstname');
				$benefeciaries->set_select('ni.middlename as middlename');

				$benefeciaries->set_select('(0) as status_name');
				$benefeciaries->set_select('(0) as group_name');
				$benefeciaries->set_select('(0) as position_name');
				$benefeciaries->set_select('(0) as area_name');

				$benefeciaries->set_limit(0);
				$benefeciaries->set_order('ni.lastname', 'ASC');
				$benefeciaries->set_order('ni.firstname', 'ASC');
				$benefeciaries->set_order('ni.middlename', 'ASC');

		switch( $page ) {
			case 'display':
				//if($this->input->get('benefeciary')) {
					
					$benefeciaries->set_select('e.*');

					$this->_employee_columns($benefeciaries, $this->input->get('columns'));

					//$benefeciaries->set_where_in('e.name_id', $this->input->get('benefeciary'));

					$this->template_data->set('benefeciaries', $benefeciaries->populate());
				//}
				$this->load->view('benefeciaries/benefeciaries/benefeciaries_report_display', $this->template_data->get_data());
			break;
			case 'download':
				if($this->input->get('benefeciary')) {
					
					$benefeciaries->set_select('e.*');

					$this->_employee_columns($benefeciaries, $this->input->get('columns'));

					$benefeciaries->set_where_in('e.name_id', $this->input->get('benefeciary'));
					$this->template_data->set('benefeciaries', $benefeciaries->populate());
				}

				$center = new $this->Centers_list_model;
				$center->setId($this->session->userdata('current_center_id'),true);
				$center_data = $center->get();
				$filename = url_title($center_data->name)."-Benefeciaries-Report.xls";

				$this->output->set_content_type('application/vnd-ms-excel');
				$this->output->set_header('Content-Disposition: attachment; filename=' . $filename);
				$this->load->view('benefeciaries/benefeciaries/benefeciaries_report_xls', $this->template_data->get_data());
			break;
			case 'config':
			default:

				if($this->input->post()) {
					redirect( site_url("benefeciaries/report/display") . "?" . http_build_query($this->input->post()) );
					exit;
				}

				//$benefeciaries->set_select('e.*');
				//$benefeciaries->set_order('ni.lastname', 'ASC');
				//$benefeciaries->set_limit(0);
				//$this->template_data->set('benefeciaries', $benefeciaries->populate());

				$this->load->view('benefeciaries/benefeciaries/benefeciaries_report_config', $this->template_data->get_data());
			break;
		}

	}

	public function height_weight($name_id) {

		$this->template_data->set('current_page', 'Height & Weight');
		$this->template_data->set('current_uri', 'height_weight');

		$name = new $this->Names_list_model('nl');
		$name->setId($name_id, true);
		$name->set_select("nl.*");
		$name->set_join("names_info ni", "ni.name_id=nl.id");
		$name->set_select("ni.*");
		$name->set_select("(SELECT COUNT(*) FROM benefeciaries e WHERE e.name_id=nl.id AND e.trash=0) as is_employed");
		$name->set_select("(SELECT e.center_id FROM benefeciaries e WHERE e.name_id=nl.id) as center_id");
		$name->set_select("(SELECT name FROM centers_list c WHERE c.id=(SELECT e.center_id FROM benefeciaries e WHERE e.name_id=nl.id)) as company");
		$name->set_select("(TIMESTAMPDIFF(YEAR, ni.birthday, CURDATE())) as age");

		$name_data = $name->get();	
		$this->template_data->set('name', $name_data);

		$hw = new $this->Benefeciaries_hw_model('hw');
		$hw->setNameId($name_id,true);
		$hw->set_order('hw.date_hw', 'DESC');
		$hw->set_select('hw.*');
		$hw->set_join("names_info ni", "ni.name_id=hw.name_id");
		$hw->set_select("(TIMESTAMPDIFF(YEAR, ni.birthday, hw.date_hw)) as age");
		$hw->set_select("(TIMESTAMPDIFF(MONTH, ni.birthday, hw.date_hw) % 12) as age_m");
		$hw->set_select("(FLOOR(TIMESTAMPDIFF(DAY, ni.birthday, hw.date_hw)) % 30.4375 ) as age_d");
		$this->template_data->set('height_weights', $hw->populate());
		$this->template_data->set('height_weights_count', $hw->count_all_results());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 3,
			'base_url' => base_url( $this->config->item('index_page') . "/benefeciaries/height_weight/{$name_id}"),
			'total_rows' => $hw->count_all_results(),
			'per_page' => $hw->get_limit(),
			'ajax'=>true
		)));

		$this->load->view('benefeciaries/benefeciaries/height_weight/height_weight_list', $this->template_data->get_data());

	}

	public function add_height_weight($name_id, $output='') {

		$this->template_data->set('current_uri', 'height_weight');

		if( $this->input->post() ) {
				$this->form_validation->set_rules('date', 'Date', 'trim|required');
				$this->form_validation->set_rules('height', 'Height', 'trim|required');
				$this->form_validation->set_rules('weight', 'Weight', 'trim|required');
				$this->form_validation->set_rules('notes', 'Notes', 'trim');
				if( $this->form_validation->run() ) {
					$hw = new $this->Benefeciaries_hw_model('hw');
					$hw->setNameId($name_id,true);
					$hw->setDateHw(date('Y-m-d', strtotime($this->input->post('date'))),true);
					$hw->setHeight($this->input->post('height'));
					$hw->setWeight($this->input->post('weight'));
					$hw->setNotes($this->input->post('notes'));
					if( !$hw->nonEmpty() ) {
						$hw->insert();
					}
				}
				$this->postNext("q=" . $this->input->get('q') );
		}

		$name = new $this->Names_list_model('nl');
		$name->setId($name_id, true);
		$name->set_select("nl.*");
		$name->set_join("names_info ni", "ni.name_id=nl.id");
		$name->set_select("ni.*");
		$name->set_select("(SELECT COUNT(*) FROM benefeciaries e WHERE e.name_id=nl.id AND e.trash=0) as is_employed");
		$name->set_select("(SELECT e.center_id FROM benefeciaries e WHERE e.name_id=nl.id) as center_id");
		$name->set_select("(SELECT name FROM centers_list c WHERE c.id=(SELECT e.center_id FROM benefeciaries e WHERE e.name_id=nl.id)) as company");
		$name->set_select("(TIMESTAMPDIFF(YEAR, ni.birthday, CURDATE())) as age");

		$name_data = $name->get();	
		$this->template_data->set('name', $name_data);

		$this->template_data->set('output', $output);
		$this->load->view('benefeciaries/benefeciaries/height_weight/height_weight_add', $this->template_data->get_data());

	}

	public function edit_height_weight($hw_id, $output='') {

		$this->template_data->set('current_uri', 'height_weight');

		$hw = new $this->Benefeciaries_hw_model('hw');
		$hw->setId($hw_id,true,false);
		
		if( $this->input->post() ) {
				$this->form_validation->set_rules('date', 'Date', 'trim|required');
				$this->form_validation->set_rules('height', 'Height', 'trim|required');
				$this->form_validation->set_rules('weight', 'Weight', 'trim|required');
				$this->form_validation->set_rules('notes', 'Notes', 'trim');
				if( $this->form_validation->run() ) {
					$hw->setDateHw(date('Y-m-d', strtotime($this->input->post('date'))),false,true);
					$hw->setHeight($this->input->post('height'),false,true);
					$hw->setWeight($this->input->post('weight'),false,true);
					$hw->setNotes($this->input->post('notes'),false,true);
					if( $hw->nonEmpty() ) {
						$hw->update();
					}
				}
				$this->postNext("q=" . $this->input->get('q') );
		}

		$hw_data = $hw->get();
		$this->template_data->set('hw',  $hw_data);

		$name = new $this->Names_list_model('nl');
		$name->setId($hw_data->name_id, true);
		$name->set_select("nl.*");
		$name->set_join("names_info ni", "ni.name_id=nl.id");
		$name->set_select("ni.*");
		$name->set_select("(SELECT COUNT(*) FROM benefeciaries e WHERE e.name_id=nl.id AND e.trash=0) as is_employed");
		$name->set_select("(SELECT e.center_id FROM benefeciaries e WHERE e.name_id=nl.id) as center_id");
		$name->set_select("(SELECT name FROM centers_list c WHERE c.id=(SELECT e.center_id FROM benefeciaries e WHERE e.name_id=nl.id)) as company");
		$name->set_select("(TIMESTAMPDIFF(YEAR, ni.birthday, CURDATE())) as age");

		$name_data = $name->get();	
		$this->template_data->set('name', $name_data);

		$this->template_data->set('output', $output);
		$this->load->view('benefeciaries/benefeciaries/height_weight/height_weight_edit', $this->template_data->get_data());

	}

	public function attendance($name_id) {

		$this->template_data->set('current_page', 'Attendance');
		$this->template_data->set('current_uri', 'attendance');

		$name = new $this->Names_list_model('nl');
		$name->setId($name_id, true);
		$name->set_select("nl.*");
		
		$name->set_join("names_info ni", "ni.name_id=nl.id");
		$name->set_select("ni.*");
		$name->set_select("(SELECT COUNT(*) FROM benefeciaries e WHERE e.name_id=nl.id AND e.trash=0) as is_employed");
		$name->set_select("(SELECT e.center_id FROM benefeciaries e WHERE e.name_id=nl.id) as center_id");
		$name->set_select("(SELECT name FROM centers_list c WHERE c.id=(SELECT e.center_id FROM benefeciaries e WHERE e.name_id=nl.id)) as company");
		$name->set_select("(TIMESTAMPDIFF(YEAR, ni.birthday, CURDATE())) as age");

		$name_data = $name->get();	
		$this->template_data->set('name', $name_data);

		$activities = new $this->Activities_attendance_model('aa');
		$activities->setNameId($name_id,true);
		$activities->set_join('activities_list al', 'al.id=aa.activity_id');
		$activities->set_select('al.*');
		$activities->set_select('(SELECT sl.name FROM services_list sl WHERE sl.id=al.service_id) as service_name');
		$this->template_data->set('activities', $activities->populate());
		$this->template_data->set('activities_count', $activities->count_all_results());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 3,
			'base_url' => base_url( $this->config->item('index_page') . "/benefeciaries/attendance/{$name_id}"),
			'total_rows' => $activities->count_all_results(),
			'per_page' => $activities->get_limit(),
			'ajax'=>true
		)));

		$this->load->view('benefeciaries/benefeciaries/benefeciaries_attendance', $this->template_data->get_data());

	}

}
