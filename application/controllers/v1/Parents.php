<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Parents extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->template_data->set('current_page', 'Parents');
		$this->template_data->set('current_uri', 'parents');
		$this->template_data->set('navbar_search', true);

		$this->_isCompanyId();
		
		$this->_isAuth('parents', 'parents', 'view');

	}

	public function index($start=0) {

		$parents = new $this->Parents_model('e');
		if( $this->input->get('q') ) {
			$parents->set_where('(ni.lastname LIKE "%' . $this->input->get('q') . '%"', NULL, 99);
			$parents->set_where_or('ni.firstname LIKE "%' . $this->input->get('q') . '%"', NULL, 99);
			$parents->set_where_or('ni.middlename LIKE "%' . $this->input->get('q') . '%")', NULL, 99);
		}

		if( $this->input->get('filter') == 'trash' ) {
			$parents->setTrash(1,true);
		} else {
			$parents->setTrash(0,true);
		}

		$parents->setCenterId($this->session->userdata('current_center_id'),true);
		
		$parents->set_select('e.*');
		$parents->set_select('(SELECT name FROM parents_groups WHERE id=e.group_id) as group_name');
		$parents->set_select('(SELECT name FROM parents_areas WHERE id=e.area_id) as area_name');
		$parents->set_select('(SELECT name FROM terms_list WHERE id=e.status) as status_name');

		$parents->set_join('names_info ni','ni.name_id=e.name_id');
		$parents->set_select('ni.lastname as lastname');
		$parents->set_select('ni.firstname as firstname');
		$parents->set_select('ni.middlename as middlename');

		$parents->set_order('ni.lastname', 'ASC');
		$parents->set_start($start);

		$this->template_data->set('parents', $parents->populate());
		$this->template_data->set('parents_count', $parents->count_all_results());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 3,
			'base_url' => base_url( $this->config->item('index_page') . "/parents/index"),
			'total_rows' => $parents->count_all_results(),
			'per_page' => $parents->get_limit(),
			'ajax'=>true
		)));

		if( $this->input->get('filter') == 'trash' ) {
			$this->load->view('parents/parents/parents_list_trash', $this->template_data->get_data());
		} else {
			$this->load->view('parents/parents/parents_list', $this->template_data->get_data());
		}
	}

	public function group($id, $start=0) {

		$groups = new $this->Parents_groups_model;
		$groups->setId($id,true);
		$this->template_data->set('group', $groups->get());

		$parents = new $this->Parents_model('e');
		if( $this->input->get('q') ) {
			$parents->set_where('(ni.lastname LIKE "%' . $this->input->get('q') . '%"', NULL, 99);
			$parents->set_where_or('ni.firstname LIKE "%' . $this->input->get('q') . '%"', NULL, 99);
			$parents->set_where_or('ni.middlename LIKE "%' . $this->input->get('q') . '%")', NULL, 99);
		}

		if( $this->input->get('filter') == 'trash' ) {
			$parents->setTrash(1,true);
		} else {
			$parents->setTrash(0,true);
		}

		$parents->setCenterId($this->session->userdata('current_center_id'),true);
		$parents->setGroupId($id,true);
		$parents->set_select('e.*');
		$parents->set_select('(SELECT name FROM parents_groups WHERE id=e.group_id) as group_name');
		$parents->set_select('(SELECT name FROM parents_areas WHERE id=e.area_id) as area_name');
		$parents->set_select('(SELECT name FROM terms_list WHERE id=e.status) as status_name');
		
		$parents->set_join('names_info ni','ni.name_id=e.name_id');
		$parents->set_select('ni.lastname as lastname');
		$parents->set_select('ni.firstname as firstname');
		$parents->set_select('ni.middlename as middlename');

		$parents->set_order('ni.lastname', 'ASC');

		$parents->set_start($start);
		$this->template_data->set('parents', $parents->populate());
		$this->template_data->set('parents_count', $parents->count_all_results());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 4,
			'base_url' => base_url($this->config->item('index_page') . "/parents/group/{$id}"),
			'total_rows' => $parents->count_all_results(),
			'per_page' => $parents->get_limit(),
			'ajax'=>true
		)));

		$this->load->view('parents/parents/parents_list', $this->template_data->get_data());
	}

	public function area($id, $start=0) {

		$area = new $this->Parents_areas_model;
		$area->setId($id,true);
		$this->template_data->set('area', $area->get());

		$parents = new $this->Parents_model('e');
		if( $this->input->get('q') ) {
			$parents->set_where('(lastname LIKE "%' . $this->input->get('q') . '%"', NULL, 99);
			$parents->set_where_or('firstname LIKE "%' . $this->input->get('q') . '%"', NULL, 99);
			$parents->set_where_or('middlename LIKE "%' . $this->input->get('q') . '%")', NULL, 99);
		}
		$parents->setCenterId($this->session->userdata('current_center_id'),true);
		$parents->setAreaId($id,true);
		$parents->set_select('e.*');
		$parents->set_select('(SELECT name FROM parents_groups WHERE id=e.group_id) as group_name');
		$parents->set_select('(SELECT name FROM parents_areas WHERE id=e.area_id) as area_name');
		$parents->set_select('(SELECT name FROM terms_list WHERE id=e.status) as status_name');
		$parents->set_join('names_info ni','ni.name_id=e.name_id');
		$parents->set_select('ni.lastname as lastname');
		$parents->set_select('ni.firstname as firstname');
		$parents->set_select('ni.middlename as middlename');
		$parents->set_order('ni.lastname', 'ASC');
		$parents->set_start($start);
		$this->template_data->set('parents', $parents->populate());
		$this->template_data->set('parents_count', $parents->count_all_results());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 4,
			'base_url' => base_url($this->config->item('index_page') . "/parents/area/{$id}"),
			'total_rows' => $parents->count_all_results(),
			'per_page' => $parents->get_limit(),
			'ajax'=>true
		)));

		$this->load->view('parents/parents/parents_list', $this->template_data->get_data());
	}

	public function add($id, $output='') {
		$this->_isAuth('parents', 'parents', 'add');

		$this->template_data->set('output', $output);

		$names = new $this->Names_list_model;
		$names->setId($id, true);
		$names->setTrash(0,true);
		$names->setTypeParent(1,true);
		$names->set_join('names_info', 'names_info.name_id=names_list.id');
		$name_data = $names->get();
		$this->template_data->set('name', $name_data);

		if( $names->nonEmpty() ) {
			if( $this->input->post('action') == 'add' ) {
				$benefeciary = new $this->Parents_model;
				$benefeciary->setNameId($id);
				$benefeciary->setGroupId($this->input->post('group_id'));
				$benefeciary->setAreaId($this->input->post('area_id'));
				$benefeciary->setCenterId($this->session->userdata('current_center_id'));
				$benefeciary->setTrash(0);
				if( $benefeciary->insert() ) {
					record_system_audit($this->session->userdata('user_id'), 'parents', 'parents', 'add', $this->session->userdata('current_center_id'), "Benefeciary Added: {$name_data->full_name}", $id);
				}
				$this->postNext();
			}
		}


		$groups = new $this->Parents_groups_model;
		$groups->setCenterId($this->session->userdata('current_center_id'),true);
		$groups->set_limit(0);
		$groups->set_order('name', 'ASC');
		$this->template_data->set('groups', $groups->populate());

		$areas = new $this->Parents_areas_model;
		$areas->setCenterId($this->session->userdata('current_center_id'),true);
		$areas->set_limit(0);
		$areas->set_order('name', 'ASC');
		$this->template_data->set('areas', $areas->populate());

		$this->load->view('parents/parents/parents_add', $this->template_data->get_data());

	}

	public function add_multiple($output='0', $start=0) {
		$this->_isAuth('parents', 'parents', 'add');

		if( $this->input->post('employee') ) {
			foreach($this->input->post('employee') as $name_id => $benefeciary) {
				if( isset($benefeciary['add']) ) {
					$benefeciary1 = new $this->Parents_model;
					$benefeciary1->setNameId( $name_id, true);
					$benefeciary1->setGroupId( $benefeciary['group_id'] );
					$benefeciary1->setAreaId( $benefeciary['area_id'] );
					$benefeciary1->setCenterId( $this->session->userdata('current_center_id'), true);
					$benefeciary1->setTrash(0);
					if( $benefeciary1->nonEmpty()===FALSE ) {
						if( $benefeciary1->insert() ) {
							record_system_audit($this->session->userdata('user_id'), 'parents', 'parents', 'add_multiple', $this->session->userdata('current_center_id'), "Added Multiple Parents");
						}
					}
					
				}
			}
			$this->postNext();
		}

		$names = new $this->Names_list_model;
		$names->set_start($start);
		$names->set_limit(10);
		$names->setTrash(0,true);
		$names->set_order('names_list.full_name', 'ASC');
		$names->set_where('((SELECT COUNT(*) FROM `parents` WHERE name_id=names_list.id) = 0)');
		$names->set_where('((SELECT COUNT(*) FROM `names_info` WHERE name_id=names_list.id) = 1)');

		$this->template_data->set('names', $names->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 4,
			'base_url' => base_url($this->config->item('index_page') . "/parents/add_multiple/{$output}"),
			'total_rows' => $names->count_all_results(),
			'per_page' => $names->get_limit()
		), '?next=' . $this->input->get('next') ));

		$groups = new $this->Parents_groups_model;
		$groups->setCenterId($this->session->userdata('current_center_id'),true);
		$groups->set_limit(0);
		$groups->set_order('name', 'ASC');
		$this->template_data->set('groups', $groups->populate());

		$areas = new $this->Parents_areas_model;
		$areas->setCenterId($this->session->userdata('current_center_id'),true);
		$areas->set_limit(0);
		$areas->set_order('name', 'ASC');
		$this->template_data->set('areas', $areas->populate());

		$this->template_data->set('output', $output);
		$this->load->view('parents/parents/parents_add_multiple', $this->template_data->get_data());

	}

	public function search_name($output='', $start=0) {

		$this->_isAuth('parents', 'parents', 'add');

		$names = new $this->Names_list_model;
		$names->set_start($start);
		$names->set_limit(5);
		$names->setTrash(0,true);
		$names->setTypeParent(1,true);
		$names->set_order('names_list.full_name', 'ASC');
		$names->set_where('((SELECT COUNT(*) FROM `parents` WHERE name_id=names_list.id) = 0)');
		$names->set_where('((SELECT COUNT(*) FROM `names_info` WHERE name_id=names_list.id) = 1)');
		
		
		if( $output == 'autocomplete') {
			
			if( $this->input->get('term') ) {
				$names->set_where('names_list.full_name LIKE "%' . $this->input->get('term') . '%"', NULL, 99);
			}

			$results = array();

			foreach($names->populate() as $name) {
					$results[] = array(
						'label' => $name->full_name,
						'id' => $name->id,
						'redirect'=> site_url( "parents/add/{$name->id}" ) . "?next=parents/search_name",
						'innerBody'=> site_url( "parents/add/{$name->id}/ajax" ) . "?next=parents",
						);
				}

			$this->output->set_content_type('application/json')->set_output(json_encode( $results ));

		} else {

			$this->template_data->set('names', $names->populate());

			$this->template_data->set('pagination', bootstrap_pagination(array(
				'uri_segment' => 4,
				'base_url' => base_url($this->config->item('index_page') . "/parents/search_name/{$output}"),
				'total_rows' => $names->count_all_results(),
				'per_page' => $names->get_limit(),
				'attributes' => array(
					'class' => 'btn btn-default ajax-modal-inner',
					'data-hide_footer' => 1
					)
			), '?next=' . $this->input->get('next') ));
			
			$this->template_data->set('output', $output);
			$this->load->view('parents/parents/parents_add_search', $this->template_data->get_data());
		}
		
	}

	public function edit_personal($id,$output='') {
		$this->_isAuth('parents', 'parents', 'edit');

		$benefeciary = new $this->Parents_model;
		$benefeciary->setNameId($id,true);
		$benefeciary_data = $benefeciary->get();

		if( $benefeciary->nonEmpty() ) {
			if( $this->input->post() ) {
				$this->form_validation->set_rules('lastname', 'Last Name', 'trim|required');
				$this->form_validation->set_rules('firstname', 'First Name', 'trim|required');
				$this->form_validation->set_rules('middlename', 'Middle Name', 'trim');
				if( $this->form_validation->run() ) {
					$benefeciary->setLastname($this->input->post('lastname'),false,true);
					$benefeciary->setFirstname($this->input->post('firstname'),false,true);
					$benefeciary->setMiddlename($this->input->post('middlename'),false,true);
					$benefeciary->setBirthday( date("Y-m-d", strtotime($this->input->post('birthday'))),false,true);
					$benefeciary->setBirthplace($this->input->post('birthplace'),false,true);
					$benefeciary->setGender($this->input->post('gender'),false,true);
					$benefeciary->setCivilStatus($this->input->post('civil_status'),false,true);
					if( $benefeciary->update() ) {
						record_system_audit($this->session->userdata('user_id'), 'parents', 'parents', 'edit', $this->session->userdata('current_center_id'), "Edit Personal Info: {$benefeciary_data->lastname}, {$benefeciary_data->firstname}", $id);
					}
				}
				$this->postNext();
			}
		}

		$this->template_data->set('employee', $benefeciary->get());

		$this->template_data->set('output', $output);
		$this->load->view('parents/parents/parents_edit_personal', $this->template_data->get_data());
	}

	public function edit_employment($id,$output='') {
		$this->_isAuth('parents', 'parents', 'edit');

		$benefeciary = new $this->Parents_model;
		$benefeciary->setNameId($id,true,false);

		if( $benefeciary->nonEmpty() ) {
			if( $this->input->post() ) {
				$this->form_validation->set_rules('center_id', 'Center', 'trim');
				$this->form_validation->set_rules('group_id', 'Group', 'trim');
				$this->form_validation->set_rules('area_id', 'Area', 'trim');
				$this->form_validation->set_rules('date_hired', 'Hired', 'trim');
				$this->form_validation->set_rules('status', 'Status', 'trim');
				$this->form_validation->set_rules('notes', 'Notes', 'trim');
				if( $this->form_validation->run() ) {
					$benefeciary->setCenterId($this->input->post('center_id'),false,true);
					$benefeciary->setGroupId($this->input->post('group_id'),false,true);
					$benefeciary->setAreaId($this->input->post('area_id'),false,true);
					$benefeciary->setHired( date('Y-m-d', strtotime($this->input->post('date_hired'))),false,true);
					$benefeciary->setStatus($this->input->post('status'),false,true);
					$benefeciary->setNotes($this->input->post('notes'),false,true);
					$benefeciary->setBenefeciaryId($this->input->post('employee_id'),false,true);
					if( $benefeciary->update() ) {
						record_system_audit($this->session->userdata('user_id'), 'parents', 'parents', 'edit', $this->session->userdata('current_center_id'), "Edit Employment", $id);
					}
				}
				$this->postNext("active=employment");
			}
		}
		$benefeciary_data = $benefeciary->get();
		$this->template_data->set('employee', $benefeciary_data);

		$center_id = ($benefeciary_data->center_id > 0) ? $benefeciary_data->center_id : $this->session->userdata('current_center_id');

		$groups = new $this->Parents_groups_model;
		$groups->setCenterId($center_id,true);
		$groups->set_limit(0);
		$groups->set_order('name', 'ASC');
		$this->template_data->set('groups', $groups->populate());

		$areas = new $this->Parents_areas_model;
		$areas->setCenterId($center_id,true);
		$areas->set_limit(0);
		$areas->set_order('name', 'ASC');
		$this->template_data->set('areas', $areas->populate());

		$centers = new $this->Centers_list_model;
		$centers->setTrash(0,true);
		$centers->set_order('name', 'ASC');
		$centers->set_limit(0);
		$this->template_data->set('centers', $centers->populate());

		$terms = new $this->Terms_list_model;
		$terms->set_select("*");
		$terms->set_order('priority', 'ASC');
		$terms->set_order('name', 'ASC');
		$terms->set_start(0);
		$terms->setTrash('0',true);
		$terms->setType('employment_status',true);
		$this->template_data->set('employment_status', $terms->populate());

		$this->template_data->set('output', $output);
		$this->load->view('parents/parents/parents_edit_employment', $this->template_data->get_data());
	}

	public function edit_address($id,$output='') {

		$this->_isAuth('parents', 'parents', 'edit');

		$benefeciary = new $this->Parents_contacts_model;
		$benefeciary->setNameId($id,true);
		$benefeciary_data = $benefeciary->get();

		if( $this->input->post() ) {
			$this->form_validation->set_rules('phone_number', 'Phone Number', 'trim');
			$this->form_validation->set_rules('cell_number', 'Cellphone Number', 'trim');
			$this->form_validation->set_rules('address', 'Address', 'trim');
			if( $this->form_validation->run() ) {
				$benefeciary->setPhoneNumber($this->input->post('phone_number'),false,true);
				$benefeciary->setCellNumber($this->input->post('cell_number'),false,true);
				$benefeciary->setAddress($this->input->post('address'),false,true);
				if($benefeciary->nonEmpty()) {
					if( $benefeciary->update() ) {
						record_system_audit($this->session->userdata('user_id'), 'parents', 'parents', 'edit', $this->session->userdata('current_center_id'), "Edit Address: {$benefeciary_data->full_name}", $id);
					}
				} else {
					$benefeciary->setNameId($id,true,true);
					if( $benefeciary->insert() ) {
						record_system_audit($this->session->userdata('user_id'), 'parents', 'parents', 'edit', $this->session->userdata('current_center_id'), "Added Address: {$benefeciary_data->full_name}", $id);
					}
				}
			}
			$this->postNext();
		}
		

		$this->template_data->set('employee', $benefeciary->get());

		$this->template_data->set('output', $output);
		$this->load->view('parents/parents/parents_edit_address', $this->template_data->get_data());
	}

	public function restore($id) {
		$this->_isAuth('parents', 'parents', 'delete');

		$benefeciary = new $this->Parents_model;
		$benefeciary->setNameId($id,true,false);
		$benefeciary->setTrash(0,false,true);
		if( $benefeciary->update() ) {
			record_system_audit($this->session->userdata('user_id'), 'parents', 'parents', 'restore', $this->session->userdata('current_center_id'), "Benefeciary Restored", $id);
		}
		
		$this->getNext("parents");
	}

	public function delete($id) {
		$this->_isAuth('parents', 'parents', 'delete');

		$benefeciary = new $this->Parents_model;
		$benefeciary->setNameId($id,true);
		$benefeciary->setTrash(1,true);
		if( $benefeciary->delete() ) {
			record_system_audit($this->session->userdata('user_id'), 'parents', 'parents', 'delete', $this->session->userdata('current_center_id'), "Benefeciary Deactivated", $id);
		}
		
		$this->getNext("parents");
	}

	public function deactivate($id) {
		$this->_isAuth('parents', 'parents', 'delete');

		$benefeciary = new $this->Parents_model;
		$benefeciary->setNameId($id,true,false);
		$benefeciary->setTrash(1,false,true);
		if( $benefeciary->update() ) {
			record_system_audit($this->session->userdata('user_id'), 'parents', 'parents', 'deactivate', $this->session->userdata('current_center_id'), "Benefeciary Deactivated", $id);
		}
		
		$this->getNext("parents");
	}

	public function config($id, $output='') {

		$benefeciary = new $this->Parents_model;
		$benefeciary->setNameId($id,true);
		$this->template_data->set('employee', $benefeciary->get());
		
		$this->template_data->set('output', $output);

		$this->load->view('parents/parents/parents_config', $this->template_data->get_data());

	}

	public function edit_leave_benefits($id,$selected_year=NULL,$output='') {

		$this->template_data->set('selected_year', $selected_year);

		$benefeciary = new $this->Parents_model;
		$benefeciary->setNameId($id,true);
		$benefeciary_data = $benefeciary->get();
		$this->template_data->set('employee', $benefeciary_data);

		if( $selected_year ) {
			if( $this->input->post('leave') ) {
				foreach($this->input->post('leave') as $benefit_id=>$days) {
					$leave_b = new $this->Parents_leave_benefits_model('elb');
					$leave_b->setCenterId($benefeciary_data->center_id,true);
					$leave_b->setNameId($benefeciary_data->name_id,true);
					$leave_b->setBenefitId($benefit_id,true);
					$leave_b->setYear($selected_year,true);

					if( $leave_b->nonEmpty() ) {
						$leave_b->setDays($days,false,true);
						if( $leave_b->update() ) {
							record_system_audit($this->session->userdata('user_id'), 'parents', 'parents', 'edit_leave_benefits', $this->session->userdata('current_center_id'), "Leave Benefits Updated", $id);
						}
					} else {
						$leave_b->setDays($days);
						if( $leave_b->insert() ) {
							record_system_audit($this->session->userdata('user_id'), 'parents', 'parents', 'edit_leave_benefits', $this->session->userdata('current_center_id'), "Leave Benefits Inserted", $id);
						}
					}
				}
				$this->postNext();
			}
			
			$leave = new $this->Benefits_list_model('b');
			$leave->setLeave(1,true);
			$leave->setTrash(0,true);
			$leave->set_select("*");
			$leave->set_select("(SELECT elb.days FROM parents_leave_benefits elb WHERE elb.name_id={$benefeciary_data->name_id} AND elb.center_id={$benefeciary_data->center_id} AND b.id=elb.benefit_id AND elb.year='{$selected_year}' LIMIT 1) as days");
			$this->template_data->set('leaves', $leave->populate());
		}

		$payroll_years = new $this->Payroll_model;
		$payroll_years->setCenterId($this->session->userdata('current_center_id'),true);
		$payroll_years->set_select('year');
		$payroll_years->set_group_by('year');
		$payroll_years->set_order('year', 'DESC');
		$payroll_years->set_limit(0);
		$this->template_data->set('payroll_years', $payroll_years->populate());

		$this->template_data->set('output', $output);
		$this->load->view('parents/parents/parents_edit_leave_benefits', $this->template_data->get_data());
	}

	private function _employee_columns($parents, $columns) {
					
					$columns = ($columns) ? $columns : array();
					// personal info
					$parents->set_select('ni.*');
					if( in_array('age', $columns)) {
						$parents->set_select("(TIMESTAMPDIFF(YEAR, ni.birthday, CURDATE())) as age");
					}

					// employment info
					if( in_array('emp_group', $columns)) {
						$parents->set_select('(SELECT name FROM parents_groups WHERE id=e.group_id) as group_name');
					}
					if( in_array('emp_area', $columns)) {
						$parents->set_select('(SELECT name FROM parents_areas WHERE id=e.area_id) as area_name');
					}
					if( in_array('emp_status', $columns)) {
						$parents->set_select('(SELECT name FROM terms_list WHERE id=e.status) as status_name');
					}

					if( in_array('contact_address', $columns)) {
						$parents->set_select('(SELECT nm.meta_value FROM  names_meta nm WHERE nm.name_id=e.name_id AND nm.meta_key="address") as address');
					}
					if( in_array('contact_email', $columns)) {
						$parents->set_select('(SELECT nm.meta_value FROM  names_meta nm WHERE nm.name_id=e.name_id AND nm.meta_key="email") as email');
					}
					if( in_array('contact_phone', $columns)) {
						$parents->set_select('(SELECT nm.meta_value FROM  names_meta nm WHERE nm.name_id=e.name_id AND nm.meta_key="phone_number") as phone_number');
					}
					if( in_array('contact_smart', $columns)) {
						$parents->set_select('(SELECT nm.meta_value FROM  names_meta nm WHERE nm.name_id=e.name_id AND nm.meta_key="cell_smart") as cell_smart');
					}
					if( in_array('contact_globe', $columns)) {
						$parents->set_select('(SELECT nm.meta_value FROM  names_meta nm WHERE nm.name_id=e.name_id AND nm.meta_key="cell_globe") as cell_globe');
					}
					if( in_array('contact_sun', $columns)) {
						$parents->set_select('(SELECT nm.meta_value FROM  names_meta nm WHERE nm.name_id=e.name_id AND nm.meta_key="cell_sun") as cell_sun');
					}

					if( in_array('sm_facebook', $columns)) {
						$parents->set_select('(SELECT nm.meta_value FROM  names_meta nm WHERE nm.name_id=e.name_id AND nm.meta_key="facebook_id") as facebook_id');
					}
					if( in_array('sm_twitter', $columns)) {
						$parents->set_select('(SELECT nm.meta_value FROM  names_meta nm WHERE nm.name_id=e.name_id AND nm.meta_key="twitter_id") as twitter_id');
					}
					if( in_array('sm_instagram', $columns)) {
						$parents->set_select('(SELECT nm.meta_value FROM  names_meta nm WHERE nm.name_id=e.name_id AND nm.meta_key="instagram_id") as instagram_id');
					}
					if( in_array('sm_skype', $columns)) {
						$parents->set_select('(SELECT nm.meta_value FROM  names_meta nm WHERE nm.name_id=e.name_id AND nm.meta_key="skype_id") as skype_id');
					}
					if( in_array('sm_yahoo', $columns)) {
						$parents->set_select('(SELECT nm.meta_value FROM  names_meta nm WHERE nm.name_id=e.name_id AND nm.meta_key="yahoo_id") as yahoo_id');
					}
					if( in_array('sm_google', $columns)) {
						$parents->set_select('(SELECT nm.meta_value FROM  names_meta nm WHERE nm.name_id=e.name_id AND nm.meta_key="google_id") as google_id');
					}

					if( in_array('idn_tin', $columns)) {
						$parents->set_select('(SELECT nm.meta_value FROM  names_meta nm WHERE nm.name_id=e.name_id AND nm.meta_key="tin") as tin');
					}
					if( in_array('idn_sss', $columns)) {
						$parents->set_select('(SELECT nm.meta_value FROM  names_meta nm WHERE nm.name_id=e.name_id AND nm.meta_key="sss") as sss');
					}
					if( in_array('idn_hdmf', $columns)) {
						$parents->set_select('(SELECT nm.meta_value FROM  names_meta nm WHERE nm.name_id=e.name_id AND nm.meta_key="hdmf") as hdmf');
					}
					if( in_array('idn_phic', $columns)) {
						$parents->set_select('(SELECT nm.meta_value FROM  names_meta nm WHERE nm.name_id=e.name_id AND nm.meta_key="phic") as phic');
					}
					if( in_array('idn_driver', $columns)) {
						$parents->set_select('(SELECT nm.meta_value FROM  names_meta nm WHERE nm.name_id=e.name_id AND nm.meta_key="drivers_license") as drivers_license');
					}
					if( in_array('idn_voter', $columns)) {
						$parents->set_select('(SELECT nm.meta_value FROM  names_meta nm WHERE nm.name_id=e.name_id AND nm.meta_key="voters_number") as voters_number');
					}

					if( in_array('emergency_name', $columns)) {
						$parents->set_select('(SELECT nm.meta_value FROM  names_meta nm WHERE nm.name_id=e.name_id AND nm.meta_key="emergency_name") as emergency_name');
					}
					if( in_array('emergency_address', $columns)) {
						$parents->set_select('(SELECT nm.meta_value FROM  names_meta nm WHERE nm.name_id=e.name_id AND nm.meta_key="emergency_address") as emergency_address');
					}
					if( in_array('emergency_number', $columns)) {
						$parents->set_select('(SELECT nm.meta_value FROM  names_meta nm WHERE nm.name_id=e.name_id AND nm.meta_key="emergency_contact") as emergency_contact');
					}
					if( in_array('emergency_rel', $columns)) {
						$parents->set_select('(SELECT nm.meta_value FROM  names_meta nm WHERE nm.name_id=e.name_id AND nm.meta_key="emergency_relationship") as emergency_relationship');
					}
					return $parents;
	}

	public function report($page='config', $output='') {
	
		$this->template_data->set('page', $page);
		$this->template_data->set('output', $output);

		$parents = new $this->Parents_model('e');
		$parents->setCenterId($this->session->userdata('current_center_id'),true);
		$parents->setTrash(0,true);
		
				$parents->set_join('names_info ni','ni.name_id=e.name_id');
				$parents->set_select('ni.lastname as lastname');
				$parents->set_select('ni.firstname as firstname');
				$parents->set_select('ni.middlename as middlename');

				$parents->set_limit(0);
				$parents->set_order('ni.lastname', 'ASC');
				$parents->set_order('ni.firstname', 'ASC');
				$parents->set_order('ni.middlename', 'ASC');
		switch( $page ) {
			case 'display':
				if($this->input->get('employee')) {
					
					$parents->set_select('e.*');

					$this->_employee_columns($parents, $this->input->get('columns'));

					$parents->set_where_in('e.name_id', $this->input->get('employee'));
					$this->template_data->set('parents', $parents->populate());
				}
				$this->load->view('parents/parents/parents_report_display', $this->template_data->get_data());
			break;
			case 'download':
				if($this->input->get('employee')) {
					
					$parents->set_select('e.*');

					$this->_employee_columns($parents, $this->input->get('columns'));

					$parents->set_where_in('e.name_id', $this->input->get('employee'));
					$this->template_data->set('parents', $parents->populate());
				}

				$center = new $this->Centers_list_model;
				$center->setId($this->session->userdata('current_center_id'),true);
				$center_data = $center->get();
				$filename = url_title($center_data->name)."-Parents-Report.xls";

				$this->output->set_content_type('application/vnd-ms-excel');
				$this->output->set_header('Content-Disposition: attachment; filename=' . $filename);
				$this->load->view('parents/parents/parents_report_xls', $this->template_data->get_data());
			break;
			case 'config':
			default:

				if($this->input->post()) {
					redirect( site_url("parents/report/display") . "?" . http_build_query($this->input->post()) );
					exit;
				}

				$parents->set_select('e.*');
				$parents->set_order('ni.lastname', 'ASC');
				$parents->set_limit(0);
				$this->template_data->set('parents', $parents->populate());
				$this->load->view('parents/parents/parents_report_config', $this->template_data->get_data());
			break;
		}

	}

}
