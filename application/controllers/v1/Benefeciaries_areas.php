<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Benefeciaries_areas extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->template_data->set('current_page', 'Benefeciary Areas');
		$this->template_data->set('current_uri', 'benefeciaries_areas');
		$this->template_data->set('navbar_search', true);

		$this->_isAuth('benefeciaries', 'benefeciaries', 'edit');

	}

	public function index($start=0) {
		
		$areas = new $this->Benefeciaries_areas_model;
		if( $this->input->get('q') ) {
			$areas->set_where('name LIKE "%' . $this->input->get('q') . '%"', NULL, 99);
		}
		
		if( $this->input->get('filter') == 'trash' ) {
			$areas->setTrash(1, true);
		} else {
			$areas->setTrash(0, true);
		}

		$areas->setCenterId($this->session->userdata('current_center_id'),true);
		
		$areas->set_select("*");
		$areas->set_select("(SELECT COUNT(*) FROM `benefeciaries` WHERE area_id=benefeciaries_areas.id) as benefeciaries_count");
		$areas->set_order('name', 'ASC');
		$areas->set_start($start);
		$this->template_data->set('areas', $areas->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => base_url($this->config->item('index_page') . '/benefeciaries_areas/index/'),
			'total_rows' => $areas->count_all_results(),
			'per_page' => $areas->get_limit(),
			'ajax'=>true,
		)));
		
		$this->load->view('benefeciaries/areas/areas_list', $this->template_data->get_data());
	}

	public function add($output='') {

		$this->_isAuth('benefeciaries', 'areas', 'add');

		if( $this->input->post() ) {
			$this->form_validation->set_rules('area_name', 'Area Name', 'trim|required');
			$this->form_validation->set_rules('notes', 'Notes', 'trim');
			if( $this->form_validation->run() ) {
				$areas = new $this->Benefeciaries_areas_model;
				$areas->setName($this->input->post('area_name'));
				$areas->setNotes($this->input->post('notes'));
				$areas->setCenterId($this->session->userdata('current_center_id'));
				if( $areas->insert() ) {
					record_system_audit($this->session->userdata('user_id'), 'benefeciaries', 'areas', 'add', $this->session->userdata('current_center_id'), "Benefeciary Area Added : {$this->input->post('area_name')}");
				}
				$this->getNext("benefeciaries_areas");
			}
		}

		$this->template_data->set('output', $output);
		$this->load->view('benefeciaries/areas/areas_add', $this->template_data->get_data());
	}

	public function edit($id,$output='') {

		$this->_isAuth('benefeciaries', 'areas', 'edit');

		$areas = new $this->Benefeciaries_areas_model;
		$areas->setId($id,true);
		$area_data = $areas->get();

		if( $areas->nonEmpty() ) {
			if( $this->input->post() ) {
				$this->form_validation->set_rules('area_name', 'Area Name', 'trim|required');
				$this->form_validation->set_rules('notes', 'Notes', 'trim');
				if( $this->form_validation->run() ) {
					$areas->setName($this->input->post('area_name'),false,true);
					$areas->setNotes($this->input->post('notes'),false,true);
					if( $areas->update() ) {
						record_system_audit($this->session->userdata('user_id'), 'benefeciaries', 'areas', 'edit', $this->session->userdata('current_center_id'), "Benefeciary Area Edited : {$area_data->name}");
					}
				}
				$this->postNext();
			}
		}

		$areas->set_select("*");
		$areas->set_select("(SELECT COUNT(*) FROM `benefeciaries` WHERE area_id=benefeciaries_areas.id) as benefeciaries_count");
		$this->template_data->set('area', $areas->get());

		$this->template_data->set('output', $output);
		$this->load->view('benefeciaries/areas/areas_edit', $this->template_data->get_data());
	}

	public function deactivate($id) {
		
		$this->_isAuth('benefeciaries', 'areas', 'delete');

		$areas = new $this->Benefeciaries_areas_model;
		$areas->setId($id,true,false);
		$areas->setTrash(1,false,true);
		$area_data = $areas->get();

		if( $areas->update() ) {
			record_system_audit($this->session->userdata('user_id'), 'benefeciaries', 'areas', 'delete', $this->session->userdata('current_center_id'), "Benefeciary Area Deleted : {$area_data->name}");
		}

		$this->getNext("benefeciaries_areas");
	}


	public function restore($id) {
		
		$this->_isAuth('benefeciaries', 'areas', 'delete');

		$areas = new $this->Benefeciaries_areas_model;
		$areas->setId($id,true,false);
		$areas->setTrash(0,false,true);
		$area_data = $areas->get();

		if( $areas->update() ) {
			record_system_audit($this->session->userdata('user_id'), 'benefeciaries', 'areas', 'delete', $this->session->userdata('current_center_id'), "Benefeciary Area Restored : {$area_data->name}");
		}

		$this->getNext("benefeciaries_areas");
	}

}
