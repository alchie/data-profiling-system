<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Parents_areas extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->template_data->set('current_page', 'Parent Areas');
		$this->template_data->set('current_uri', 'parents_areas');
		$this->template_data->set('navbar_search', true);

		$this->_isAuth('parents', 'parents', 'edit');

	}

	public function index($start=0) {
		
		$areas = new $this->Parents_areas_model;
		if( $this->input->get('q') ) {
			$areas->set_where('name LIKE "%' . $this->input->get('q') . '%"', NULL, 99);
		}
		
		if( $this->input->get('filter') == 'trash' ) {
			$areas->setTrash(1, true);
		} else {
			$areas->setTrash(0, true);
		}

		$areas->setCenterId($this->session->userdata('current_center_id'),true);
		
		$areas->set_select("*");
		$areas->set_select("(SELECT COUNT(*) FROM `parents` WHERE area_id=parents_areas.id) as parents_count");
		$areas->set_order('name', 'ASC');
		$areas->set_start($start);
		$this->template_data->set('areas', $areas->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => base_url($this->config->item('index_page') . '/parents_areas/index/'),
			'total_rows' => $areas->count_all_results(),
			'per_page' => $areas->get_limit(),
			'ajax'=>true,
		)));
		
		$this->load->view('parents/areas/areas_list', $this->template_data->get_data());
	}

	public function add($output='') {

		$this->_isAuth('parents', 'areas', 'add');

		if( $this->input->post() ) {
			$this->form_validation->set_rules('area_name', 'Area Name', 'trim|required');
			$this->form_validation->set_rules('notes', 'Notes', 'trim');
			if( $this->form_validation->run() ) {
				$areas = new $this->Parents_areas_model;
				$areas->setName($this->input->post('area_name'));
				$areas->setNotes($this->input->post('notes'));
				$areas->setCenterId($this->session->userdata('current_center_id'));
				if( $areas->insert() ) {
					record_system_audit($this->session->userdata('user_id'), 'parents', 'areas', 'add', $this->session->userdata('current_center_id'), "Parent Area Added : {$this->input->post('area_name')}");
				}
				$this->getNext("parents_areas");
			}
		}

		$this->template_data->set('output', $output);
		$this->load->view('parents/areas/areas_add', $this->template_data->get_data());
	}

	public function edit($id,$output='') {

		$this->_isAuth('parents', 'areas', 'edit');

		$areas = new $this->Parents_areas_model;
		$areas->setId($id,true);
		$area_data = $areas->get();

		if( $areas->nonEmpty() ) {
			if( $this->input->post() ) {
				$this->form_validation->set_rules('area_name', 'Area Name', 'trim|required');
				$this->form_validation->set_rules('notes', 'Notes', 'trim');
				if( $this->form_validation->run() ) {
					$areas->setName($this->input->post('area_name'),false,true);
					$areas->setNotes($this->input->post('notes'),false,true);
					if( $areas->update() ) {
						record_system_audit($this->session->userdata('user_id'), 'parents', 'areas', 'edit', $this->session->userdata('current_center_id'), "Parent Area Edited : {$area_data->name}");
					}
				}
				$this->postNext();
			}
		}

		$areas->set_select("*");
		$areas->set_select("(SELECT COUNT(*) FROM `parents` WHERE area_id=parents_areas.id) as parents_count");
		$this->template_data->set('area', $areas->get());

		$this->template_data->set('output', $output);
		$this->load->view('parents/areas/areas_edit', $this->template_data->get_data());
	}

	public function deactivate($id) {
		
		$this->_isAuth('parents', 'areas', 'delete');

		$areas = new $this->Parents_areas_model;
		$areas->setId($id,true,false);
		$areas->setTrash(1,false,true);
		$area_data = $areas->get();

		if( $areas->update() ) {
			record_system_audit($this->session->userdata('user_id'), 'parents', 'areas', 'delete', $this->session->userdata('current_center_id'), "Parent Area Deleted : {$area_data->name}");
		}

		$this->getNext("parents_areas");
	}


	public function restore($id) {
		
		$this->_isAuth('parents', 'areas', 'delete');

		$areas = new $this->Parents_areas_model;
		$areas->setId($id,true,false);
		$areas->setTrash(0,false,true);
		$area_data = $areas->get();

		if( $areas->update() ) {
			record_system_audit($this->session->userdata('user_id'), 'parents', 'areas', 'delete', $this->session->userdata('current_center_id'), "Parent Area Restored : {$area_data->name}");
		}

		$this->getNext("parents_areas");
	}

}
