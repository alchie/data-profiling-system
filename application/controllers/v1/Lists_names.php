<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lists_names extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->template_data->set('current_page', 'Names');
		$this->template_data->set('current_uri', 'lists_names');
		$this->template_data->set('navbar_search', false);

		$this->_isAuth('lists', 'names', 'view');
	}

	private function _searchRedirect($uri='lists_names') {
		if( $this->input->get('q') ) {
			redirect(site_url($uri."?q=" . $this->input->get('q') ));
		}
	}

	public function index($start=0) {
		
		if( $start > 0 ) {
			$this->_searchRedirect();
		}

		$names = new $this->Names_list_model('nl');

		if( $this->input->get('filter') == 'trash' ) {
			$names->setTrash(1, true);
		} else {
			$names->setTrash(0, true);
		}

		if( $this->input->get('q') ) {
			$names->set_where('nl.full_name LIKE "%' . $this->input->get('q') . '%"');
			$names->set_where_or('nl.address LIKE "%' . $this->input->get('q') . '%"');
		}

		$names->set_select("nl.*");
		$names->set_select("(SELECT COUNT(*) FROM benefeciaries e WHERE e.name_id=nl.id) as is_employed");
		$names->set_select("(SELECT name FROM centers_list c WHERE c.id=(SELECT e.center_id FROM benefeciaries e WHERE e.name_id=nl.id)) as center");

		$names->set_join("names_info ni", 'ni.name_id=nl.id');
		$names->set_select("(TIMESTAMPDIFF(YEAR, ni.birthday, CURDATE())) as age");

		$names->set_order('nl.full_name', 'ASC');
		$names->set_start($start);

		$this->template_data->set('names', $names->populate());
		$this->template_data->set('names_count', $names->count_all_results());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => base_url($this->config->item('index_page') . '/lists_names/index/'),
			'total_rows' => $names->count_all_results(),
			'per_page' => $names->get_limit(),
			'ajax'=>true,
		)));

		$this->load->view('lists/names/names_list', $this->template_data->get_data());
	}

	public function add($output='') {

		$this->_isAuth('lists', 'names', 'add');

		$this->template_data->set('output', $output);
		$name_id = false;
		if( $this->input->post() ) {
			$this->form_validation->set_rules('full_name', 'Full Name', 'trim|required|is_unique[names_list.full_name]');
			$this->form_validation->set_rules('address', 'Address', 'trim');
			$this->form_validation->set_rules('contact_number', 'Contact Number', 'trim');
			if( $this->form_validation->run() ) {
				$name = new $this->Names_list_model;
				$name->setFullName($this->input->post('full_name'), true);
				$name->setAddress($this->input->post('address'));
				$name->setContactNumber($this->input->post('contact_number'));
				$name->setTypeBenefeciary( ( ( $this->input->post('benefeciary') ) ? 1 : 0)  );
				$name->setTypeParent( ( ( $this->input->post('parent') ) ? 1 : 0 )  );
				$name->setTypeSponsor( ( ( $this->input->post('sponsor') ) ? 1 : 0 )  );
				$name->setTrash('0');
				if( ! $name->nonEmpty() ) {
					if( $name->insert() ) {
						$name_id = $name->get_inserted_id();
						record_system_audit($this->session->userdata('user_id'), 'lists', 'names', 'add', $this->session->userdata('current_center_id'), "Name Added: {$this->input->post('full_name')}", $name_id);
					}
					
				}
			}

			if( $this->input->get('next') ) {
					
					$uri = $this->input->get('next');
					$q = $this->input->get('q');

					$url = site_url( $uri ) . "?q={$q}" ;

					if( $name_id ) {
						$url = str_replace('$new_id', $name_id, $url);
					}
                    
                    if( $name_id ) {
                    	$url .= "&error_code=340&new_name=" . $name_id;
                    } else {
                    	$url .= "&error_code=341";
                    }

                    redirect( $url );

            } else {

            	if( $name_id ) {
            		redirect( "lists_names/edit/" . $name_id );
            	}
            	
            }
		}
		$this->load->view('lists/names/names_add', $this->template_data->get_data());
	}

	public function edit($id,$output='') {

		$this->_isAuth('lists', 'names', 'edit');

		$this->template_data->set('output', $output);

		$name = new $this->Names_list_model;
		$name->setId($id, true);
		$name_data = $name->get();

		if( $this->input->post() ) {
			$this->form_validation->set_rules('full_name', 'Full Name', 'trim|required');
			$this->form_validation->set_rules('address', 'Address', 'trim');
			$this->form_validation->set_rules('contact_number', 'Contact Number', 'trim');
			if( $this->form_validation->run() ) {
				$name->setFullName($this->input->post('full_name'),false,true);
				$name->setAddress($this->input->post('address'),false,true);
				$name->setContactNumber($this->input->post('contact_number'),false,true);
				$name->setTypeBenefeciary( ( ( $this->input->post('benefeciary') ) ? 1 : 0) ,false,true );
				$name->setTypeParent( ( ( $this->input->post('parent') ) ? 1 : 0 ) ,false,true );
				$name->setTypeSponsor( ( ( $this->input->post('sponsor') ) ? 1 : 0 ) ,false,true );
				if( $name->nonEmpty() ) {
					//$name->set_exclude('id');
					if( $name->update() ) {
						record_system_audit($this->session->userdata('user_id'), 'lists', 'names', 'edit', $this->session->userdata('current_center_id'), "Name Edited: {$name_data->full_name}", $id);
					}
				} 
			}
			$this->postNext( "q=" . $this->input->get('q') );
		}

		$name->set_select("names_list.*");

		$this->template_data->set('name', $name->get());
		$this->template_data->set('next_item', $this->_next_name($id, 'lists_names/edit/'));
		$this->template_data->set('previous_item', $this->_previous_name($id, 'lists_names/edit/'));
		
		$this->load->view('lists/names/names_edit', $this->template_data->get_data());
	}

	public function delete($id) {
		
		$this->_isAuth('lists', 'names', 'delete');
		$this->_isAuth('lists', 'names', 'edit');

		$name = new $this->Names_list_model;
		$name->setId($id, true,false);
		$name->setTrash('1',false,true);
		$name_data = $name->get();

		if( $name->update() ) {
			record_system_audit($this->session->userdata('user_id'), 'lists', 'names', 'delete', $this->session->userdata('current_center_id'), "Name Deactivated: {$name_data->full_name}", $id);
		}

		redirect( "lists_names" );
	}

	public function deactivate($id) {
		
		$this->_isAuth('lists', 'names', 'delete');
		$this->_isAuth('lists', 'names', 'edit');

		$name = new $this->Names_list_model;
		$name->setId($id, true,false);
		$name->setTrash('1',false,true);

		$name_data = $name->get();

		if( $name->update() ) {
			record_system_audit($this->session->userdata('user_id'), 'lists', 'names', 'deactivate', $this->session->userdata('current_center_id'), "Name Deactivated: {$name_data->full_name}", $id);
		}

		redirect( "lists_names" );
	}

	public function restore($id) {
		
		$this->_isAuth('lists', 'names', 'delete');
		$this->_isAuth('lists', 'names', 'edit');

		$name = new $this->Names_list_model;
		$name->setId($id, true,false);
		$name->setTrash('0',false,true);
		$name_data = $name->get();
		
		if( $name->update() ) {
			record_system_audit($this->session->userdata('user_id'), 'lists', 'names', 'restore', $this->session->userdata('current_center_id'), "Name Restored: {$name_data->full_name}", $id);
		}

		redirect( "lists_names" );
	}

	public function profile($id,$output='') {

		$this->_isAuth('lists', 'names', 'edit');

		$this->template_data->set('output', $output);

		$name = new $this->Names_list_model('nl');
		$name->setId($id, true);
		$name->set_select("nl.*");
		
		$name->set_join("names_info ni", "ni.name_id=nl.id");
		$name->set_select("ni.*");
		$name->set_select("(SELECT COUNT(*) FROM benefeciaries e WHERE e.name_id=nl.id AND e.trash=0) as is_employed");
		$name->set_select("(SELECT e.center_id FROM benefeciaries e WHERE e.name_id=nl.id) as center_id");
		$name->set_select("(SELECT name FROM centers_list c WHERE c.id=(SELECT e.center_id FROM benefeciaries e WHERE e.name_id=nl.id)) as company");
		$name->set_select("(TIMESTAMPDIFF(YEAR, ni.birthday, CURDATE())) as age");
		
		foreach(array('address', 'email', 'phone_number', 'cell_smart', 'cell_globe', 'cell_sun') as $k) {
			$data = new $this->Names_meta_model('d');
			$data->setMetaKey($k,true);
			$data->set_select('d.meta_value');
			$data->set_where('d.name_id=nl.id');
			$name->set_select('('.$data->get_compiled_select().') as ' . $k);
		}

		foreach(array('facebook_id','twitter_id','instagram_id','skype_id','yahoo_id','google_id') as $k) {
			$data = new $this->Names_meta_model('d');
			$data->setMetaKey($k,true);
			$data->set_select('d.meta_value');
			$data->set_where('d.name_id=nl.id');
			$name->set_select('('.$data->get_compiled_select().') as ' . $k);
		}

		foreach(array('tin','sss','hdmf','phic','drivers_license','voters_number') as $k) {
			$data = new $this->Names_meta_model('d');
			$data->setMetaKey($k,true);
			$data->set_select('d.meta_value');
			$data->set_where('d.name_id=nl.id');
			$name->set_select('('.$data->get_compiled_select().') as ' . $k);
		}

		foreach(array('emergency_name','emergency_address', 'emergency_relationship', 'emergency_contact') as $k) {
			$data = new $this->Names_meta_model('d');
			$data->setMetaKey($k,true);
			$data->set_select('d.meta_value');
			$data->set_where('d.name_id=nl.id');
			$name->set_select('('.$data->get_compiled_select().') as ' . $k);
		}

		$name_data = $name->get();	
		$this->template_data->set('name', $name_data);

		if( $name_data->is_employed ) {
			
			$benefeciary = new $this->Benefeciaries_model('e');
			$benefeciary->setNameId($id,true);
			$benefeciary->set_select('*');
			$benefeciary->set_select('(SELECT name FROM benefeciaries_groups WHERE id=e.group_id) as group_name');
			$benefeciary->set_select('(SELECT name FROM benefeciaries_areas WHERE id=e.area_id) as area_name');
			$benefeciary->set_select('(SELECT name FROM terms_list WHERE id=e.status) as status_name');
			$benefeciary->set_select("(TIMESTAMPDIFF(YEAR, e.accepted, CURDATE())) as years_service");
			$this->template_data->set('benefeciary', $benefeciary->get());
			
		}

		$this->template_data->set('next_item', $this->_next_name($id, 'lists_names/profile/'));
		$this->template_data->set('previous_item', $this->_previous_name($id, 'lists_names/profile/'));

		if( $output == 'ajax' ) {
			$this->load->view('lists/names/names_profile_ajax', $this->template_data->get_data());
		} else {
			$this->load->view('lists/names/names_profile', $this->template_data->get_data());
		}
	}

	public function update_personal($id,$output='') {

		$this->_isAuth('lists', 'names', 'edit');

		$info = new $this->Names_info_model;
		$info->setNameId($id,true);

		if( $this->input->post() ) {
			$this->form_validation->set_rules('lastname', 'Last Name', 'trim|required');
			$this->form_validation->set_rules('firstname', 'First Name', 'trim|required');
			$this->form_validation->set_rules('middlename', 'Middle Name', 'trim');
			if( $this->form_validation->run() ) {
				$info->setLastname($this->input->post('lastname'),false,true);
				$info->setFirstname($this->input->post('firstname'),false,true);
				$info->setMiddlename($this->input->post('middlename'),false,true);
				$info->setBirthday( date("Y-m-d", strtotime($this->input->post('birthday'))),false,true);
				$info->setBirthplace($this->input->post('birthplace'),false,true);
				$info->setGender($this->input->post('gender'),false,true);
				$info->setCivilStatus($this->input->post('civil_status'),false,true);
				$info->setPrefix($this->input->post('prefix'),false,true);
				$info->setSuffix($this->input->post('suffix'),false,true);
				if( $info->nonEmpty() ) {
					if( $info->update() ) {
						record_system_audit($this->session->userdata('user_id'), 'lists', 'names', 'update_personal', $this->session->userdata('current_center_id'), "Personal Info Updated", $id);
					}
				} else {
					$info->setNameId($id,true,true);
					if( $info->insert() ) {
						record_system_audit($this->session->userdata('user_id'), 'lists', 'names', 'update_personal', $this->session->userdata('current_center_id'), "Personal Info Updated", $id);
					}
				}
			}
			$this->postNext("active=personal");
		}

		$this->template_data->set('info', $info->get());

		$this->template_data->set('output', $output);
		$this->load->view('lists/names/names_update_personal', $this->template_data->get_data());
	}

	public function update_contacts($id,$output='') {

		$this->_isAuth('lists', 'names', 'edit');

		if( $this->input->post('data') ) {
			foreach($this->input->post('data') as $key=>$value) {
				
				$meta = new $this->Names_meta_model;
				$meta->setNameId($id,true);
				$meta->setMetaKey($key,true);
				if( !empty( $value ) ) {
					if( $meta->nonEmpty() ) {
						$meta->setMetaValue($value,false,true);
						if( $meta->update() ) {
							record_system_audit($this->session->userdata('user_id'), 'lists', 'names', 'update_contacts', $this->session->userdata('current_center_id'), "Contact Info Updated", $id);
						}
					} else {
						$meta->setMetaValue($value);
						if( $meta->insert() ) {
							record_system_audit($this->session->userdata('user_id'), 'lists', 'names', 'update_contacts', $this->session->userdata('current_center_id'), "Contact Info Updated", $id);
						}
					}
				} else {
					if( $meta->delete() ) {
						record_system_audit($this->session->userdata('user_id'), 'lists', 'names', 'update_contacts', $this->session->userdata('current_center_id'), "Contact Info Updated", $id);
					}
				}
			}
			$this->postNext("active=contacts");
		}

		$meta = new $this->Names_meta_model('nc');
		$meta->setNameId($id,true);
		$meta->setMetaKey('address',true);
		$meta->set_select('nc.meta_value as address');

		foreach(array('email', 'phone_number', 'cell_smart', 'cell_globe', 'cell_sun') as $k) {
			$data = new $this->Names_meta_model('d');
			$data->setMetaKey($k,true);
			$data->set_select('d.meta_value');
			$data->set_where('d.name_id=nc.name_id');
			$meta->set_select('('.$data->get_compiled_select().') as ' . $k);
		}

		$this->template_data->set('meta', $meta->get());

		$this->template_data->set('output', $output);
		$this->load->view('lists/names/names_update_contacts', $this->template_data->get_data());
	}

	public function update_social_media($id,$output='') {

		$this->_isAuth('lists', 'names', 'edit');

		if( $this->input->post('data') ) {
			foreach($this->input->post('data') as $key=>$value) {
				
				$meta = new $this->Names_meta_model;
				$meta->setNameId($id,true);
				$meta->setMetaKey($key,true);
				if( !empty( $value ) ) {
					if( $meta->nonEmpty() ) {
						$meta->setMetaValue($value,false,true);
						if( $meta->update() ) {
							record_system_audit($this->session->userdata('user_id'), 'lists', 'names', 'update_social_media', $this->session->userdata('current_center_id'), "Social Media Updated", $id);
						}
					} else {
						$meta->setMetaValue($value);
						if( $meta->insert() ) {
							record_system_audit($this->session->userdata('user_id'), 'lists', 'names', 'update_social_media', $this->session->userdata('current_center_id'), "Social Media Updated", $id);
						}
					}
				} else {
					if( $meta->delete() ) {
						record_system_audit($this->session->userdata('user_id'), 'lists', 'names', 'update_social_media', $this->session->userdata('current_center_id'), "Social Media Updated", $id);
					}
				}
			}
			$this->postNext("active=social_media");
		}

		$meta = new $this->Names_meta_model('nc');
		$meta->setNameId($id,true);
		$meta->setMetaKey('facebook_id',true);
		$meta->set_select('nc.meta_value as facebook_id');

		foreach(array('twitter_id','instagram_id','skype_id','yahoo_id','google_id') as $k) {
			$data = new $this->Names_meta_model('d');
			$data->setMetaKey($k,true);
			$data->set_select('d.meta_value');
			$data->set_where('d.name_id=nc.name_id');
			$meta->set_select('('.$data->get_compiled_select().') as ' . $k);
		}

		$this->template_data->set('meta', $meta->get());

		$this->template_data->set('output', $output);
		$this->load->view('lists/names/names_update_social_media', $this->template_data->get_data());
	}

	public function update_ids($id,$output='') {

		$this->_isAuth('lists', 'names', 'edit');

		if( $this->input->post('data') ) {
			foreach($this->input->post('data') as $key=>$value) {
				
				$meta = new $this->Names_meta_model;
				$meta->setNameId($id,true);
				$meta->setMetaKey($key,true);
				if( !empty( $value ) ) {
					if( $meta->nonEmpty() ) {
						$meta->setMetaValue($value,false,true);
						if( $meta->update() ) {
							record_system_audit($this->session->userdata('user_id'), 'lists', 'names', 'update_ids', $this->session->userdata('current_center_id'), "ID Info Updated", $id);
						}
					} else {
						$meta->setMetaValue($value);
						if( $meta->insert() ) {
							record_system_audit($this->session->userdata('user_id'), 'lists', 'names', 'update_ids', $this->session->userdata('current_center_id'), "ID Info Updated", $id);
						}
					}
				} else {
					if( $meta->delete() ) {
						record_system_audit($this->session->userdata('user_id'), 'lists', 'names', 'update_ids', $this->session->userdata('current_center_id'), "ID Info Updated", $id);
					}
				}
			}
			$this->postNext("active=ids");
		}

		$meta = new $this->Names_meta_model('nc');
		$meta->setNameId($id,true);
		//$meta->setMetaKey('tin',true);
		//$meta->set_select('nc.meta_value as tin');

		foreach(array('tin', 'sss','hdmf','phic','drivers_license','voters_number') as $k) {
			$data = new $this->Names_meta_model('d');
			$data->setMetaKey($k,true);
			$data->set_select('d.meta_value');
			$data->set_where('d.name_id=nc.name_id');
			$meta->set_select('('.$data->get_compiled_select().') as ' . $k);
		}

		$this->template_data->set('meta', $meta->get());

		$this->template_data->set('output', $output);
		$this->load->view('lists/names/names_update_ids', $this->template_data->get_data());
	}

	public function update_emergency($id,$output='') {

		$this->_isAuth('lists', 'names', 'edit');

		if( $this->input->post('data') ) {
			foreach($this->input->post('data') as $key=>$value) {
				
				$meta = new $this->Names_meta_model;
				$meta->setNameId($id,true);
				$meta->setMetaKey($key,true);
				if( !empty( $value ) ) {
					if( $meta->nonEmpty() ) {
						$meta->setMetaValue($value,false,true);
						if( $meta->update() ) {
							record_system_audit($this->session->userdata('user_id'), 'lists', 'names', 'update_emergency', $this->session->userdata('current_center_id'), "Emergency Updated", $id);
						}
					} else {
						$meta->setMetaValue($value);
						if( $meta->insert() ) {
							record_system_audit($this->session->userdata('user_id'), 'lists', 'names', 'update_emergency', $this->session->userdata('current_center_id'), "Emergency Updated", $id);
						}
					}
				} else {
					if( $meta->delete() ) {
						record_system_audit($this->session->userdata('user_id'), 'lists', 'names', 'update_emergency', $this->session->userdata('current_center_id'), "Emergency Updated", $id);
					}
				}
			}
			$this->postNext("active=emergency");
		}

		$meta = new $this->Names_meta_model('nc');
		$meta->setNameId($id,true);
		$meta->setMetaKey('emergency_name',true);
		$meta->set_select('nc.meta_value as emergency_name');

		foreach(array('emergency_address', 'emergency_relationship', 'emergency_contact') as $k) {
			$data = new $this->Names_meta_model('d');
			$data->setMetaKey($k,true);
			$data->set_select('d.meta_value');
			$data->set_where('d.name_id=nc.name_id');
			$meta->set_select('('.$data->get_compiled_select().') as ' . $k);
		}

		$this->template_data->set('meta', $meta->get());

		$this->template_data->set('output', $output);
		$this->load->view('lists/names/names_update_emergency', $this->template_data->get_data());
	}	

	public function update_security_guard_license($id,$output='') {

		$this->_isAuth('lists', 'names', 'edit');

		if( $this->input->post('data') ) {
			foreach($this->input->post('data') as $key=>$value) {
				
				$meta = new $this->Names_meta_model;
				$meta->setNameId($id,true);
				$meta->setMetaKey($key,true);
				if( !empty( $value ) ) {
					if( $meta->nonEmpty() ) {
						$meta->setMetaValue($value,false,true);
						if( $meta->update() ) {
							record_system_audit($this->session->userdata('user_id'), 'lists', 'names', 'update_security_guard_license', $this->session->userdata('current_center_id'), "Security Guard License Updated: {$key}", $id);
						}
					} else {
						$meta->setMetaValue($value);
						if( $meta->insert() ) {
							record_system_audit($this->session->userdata('user_id'), 'lists', 'names', 'update_security_guard_license', $this->session->userdata('current_center_id'), "Security Guard License Added: {$key}", $id);
						}
					}
				} else {
					if( $meta->delete() ) {
						record_system_audit($this->session->userdata('user_id'), 'lists', 'names', 'update_security_guard_license', $this->session->userdata('current_center_id'), "Security Guard License Removed: {$key}", $id);
					}
				}
			}
			$this->postNext("active=security_guard_license");
		}

		$meta = new $this->Names_meta_model('nc');
		$meta->setNameId($id,true);
		$meta->setMetaKey('sgl_number',true);
		$meta->set_select('nc.meta_value as sgl_number');

		foreach(array('sgl_expiry', 'sgl_status') as $k) {
			$data = new $this->Names_meta_model('d');
			$data->setMetaKey($k,true);
			$data->set_select('d.meta_value');
			$data->set_where('d.name_id=nc.name_id');
			$meta->set_select('('.$data->get_compiled_select().') as ' . $k);
		}

		$this->template_data->set('meta', $meta->get());

		$this->template_data->set('output', $output);
		$this->load->view('lists/names/names_update_security_guard_license', $this->template_data->get_data());
	}

	public function birthdays($company_id, $start=0) {
		
		$this->template_data->set('company_id', $company_id);

		if( $start > 0 ) {
			$this->_searchRedirect("lists_names/birthdays/" . $company_id);
		}

		$names = new $this->Names_list_model('nl');
		$names->setTrash(0, true);

		if( $this->input->get('q') ) {
			$names->set_where('nl.full_name LIKE "%' . $this->input->get('q') . '%"');
			$names->set_where_or('nl.address LIKE "%' . $this->input->get('q') . '%"');
		}

		$names->set_select("nl.*");
		$names->set_select("ni.*");
		$names->set_select("(SELECT COUNT(*) FROM employees e WHERE e.name_id=nl.id) as is_employed");

		$names->set_join("names_info ni", 'ni.name_id=nl.id');
		$names->set_join("employees e", 'e.name_id=nl.id');
		$names->set_select("(TIMESTAMPDIFF(YEAR, ni.birthday, CURDATE())) as age");

		$names->set_order('(MONTH(ni.birthday))', 'ASC');
		$names->set_order('(DAY(ni.birthday))', 'ASC');
		$names->set_where('e.company_id=' . $company_id);

		if( $this->input->get('month') ) {
			$names->set_where('((MONTH(ni.birthday))=' . $this->input->get('month') .')');
			$names->set_limit(0);
		} else {
			$names->set_start($start);
		}
		$names->set_start($start);
		$names->setTrash('0',true);

		$this->template_data->set('names', $names->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => base_url($this->config->item('index_page') . '/lists_names/birthdays/' . $company_id),
			'total_rows' => $names->count_all_results(),
			'per_page' => $names->get_limit(),
			'ajax'=>true,
		)));

		$this->load->view('lists/names/names_birthdays', $this->template_data->get_data());
	}

	public function report($company_id=0) {
/*
		$companies = new $this->Companies_list_model;
		$companies->set_select("*");
		$companies->set_order('name', 'ASC');
		$companies->set_limit(0);
		$companies->setTrash('0',true);
		$this->template_data->set('companies', $companies->populate());
*/
		$names = new $this->Names_list_model('nl');
		$names->setTrash(0, true);
		$names->set_select("nl.*");
		$names->set_select("ni.*");
		$names->set_select("ni.name_id as ni_name_id");
		$names->set_select("(SELECT e.company_id FROM employees e WHERE e.name_id=nl.id) as company_id");
		$names->set_select("(SELECT name FROM companies_list c WHERE c.id=(SELECT e.company_id FROM employees e WHERE e.name_id=nl.id)) as company");
		$names->set_join("names_info ni", 'ni.name_id=nl.id');
		$names->set_select("(TIMESTAMPDIFF(YEAR, ni.birthday, CURDATE())) as age");
		$names->set_order('nl.full_name', 'ASC');
		$names->set_limit(0);

		// meta
		$names->set_select("(SELECT m.meta_value FROM names_meta m WHERE m.name_id=nl.id AND m.meta_key='address') as meta_address");
		$names->set_select("(SELECT m.meta_value FROM names_meta m WHERE m.name_id=nl.id AND m.meta_key='phone_number') as meta_phone_number");
		$names->set_select("(SELECT m.meta_value FROM names_meta m WHERE m.name_id=nl.id AND m.meta_key='cell_smart') as meta_cell_smart");
		$names->set_select("(SELECT m.meta_value FROM names_meta m WHERE m.name_id=nl.id AND m.meta_key='cell_globe') as meta_cell_globe");
		$names->set_select("(SELECT m.meta_value FROM names_meta m WHERE m.name_id=nl.id AND m.meta_key='cell_sun') as meta_cell_sun");

		$this->template_data->set('names', $names->populate());

		$this->load->view('lists/names/names_report', $this->template_data->get_data());
	}

	public function multi_edit($module=NULL, $limit=10, $start=0) {
		
		$modules = array(
			'personal_info' => 'Personal Information',
			'benefeciary_info' => 'Benefeciary Information',
			'contact_info' => 'Address & Contact Numbers',
			'social_media' => 'Social Media Accounts',
			'id_numbers' => 'Identification Numbers',
			'emergency' => 'Emergency Information',
		);
		$this->template_data->set('modules', $modules);

		$names = new $this->Names_list_model('nl');
		$names->set_select("nl.*");
		$names->setTrash(0, true);
		$names->set_order('nl.full_name', 'ASC');
		$names->set_start($start);
		$names->set_limit($limit);
		$names->set_where("((SELECT e.center_id FROM benefeciaries e WHERE e.name_id=nl.id) = {$this->session->userdata('current_center_id')})");

		if( $this->input->post('input') ) {
			switch($module) {
				case 'benefeciary_info': 
					foreach( $this->input->post('input') as $name_id => $input_values ) { 
						//print_r( $input_values ); exit;
						$benefeciary = new $this->Benefeciaries_model;
						$benefeciary->setNameId($name_id,true,false);
						//print_r( $benefeciary->get() ); exit;
						foreach($input_values as $key=>$value) {
							if( $value != '') {
								if(($key=='accepted')||($key=='graduated')) {
									$value=date('Y-m-d', strtotime($value));
								}
								$benefeciary->set_field_value($key,$value,false,true);
							} else {
								$benefeciary->set_field_value($key,'',false,true);
							}
						}
						if( $benefeciary->nonEmpty() ) {
							$benefeciary->update();
						}
					}
				break;
				case 'emergency': 
					foreach( $this->input->post('input') as $name_id => $input_values ) {
						foreach($input_values as $key=>$value) {
							$meta = new $this->Names_meta_model;
							$meta->setNameId($name_id,true);
							$meta->setMetaKey($key,true);
							if( !empty( $value ) ) {
								if( $meta->nonEmpty() ) {
									$meta->setMetaValue($value,false,true);
									$meta->update();
								} else {
									$meta->setMetaValue($value);
									$meta->insert();
								}
							} else {
								$meta->delete();
							}
						}
					}
				break;
				case 'id_numbers': 
					foreach( $this->input->post('input') as $name_id => $input_values ) {
						foreach($input_values as $key=>$value) {
							$meta = new $this->Names_meta_model;
							$meta->setNameId($name_id,true);
							$meta->setMetaKey($key,true);
							if( !empty( $value ) ) {
								if( $meta->nonEmpty() ) {
									$meta->setMetaValue($value,false,true);
									$meta->update();
								} else {
									$meta->setMetaValue($value);
									$meta->insert();
								}
							} else {
								$meta->delete();
							}
						}
					}
				break;
				case 'social_media': 
					foreach( $this->input->post('input') as $name_id => $input_values ) {
						foreach($input_values as $key=>$value) {
							$meta = new $this->Names_meta_model;
							$meta->setNameId($name_id,true);
							$meta->setMetaKey($key,true);
							if( !empty( $value ) ) {
								if( $meta->nonEmpty() ) {
									$meta->setMetaValue($value,false,true);
									$meta->update();
								} else {
									$meta->setMetaValue($value);
									$meta->insert();
								}
							} else {
								$meta->delete();
							}
						}
					}
				break;
				case 'contact_info': 
					foreach( $this->input->post('input') as $name_id => $input_values ) {
						foreach($input_values as $key=>$value) {
							$meta = new $this->Names_meta_model;
							$meta->setNameId($name_id,true);
							$meta->setMetaKey($key,true);
							if( !empty( $value ) ) {
								if( $meta->nonEmpty() ) {
									$meta->setMetaValue($value,false,true);
									$meta->update();
								} else {
									$meta->setMetaValue($value);
									$meta->insert();
								}
							} else {
								$meta->delete();
							}
						}
					}
				break;
				case 'personal_info':
				default:
					foreach( $this->input->post('input') as $name_id => $input_values ) {
						$info = new $this->Names_info_model;
						$info->setNameId($name_id,true,false);
						foreach($input_values as $key=>$value) {
							if( $value != '') {
								if( $key=='birthday') {
									if( $value != '') {
										$value=date('Y-m-d', strtotime($value));
									}
								}
								$info->set_field_value($key,$value,false,true);
							} else {
								$info->set_field_value($key,'',false,true);
							}
						}
						if( $info->nonEmpty() ) {
							$info->update();
						} else {
							$info->setNameId($name_id,true,true);
							$info->insert();
						}
					}
				break;
			}
		}

		switch($module) {
			case 'benefeciary_info': 
				$module = 'benefeciary_info';
				$names->set_join("benefeciaries b", 'b.name_id=nl.id');
				$names->set_select("b.*");

				$groups = new $this->Benefeciaries_groups_model;
				$groups->set_limit(0);
				$groups->set_order('name');
				$this->template_data->set('groups', $groups->populate());

				$areas = new $this->Benefeciaries_areas_model;
				$areas->set_limit(0);
				$areas->set_order('name');
				$this->template_data->set('areas', $areas->populate());

				$status = new $this->Terms_list_model;
				$status->set_limit(0);
				$status->setType('benefeciary_status',true);
				$status->set_order('name');
				$this->template_data->set('statuses', $status->populate());

			break;
			case 'emergency': 
				foreach(array('emergency_name','emergency_address', 'emergency_relationship', 'emergency_contact') as $k) {
					$data = new $this->Names_meta_model('d');
					$data->setMetaKey($k,true);
					$data->set_select('d.meta_value');
					$data->set_where('d.name_id=nl.id');
					$names->set_select('('.$data->get_compiled_select().') as ' . $k);
				}
				$module = 'emergency';
			break;
			case 'id_numbers': 
				foreach(array('tin','sss','hdmf','phic','drivers_license','voters_number') as $k) {
					$data = new $this->Names_meta_model('d');
					$data->setMetaKey($k,true);
					$data->set_select('d.meta_value');
					$data->set_where('d.name_id=nl.id');
					$names->set_select('('.$data->get_compiled_select().') as ' . $k);
				}
				$module = 'id_numbers';
			break;
			case 'social_media': 
				foreach(array('facebook_id','twitter_id','instagram_id','skype_id','yahoo_id','google_id') as $k) {
					$data = new $this->Names_meta_model('d');
					$data->setMetaKey($k,true);
					$data->set_select('d.meta_value');
					$data->set_where('d.name_id=nl.id');
					$names->set_select('('.$data->get_compiled_select().') as ' . $k);
				}
				$module = 'social_media';
			break;
			case 'contact_info': 
				foreach(array('address', 'email', 'phone_number', 'cell_smart', 'cell_globe', 'cell_sun') as $k) {
					$data = new $this->Names_meta_model('d');
					$data->setMetaKey($k,true);
					$data->set_select('d.meta_value');
					$data->set_where('d.name_id=nl.id');
					$names->set_select('('.$data->get_compiled_select().') as ' . $k);
				}
				$module = 'contact_info';
			break;
			case 'personal_info':
			default:
				$names->set_join("names_info ni", 'ni.name_id=nl.id');
				$names->set_select("ni.*");
				$module = 'personal_info';
			break;
		}

		$this->template_data->set('names', $names->populate());
		$this->template_data->set('names_count', $names->count_all_results());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 5,
			'base_url' => base_url($this->config->item('index_page') . "/lists_names/multi_edit/{$module}/{$limit}" ),
			'total_rows' => $names->count_all_results(),
			'per_page' => $names->get_limit(),
			'ajax'=>true,
		)));

		$this->template_data->set('module', $module);

		$this->load->view('lists/names/multi_edit/' . $module, $this->template_data->get_data());
	}


	public function import($output="") {

				$config['upload_path'] = './uploads/';
                $config['allowed_types'] = 'csv';
                $config['max_size'] = 600;

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('import_file') )
                {
                        $error = array('error' => $this->upload->display_errors());

                        $this->template_data->set('output', $output);
						$this->load->view('lists/names/names_import', $this->template_data->get_data());
						if( ($this->input->post()) && $output == "ajax") {
							redirect( site_url("lists_names/import") . "?next=" . $this->input->get('next') );
						}
                }
                else
                {
                		$error = array('error' => $this->upload->display_errors());
                        $upload_data = $this->upload->data();

                        record_system_audit($this->session->userdata('user_id'), 'lists', 'names', 'import', $this->session->userdata('current_company_id'), "Import CSV File: " . $upload_data['file_name'], "");

                        $this->_process_csv_file( $upload_data['full_path'] );

                        unlink( $upload_data['full_path'] );
                        $this->getNext();
                }

		
	}

	private function _process_csv_file($csv_fullpath) {

		$csv = array_map("str_getcsv", file( $csv_fullpath ));
		$fields = array();
		foreach($csv as $index => $line) {
			if( $index == 0 ) {
				$fields = $line;
			} else {
				$name = new $this->Names_list_model;
				foreach($line as $key=>$value) {
					if( ($key == 0) && ($value != '') ) {
						$name->set_field_value($fields[$key], $value, true);
					} else {
						$name->set_field_value($fields[$key], $value);
					}
				}
				if( $name->nonEmpty() ) {
					$name->update();
				} else {
					$name->insert();
				}
			}
		}
		
	}

}
