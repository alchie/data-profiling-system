<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('calendar/calendar_navbar'); ?>

<div class="container">
<div class="row">

  <div class="col-md-6 col-md-offset-3">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Add Activity</h3>
        </div>
        <form method="post">
        <div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>
          
          <div class="list-group">
            <?php foreach($activities as $activity) { ?>
              <label class="list-group-item">
<input type="checkbox" name="activity_id[]" value="<?php echo $activity->id; ?>" <?php echo (in_array($activity->id, explode("-", $ids))) ? 'CHECKED' : ''; ?> />
                <?php echo $activity->name; ?> <span class="badge"><?php echo date("F d, Y - l", strtotime($activity->activity_date)); ?></span></label>
            <?php } ?>
          </div>
          
<?php if( isset($output) && ($output!='ajax') ) : ?>

        </div>
        <div class="panel-footer">
          <button type="submit" class="btn btn-success">Submit</button>
          <a href="<?php echo site_url($current_uri); ?>" class="btn btn-warning">Back</a>
        </div>
        </form>
      </div>
    </div>
</div>
</div>
<?php $this->load->view('footer'); ?>
<?php endif; ?>