<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('calendar/calendar_navbar'); ?>

<div class="container">
<div class="row">

  <div class="col-md-6 col-md-offset-3">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Edit Activity</h3>
        </div>
        <form method="post">
        <div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>
          <div class="form-group">
            <label>Activity Name</label>
            <input name="activity_name" type="text" class="form-control" value="<?php echo $activity->name; ?>">
          </div>

          <div class="form-group">
            <label>Notes</label>
            <textarea name="notes" class="form-control" rows="10"><?php echo $activity->notes; ?></textarea>
          </div>

          <div class="form-group">
            <label>Service</label>
            <select name="service_id" class="form-control">
              <?php foreach($services as $serv) { ?>
                <option value="<?php echo $serv->id; ?>" <?php echo ($serv->id==$activity->service_id) ? 'SELECTED' : ''; ?>><?php echo $serv->name; ?></option>
              <?php } ?>
            </select>
          </div>
          
          <div class="form-group">
            <label><input name="benefeciaries" type="checkbox" value="1" <?php echo ($activity->benefeciaries) ? 'CHECKED' : ''; ?>> Benefeciaries</label>
            <label><input name="parents" type="checkbox" value="1" <?php echo ($activity->parents) ? 'CHECKED' : ''; ?>> Parents</label>
          </div>

<?php if( isset($output) && ($output!='ajax') ) : ?>

        </div>
        <div class="panel-footer">
          <button type="submit" class="btn btn-success">Submit</button>
          <a href="<?php echo site_url($current_uri); ?>" class="btn btn-warning">Back</a>
        </div>
        </form>
      </div>
    </div>
</div>
</div>
<?php $this->load->view('footer'); ?>
<?php endif; ?>