<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
<?php if( ! $inner_page ): ?>
<?php $this->load->view('calendar/calendar_navbar'); ?>

<div class="container">
    <div class="row">
            <div class="col-md-12">

<?php if( $birthdays ) { ?>
  <div class="alert alert-success" role="alert"><span class="fa fa-birthday-cake"></span> <strong>Happy Birthday to:</strong> 
    <?php foreach($birthdays as $birthday) { ?>
      <a href="<?php echo site_url("lists_names/profile/{$birthday->name_id}"); ?>" class="body_wrapper"><?php echo $birthday->firstname; ?> <?php echo $birthday->lastname; ?></a>,
    <?php } ?>
  </div>
<?php } ?>

              <div class="panel panel-default">
                <div class="panel-heading">

<div class="row">
<div class="col-md-9 col-sm-6">
          <h3 class="panel-title bold"><?php echo $current_page . " (".date('F d, Y - l', strtotime($current_day)).")"; ?>
                    <?php if( $this->input->get('q') ) { ?>
                    <span class="badge"><?php echo $this->input->get('q'); ?> <a href="<?php echo site_url(uri_string()); ?>"><span class="glyphicon glyphicon-remove"></span></a></span>
                    <?php } ?>

                    <a href="<?php echo site_url("activities/index/" . date('Y-m-d', strtotime($current_day . "- 1 day") ) ); ?>"><span class="glyphicon glyphicon-arrow-left"></span></a>
                    <a href="<?php echo site_url("activities/index/" . date('Y-m-d', strtotime($current_day . "+ 1 day") ) ); ?>"><span class="glyphicon glyphicon-arrow-right"></span></a>
          </h3>
</div>
<div class="col-md-3">
<form method="get" action="<?php echo site_url("activities/index/{$current_day}"); ?>">
<div class="input-group input-group-sm">
  <input type="text" name="q" class="form-control" placeholder="Search for..." value="<?php echo $this->input->get('q'); ?>">
  <span class="input-group-btn">
    <button class="btn btn-default" type="submit">Search</button>
<?php if( hasAccess('activities', 'activities', 'add') ) { ?>
  <button type="button" class="btn btn-success ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Add Activity <?php echo "(".date('F d, Y - l', strtotime($current_day)).")"; ?>" data-url="<?php echo site_url("activities/add/{$current_day}/ajax") . "?next=" . uri_string(); ?>" style="margin-right: 5px">Add Activity</button>
<?php } ?>

  </span>
</div><!-- /input-group -->
</form>
</div>
</div>

                </div>
                <div class="panel-body" id="ajaxBodyInnerPage">
<?php endif; ?>
<?php if( $activities ) { ?>

          <table class="table table-default table-hover">
            <thead>
              <tr>
                <th>Activity Name</th>
                <th>Notes</th>
                <th width="1px">B</th>
                <th  width="1px">P</th>
                <?php if( hasAccess('activities', 'activities', 'edit') ) { ?>
                  <th width="180px" class="text-right">Action</th>
                <?php } ?>
              </tr>
            </thead>
            <tbody>

            <?php foreach($activities as $activity) { ?>
              <tr id="employee-group-<?php echo $activity->id; ?>">
                <td><?php echo $activity->name; ?></td>
                <td><?php echo $activity->notes; ?></td>
                <td><?php echo ($activity->benefeciaries==1) ? '<i class="fa fa-check" style="color:green"></i>' : '<i class="fa fa-remove" style="color:red"></i>'; ?></td>
                <td><?php echo ($activity->parents==1) ? '<i class="fa fa-check" style="color:green"></i>' : '<i class="fa fa-remove" style="color:red"></i>'; ?></td>
              <?php if( hasAccess('activities', 'activities', 'edit') ) { ?>
                <td class="text-right">
                <button type="button" class="btn btn-warning btn-xs ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Edit Activity" data-url="<?php echo site_url("activities/edit/{$activity->id}/ajax") . "?next=" . uri_string(); ?>">Edit</button>

                <a class="btn btn-danger btn-xs confirm_remove" href="<?php echo site_url("activities/delete/{$activity->id}"); ?>" data-target="#employee-group-<?php echo $activity->id; ?>">Delete</a>

                 <a class="btn btn-success btn-xs body_wrapper" href="<?php echo site_url("activities/attendance/{$activity->id}"); ?>">Attendance</a>

                </td>
              <?php } ?>
              </tr>
            <?php } ?>

            </tbody>
          </table>

          <?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>

<?php } else { ?>

  <div class="text-center">No Activity Found!</div>

<?php } ?>
<?php if( ! $inner_page ): ?>

              </div>
              </div>
            </div>
    </div>
</div>
<?php endif; ?>
<?php $this->load->view('footer'); ?>