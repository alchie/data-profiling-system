<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
<?php if( ! $inner_page ): ?>
<?php $this->load->view('calendar/calendar_navbar'); ?>

<div class="container">
    <div class="row">
            <div class="col-md-12">
              <div class="panel panel-default">
                <div class="panel-heading">
<form method="get" action="<?php echo site_url(uri_string()); ?>">
<div class="input-group input-group-sm col-md-3 pull-right">
  <input type="text" class="form-control" placeholder="Search..." name="q" value="<?php echo $this->input->get('q'); ?>">
  <span class="input-group-btn">
        <button class="btn btn-default" type="submit">Search</button>
      </span>
</div>
</form>
                  <h3 class="panel-title bold">Attendance <span class="badge">COUNT: <?php echo $benefeciaries_count; ?></span> 
                    <a href="<?php echo site_url("activities/add_activity/{$ids}"); ?>"><span class="glyphicon glyphicon-plus"></span></a></h3>
                </div>
                <div class="panel-body" id="ajaxBodyInnerPage">
<?php endif; ?>

<?php if( $benefeciaries ) { ?>

          <table class="table table-default table-hover">
            <thead>
              <tr>
                <th>Last Name</th>
                <th>First Name</th>
                <th>Middle Name</th>
                <th></th>
<?php foreach($activities as $activity) { ?>
                <th width="100px" class="text-center">
<?php echo $activity->name; ?><br>
<small><?php echo date("m-d-Y", strtotime($activity->activity_date)); ?><br>
<?php echo date("l", strtotime($activity->activity_date)); ?></small>
                </th>
<?php } ?>
              </tr>
            </thead>
            <tbody>

            <?php foreach($benefeciaries as $benefeciary) { //print_r($benefeciary); ?>
              <tr id="benefeciary-<?php echo $benefeciary->name_id; ?>">
                <td><?php echo $benefeciary->lastname; ?></td>
                <td><?php echo $benefeciary->firstname; ?></td>
                <td><?php echo $benefeciary->middlename; ?></td>
                <td>
<a href="javascript:void(0);" class="ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Profile" data-url="<?php echo site_url("lists_names/profile/{$benefeciary->name_id}/ajax") . "?next=" . uri_string() . "&q=" . $this->input->get('q'); ?>"><span class="glyphicon glyphicon-cog"></span></a>
                </td>
<?php foreach($activities as $activity) { 
$var = 'is_present_' . $activity->id;
  ?>
                <td class="text-center">
                  <a href="javascript:void(0);" class="update-attendance" data-checked="<?php echo $benefeciary->$var; ?>" data-href="<?php echo site_url("activities/update_attendace/{$activity->id}/{$benefeciary->name_id}/ajax"); ?>" id="line-<?php echo "{$activity->id}-{$benefeciary->name_id}"; ?>">
                <?php 
                
                if( $benefeciary->$var ) { ?>
                  <i class="fa fa-check" style="color:green;"></i>
                <?php } else { ?>
                  <i class="fa fa-remove" style="color:red;"></i>
                <?php } ?>
                </a>
                </td>
<?php } ?>
              </tr>
            <?php } ?>

            </tbody>
          </table>

<?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>

<?php } else { ?>

  <div class="text-center">No Benefeciary Found!</div>

<?php } ?>


<?php if( ! $inner_page ): ?>

              </div>
              </div>
            </div>
    </div>
</div>
<?php endif; ?>
<?php $this->load->view('footer'); ?>