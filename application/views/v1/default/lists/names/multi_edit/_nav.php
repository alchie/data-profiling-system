<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!-- Split button -->
<div class="btn-group">
  <button type="button" class="btn btn-warning btn-xs"><?php echo $modules[$module]; ?></button>
  <button type="button" class="btn btn-success btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <span class="caret"></span>
    <span class="sr-only">Toggle Dropdown</span>
  </button>
  <ul class="dropdown-menu">
  	<?php foreach($modules as $key=>$name) { 
if( $key == $module ) continue;
  		?>
    	<li><a href="<?php echo site_url("lists_names/multi_edit/{$key}"); ?>"><?php echo $name; ?></a></li>
	<?php } ?>
  </ul>
</div>