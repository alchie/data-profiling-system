<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $this->load->view('header'); ?>
<?php if( ! $inner_page ): ?>

<?php $this->load->view('lists/lists_navbar'); ?>

<form method="post">
<div class="container">
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-default">
	    	<div class="panel-heading">

<div class="row">
<div class="col-md-12">
<button type="submit" class="btn btn-success pull-right">Save Changes</button>
	    		<h3 class="panel-title">
 <strong><?php echo $current_page; ?> - Multi Edit</strong> 

<?php $this->load->view('lists/names/multi_edit/_nav'); ?>

                    <br><small><em>(<?php echo $names_count; ?> name<?php echo ($names_count>1)?"s":""; ?> found)</em></small>
	    		</h3>
</div>

 

</div>
	    	</div>
	    	<div class="panel-body" id="ajaxBodyInnerPage">

<?php endif; ?>
<?php if( $names ) { ?>
	    		<table class="table table-default hidden-xs table-hover">
	    			<thead>
	    				<tr>
	    					<th>Full Name</th>
	    					<th>Prefix</th>
	    					<th>Last Name</th>
	    					<th>First Name</th>
	    					<th>Middle Name</th>
	    					<th>Suffix</th>
	    					<th>Birthday</th>
	    					<th>Birthplace</th>
	    					<th>Civil Status</th>
	    					<th>Gender</th>
	    				</tr>
	    			</thead>
	    			<tbody>
	    			<?php foreach($names as $name) { //print_r($name); 

$lastname = $name->lastname;
if($name->lastname=='') {
	$lastname2 = explode(",", $name->full_name);
	$lastname = (isset( $lastname2[0] ) ) ? $lastname2[0] : '';
}
$firstname = $name->firstname;
if($name->firstname=='') {
	$firstname2 = explode(",", $name->full_name);
	$firstname = (isset( $firstname2[1] ) ) ? $firstname2[1] : '';
}

	    				?>
	    				<tr id="name-<?php echo $name->id; ?>">
	    					<td><?php echo $name->full_name; ?></td>
	    					<td><input type="text" class="form-control" name="input[<?php echo $name->id; ?>][prefix]" value="<?php echo $name->prefix; ?>"></td>
	    					<td><input type="text" class="form-control" name="input[<?php echo $name->id; ?>][lastname]" value="<?php echo $lastname; ?>"></td>
	    					<td><input type="text" class="form-control" name="input[<?php echo $name->id; ?>][firstname]" value="<?php echo $firstname; ?>"></td>
	    					<td><input type="text" class="form-control" name="input[<?php echo $name->id; ?>][middlename]" value="<?php echo $name->middlename; ?>"></td>
	    					<td><input type="text" class="form-control" name="input[<?php echo $name->id; ?>][suffix]" value="<?php echo $name->suffix; ?>"></td>
	    					<td><input type="text" class="form-control datepicker" name="input[<?php echo $name->id; ?>][birthday]" value="<?php echo ($name->birthday!='0000-00-00') ? date('m/d/Y', strtotime($name->birthday)) : ''; ?>"></td>
	    					<td><input type="text" class="form-control" name="input[<?php echo $name->id; ?>][birthplace]" value="<?php echo $name->birthplace; ?>"></td>
	    					<td>
	    						       
            <select name="input[<?php echo $name->id; ?>][gender]" class="form-control">
                <option value="male" <?php echo (($name) && ($name->gender=='male')) ? "SELECTED" : ""; ?>>Male</option>
                <option value="female" <?php echo (($name) && ($name->gender=='female')) ? "SELECTED" : ""; ?>>Female</option>
            </select>
       
	    					</td>
	    					<td>
	    						
	    						<select name="input[<?php echo $name->id; ?>][civil_status]" class="form-control">
<?php $civil_status = $this->config->item('civil_status'); ?>
<?php foreach($civil_status as $key=>$status) { ?>
               <option value="<?php echo $key; ?>" <?php echo (($name) && ($name->civil_status==$key)) ? "SELECTED" : ""; ?>><?php echo $status; ?></option>
<?php } ?>
            </select>

	    					</td>
	    				</tr>
	    			<?php } ?>
	    			</tbody>
	    		</table>

<ul class="list-group visible-xs">
  <?php foreach($names as $name) { 
if($name->members > 0) {
	$uri = "membership_members/member_data/{$name->id}";
} elseif($name->companies > 0) {
	$uri = "membership_companies/info/{$name->id}";
}
  	?>
      <a href="#<?php echo site_url("lists_names/edit/{$name->id}"); ?>" class="list-group-item ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Add Name" data-url="<?php echo site_url("lists_names/edit/{$name->id}/ajax") . "?next=" . uri_string(); ?>">
        <h4 class="list-group-item-heading"><?php echo $name->full_name; ?></h4>
      </a>
    <?php } ?>
</ul>

<?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>

<?php } else { ?>
	<div class="text-center">No Name Found!</div>
<?php } ?>

<?php if( ! $inner_page ): ?>

	    	</div>
	    </div>
    </div>
</div>
</div>
</form>
<?php endif; ?>

<?php $this->load->view('footer'); ?>