<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $this->load->view('header'); ?>
<?php if( ! $inner_page ): ?>

<?php $this->load->view('lists/lists_navbar'); ?>

<form method="post">
<div class="container">
<div class="row">
	<div class="col-md-12">
	    <div class="panel panel-default">
	    	<div class="panel-heading">

<div class="row">
<div class="col-md-12">
<button type="submit" class="btn btn-success pull-right">Save Changes</button>
	    		<h3 class="panel-title">
 <strong><?php echo $current_page; ?> - Multi Edit</strong> 

<?php $this->load->view('lists/names/multi_edit/_nav'); ?>

                    <br><small><em>(<?php echo $names_count; ?> name<?php echo ($names_count>1)?"s":""; ?> found)</em></small>
	    		</h3>
</div>

 

</div>
	    	</div>
	    	<div class="panel-body" id="ajaxBodyInnerPage">

<?php endif; ?>
<?php if( $names ) { ?>
	    		<table class="table table-default hidden-xs table-hover">
	    			<thead>
	    				<tr>
	    					<th>Full Name</th>
	    					<th>Name</th>
	    					<th>Address</th>
	    					<th>Contact Number</th>
	    					<th>Relationship</th>
	    				</tr>
	    			</thead>
	    			<tbody>
	    			<?php foreach($names as $name) { //print_r($name); 
	    				?>
	    				<tr id="name-<?php echo $name->id; ?>">
	    					<td><?php echo $name->full_name; ?></td>
	    					<td><input type="text" class="form-control" name="input[<?php echo $name->id; ?>][emergency_name]" value="<?php echo $name->emergency_name; ?>"></td>
	    					<td><input type="text" class="form-control" name="input[<?php echo $name->id; ?>][emergency_address]" value="<?php echo $name->emergency_address; ?>"></td>
	    					<td><input type="text" class="form-control" name="input[<?php echo $name->id; ?>][emergency_contact]" value="<?php echo $name->emergency_contact; ?>"></td>
	    					<td><input type="text" class="form-control" name="input[<?php echo $name->id; ?>][emergency_relationship]" value="<?php echo $name->emergency_relationship; ?>"></td>
	    				</tr>
	    			<?php } ?>
	    			</tbody>
	    		</table>

<ul class="list-group visible-xs">
  <?php foreach($names as $name) { 
if($name->members > 0) {
	$uri = "membership_members/member_data/{$name->id}";
} elseif($name->companies > 0) {
	$uri = "membership_companies/info/{$name->id}";
}
  	?>
      <a href="#<?php echo site_url("lists_names/edit/{$name->id}"); ?>" class="list-group-item ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Add Name" data-url="<?php echo site_url("lists_names/edit/{$name->id}/ajax") . "?next=" . uri_string(); ?>">
        <h4 class="list-group-item-heading"><?php echo $name->full_name; ?></h4>
      </a>
    <?php } ?>
</ul>

<?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>

<?php } else { ?>
	<div class="text-center">No Name Found!</div>
<?php } ?>

<?php if( ! $inner_page ): ?>

	    	</div>
	    </div>
    </div>
</div>
</div>
</form>
<?php endif; ?>

<?php $this->load->view('footer'); ?>