<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
<?php if( ! $inner_page ): ?>
<?php $this->load->view('lists/lists_navbar'); ?>

<div class="container">
    <div class="row">
            <div class="col-md-12">
              <div class="panel panel-default">
                <div class="panel-heading">

<div class="row">
<div class="col-md-9 col-sm-6">
          <h3 class="panel-title bold"><?php echo $service->name; ?>
                    <?php if( $this->input->get('q') ) { ?>
                    <span class="badge"><?php echo $this->input->get('q'); ?> <a href="<?php echo site_url(uri_string()); ?>"><span class="glyphicon glyphicon-remove"></span></a></span>
                    <?php } ?>
          </h3>
</div>
<div class="col-md-3">
<form method="get" action="<?php echo site_url(uri_string()); ?>">
<div class="input-group input-group-sm">
  <input type="text" name="q" class="form-control" placeholder="Search for..." value="<?php echo $this->input->get('q'); ?>">
  <span class="input-group-btn">
    <button class="btn btn-default" type="submit">Search</button>
    
  </span>
</div><!-- /input-group -->
</form>
</div>
</div>

                </div>
                <div class="panel-body" id="ajaxBodyInnerPage">
<?php endif; ?>
<?php if( $activities ) { ?>

          <table class="table table-default table-hover">
            <thead>
              <tr>
                <th>Service Name</th>
                <th>Notes</th>
                <th>Date</th>
                <th width="1px">B</th>
                <th  width="1px">P</th>
                <?php if( hasAccess('lists', 'services', 'edit') ) { ?>
                  <th width="200px" class="text-right">Action</th>
                <?php } ?>
              </tr>
            </thead>
            <tbody>

            <?php foreach($activities as $activity) {  ?>
              <tr id="employee-group-<?php echo $activity->id; ?>">
                <td><?php echo $activity->name; ?></td>
                <td><?php echo $activity->notes; ?></td>
                <td><?php echo date('F d, Y', strtotime($activity->activity_date)); ?></td>
                <td><?php echo ($activity->benefeciaries==1) ? '<i class="fa fa-check" style="color:green"></i>' : '<i class="fa fa-remove" style="color:red"></i>'; ?></td>
                <td><?php echo ($activity->parents==1) ? '<i class="fa fa-check" style="color:green"></i>' : '<i class="fa fa-remove" style="color:red"></i>'; ?></td>
              <?php if( hasAccess('lists', 'services', 'edit') ) { ?>
                <td class="text-right">

                 <a class="btn btn-success btn-xs body_wrapper" href="<?php echo site_url("activities/attendance/{$activity->id}"); ?>">Attendace</a>

                </td>
              <?php } ?>
              </tr>
            <?php } ?>

            </tbody>
          </table>

          <?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>

<?php } else { ?>

  <div class="text-center">No Service Found!</div>

<?php } ?>
<?php if( ! $inner_page ): ?>

              </div>
              </div>
            </div>
    </div>
</div>
<?php endif; ?>
<?php $this->load->view('footer'); ?>