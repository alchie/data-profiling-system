<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
<?php if( ! $inner_page ): ?>
<?php $this->load->view('benefeciaries/benefeciaries_navbar'); ?>

<div class="container">
    <div class="row">
            <div class="col-md-12">
              <div class="panel panel-default">
                <div class="panel-heading">

<div class="row">
<div class="col-md-6 col-sm-6">
                  <h3 class="panel-title">
                    <strong>Monthly Attendance</strong>
 <?php if( $current_month ) { ?>
 <a href="<?php echo site_url("benefeciaries/monthly_attendance/{$current_year}/{$current_month}/{$limit}") . "?output=print&show=" . $this->input->get('show') . "&cgs=" . $current_cgs; ?>"><span class="glyphicon glyphicon-print"></span></a>
<?php } ?>
<br><small><em>(<a href="<?php echo site_url("benefeciaries/monthly_attendance/{$current_year}/{$current_month}/{$benefeciaries_count}/0") . "?show=" . $this->input->get('show'). "&cgs=" . $current_cgs; ?>"><?php echo $benefeciaries_count; ?> name<?php echo ($benefeciaries_count>1)?"s":""; ?> found</a>)</em></small>
<?php if(( $limit <> 10 ) || ( $benefeciaries_count == 0 )) { ?>
<a href="<?php echo site_url("benefeciaries/monthly_attendance/{$current_year}/{$current_month}/10/0"); ?>" class="badge" style="background-color: red;">Reset <span class="glyphicon glyphicon-remove"></span></a>
<?php } ?>
                  </h3>
</div>

<div class="col-md-6 col-sm-6">
<form method="get" action="<?php echo site_url( uri_string() ); ?>">
<div class="input-group input-group-sm">

<?php if( $current_month ) { ?>

<div class="input-group-btn">
<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <?php echo date('F', strtotime($current_month.'/1/'.date('Y'))); ?> <span class="caret"></span>
  </button>
<ul class="dropdown-menu">
<?php for($i=1;$i<=12;$i++) { ?>
    <li class="<?php echo ($i==$current_month) ? 'active' : ''; ?>"><a href="<?php echo site_url("benefeciaries/monthly_attendance/{$current_year}/{$i}/{$limit}/{$start}"); ?>"><?php echo date('F', strtotime($i.'/1/'.date('Y'))); ?></a></li>
<?php } ?>
</ul>

</div><!-- /btn-group -->

<?php } ?>

<div class="input-group-btn">
<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <?php echo $current_year; ?> <span class="caret"></span>
  </button>
<ul class="dropdown-menu">
<?php foreach($years as $year) { ?>
    <li class="<?php echo ($year->year==$current_year) ? 'active' : ''; ?>"><a href="<?php echo site_url("benefeciaries/monthly_attendance/{$year->year}/{$current_month}/{$limit}/{$start}"); ?>"><?php echo $year->year; ?></a></li>
<?php } ?>
</ul>

</div><!-- /btn-group -->


  <input type="text" name="q" class="form-control" placeholder="Search for..." value="<?php echo $this->input->get('q'); ?>">
  <span class="input-group-btn">
    <button class="btn btn-default" type="submit">Search</button>
  </span>
</div><!-- /input-group -->
</form>

</div>

</div>

                </div>
                <div class="panel-body" id="ajaxBodyInnerPage">
<?php endif; ?>
<?php if( $benefeciaries ) { ?>

          <table class="table table-default table-hover">
            <thead>
              <tr>
                <th>Last Name</th>
                <th>First Name</th>
<?php 
$col_width="5%";
if( $current_month ) {
  $col_width="25%";
}
for($i=1;$i<=12;$i++) { 
if( ($current_month) && ($current_month<>$i) ) {
  continue;
} 
  ?>
                <th class="text-right" width="<?php echo $col_width; ?>">
<?php if( !$current_month ) { ?>
                  <a href="<?php echo site_url("benefeciaries/monthly_attendance/{$current_year}/{$i}/{$limit}/{$start}"); ?>">
<?php } ?>
                  <?php echo date('F Y', strtotime($i.'/1/'.date('Y'))); ?>
<?php if( !$current_month ) { ?>
                  </a>
<?php } ?>

              </th>
<?php } ?>

              </tr>
            </thead>
            <tbody>

            <?php foreach($benefeciaries as $benefeciary) { //print_r( $benefeciary ); ?>
              <tr id="benefeciary-<?php echo $benefeciary->name_id; ?>"  title="<?php echo $benefeciary->name_id; ?>">
                <td><?php echo $benefeciary->lastname; ?></td>
                <td><?php echo $benefeciary->firstname; ?></td>
<?php for($i=1;$i<=12;$i++) { 
if( ($current_month) && ($current_month<>$i) ) {
  continue;
}
$var = 'month_' . $i;
  ?>
                <td class="text-right"><?php echo $benefeciary->$var; ?></td>
<?php } ?>

              </tr>
            <?php } ?>

            </tbody>
          </table>

          <?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>

<?php } else { ?>

  <div class="text-center">No Benefeciary Found!</div>

<?php } ?>
<?php if( ! $inner_page ): ?>

              </div>
              </div>
            </div>
    </div>
</div>
<?php endif; ?>
<?php $this->load->view('footer'); ?>