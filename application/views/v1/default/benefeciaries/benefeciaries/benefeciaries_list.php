<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
<?php if( ! $inner_page ): ?>
<?php $this->load->view('benefeciaries/benefeciaries_navbar'); ?>

<div class="container">
    <div class="row">
            <div class="col-md-12">
              <div class="panel panel-default">
                <div class="panel-heading">

<div class="row">
<div class="col-md-7 col-sm-6">

                  <h3 class="panel-title">
                    <strong><?php echo $current_page; ?></strong>

  <a href="<?php echo site_url("benefeciaries") . "?filter=trash&back=" . uri_string(); ?>"><span class="glyphicon glyphicon-trash"></span></a>

<a href="<?php echo site_url("benefeciaries/report"); ?>" class=""><span class="glyphicon glyphicon-print"></span></a>


                    <?php if( isset($group) ) { ?>
                      <span class="badge"><?php echo $group->name; ?> <a href="<?php echo site_url("benefeciaries"); ?>"><span class="glyphicon glyphicon-remove"></span></a></span>
                    <?php } ?>
                    <?php if( isset($position) ) { ?>
                    <span class="badge"><?php echo $position->name; ?> <a href="<?php echo site_url("benefeciaries"); ?>"><span class="glyphicon glyphicon-remove"></span></a></span>
                    <?php } ?>
                    <?php if( $this->input->get('q') ) { ?>
                    <span class="badge"><?php echo $this->input->get('q'); ?> <a href="<?php echo site_url(uri_string()); ?>"><span class="glyphicon glyphicon-remove"></span></a></span>
                    <?php } ?>

<br><small><em>(<a href="<?php echo site_url("benefeciaries/index/{$benefeciaries_count}/0"); ?>"><?php echo $benefeciaries_count; ?> name<?php echo ($benefeciaries_count>1)?"s":""; ?> found</a>)</em></small>
                  </h3>
</div>
<div class="col-md-5 col-sm-6">
<form method="get" action="<?php echo site_url( uri_string() ); ?>">
<div class="input-group input-group-sm">
<div class="input-group-btn">
        <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo (isset($current_service)) ? $current_service->name : 'All Benefeciaries'; ?> <span class="caret"></span></button>
        <ul class="dropdown-menu">
<?php if( isset($current_service) ) { ?>
          <li><a href="<?php echo site_url('benefeciaries'); ?>">Show All</a></li>
<?php } ?>
<?php foreach( $services as $service ) { ?>
          <li class="<?php echo ((isset($current_service)) && ($service->id==$current_service->id)) ? 'active' : ''; ?>"><a href="<?php echo site_url('benefeciaries/service/' . $service->id); ?>"><?php echo $service->name; ?></a></li>
<?php } ?>
        </ul>
      </div><!-- /btn-group -->


  <input type="text" name="q" class="form-control" placeholder="Search for..." value="<?php echo $this->input->get('q'); ?>">
  <span class="input-group-btn">
    <button class="btn btn-default" type="submit">Search</button>
    
<?php if( hasAccess('benefeciaries', 'benefeciaries', 'add') ) { ?>
  <button type="button" class="btn btn-success ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Add Benefeciary" data-url="<?php echo site_url("benefeciaries/search_name/ajax") . "?next=" . (($this->input->get('next')) ? $this->input->get('next') : uri_string()); ?>" style="margin-right: 5px" data-hide_footer="1">Add Benefeciary</button>
<?php } ?>

  </span>
</div><!-- /input-group -->
</form>
</div>

</div>

                </div>
                <div class="panel-body" id="ajaxBodyInnerPage">
<?php endif; ?>
<?php if( $benefeciaries ) { ?>

          <table class="table table-default table-hover">
            <thead>
              <tr>
                <th>Last Name</th>
                <th>First Name</th>
                <th>Middle Name</th>
                <?php if( !isset($group) ) { ?>
                  <th>Group</th>
                <?php } ?>
<?php /*
                <?php if( !isset($position) ) { ?>
                  <th>Position</th>
                <?php } ?>
                <?php if( !isset($area) ) { ?>
                  <th>Area</th>
                <?php } ?>
                <th>Status</th>
*/ ?>
                <?php if( hasAccess('benefeciaries', 'benefeciaries', 'edit') ) { ?>
                  <th width="195px" class="text-right">Action</th>
                <?php } ?>
              </tr>
            </thead>
            <tbody>

            <?php foreach($benefeciaries as $benefeciary) { ?>
              <tr id="benefeciary-<?php echo $benefeciary->name_id; ?>">
                <td><?php echo $benefeciary->lastname; ?></td>
                <td><?php echo $benefeciary->firstname; ?></td>
                <td><?php echo $benefeciary->middlename; ?></td>
                <?php if( !isset($group) ) { ?>
                <td>
<?php if( $benefeciary->group_name ) { ?>
                <a class="body_wrapper" href="<?php echo site_url("benefeciaries/group/{$benefeciary->group_id}"); ?>"><?php echo $benefeciary->group_name; ?></a>
<?php } else { ?>
<a data-title="<?php echo $benefeciary->lastname; ?>, <?php echo $benefeciary->firstname; ?> <?php echo $benefeciary->middlename; ?>" class="ajax-modal" href="#ajaxModal" data-toggle="modal" data-url="<?php echo site_url("benefeciaries/edit_benefeciary/{$benefeciary->name_id}/ajax") . "?next=" . uri_string(); ?>">--</a>
<?php } ?>
                </td>
                <?php } ?>
<?php /*
                <?php if( !isset($position) ) { ?>
                <td>
                <a class="body_wrapper" href="<?php echo site_url("benefeciaries/position/{$benefeciary->position_id}"); ?>">
                <?php echo $benefeciary->position_name; ?>
                  </a>
                </td>
                <?php } ?>
                <?php if( !isset($area) ) { ?>
                <td>
                <a class="body_wrapper" href="<?php echo site_url("benefeciaries/area/{$benefeciary->area_id}"); ?>">
                <?php echo $benefeciary->area_name; ?>
                  </a>
                </td>
                <?php } ?>
                <td><?php echo $benefeciary->status_name; ?></td>
*/ ?>
              <?php if( hasAccess('benefeciaries', 'benefeciaries', 'edit') ) { ?>
                <td class="text-right">


<div class="btn-group">
                  <a class="btn btn-info btn-xs body_wrapper" href="<?php echo site_url("lists_names/profile/{$benefeciary->name_id}"); ?>">Profile</a>
  <button type="button" class="btn btn-info dropdown-toggle btn-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <span class="caret"></span>
    <span class="sr-only">Toggle Dropdown</span>
  </button>
  <ul class="dropdown-menu dropdown-menu-right">
    
    <li><a href="<?php echo site_url("benefeciaries/height_weight/{$benefeciary->name_id}"); ?>">Height & Weight</a></li>
    <li><a href="<?php echo site_url("benefeciaries/attendance/{$benefeciary->name_id}"); ?>">Attendance</a></li>

  </ul>
</div>

                <a class="btn btn-danger btn-xs confirm_remove" href="<?php echo site_url("benefeciaries/deactivate/{$benefeciary->name_id}"); ?>" data-target="#benefeciary-<?php echo $benefeciary->name_id; ?>">Deactivate</a>
                </td>
              <?php } ?>
              </tr>
            <?php } ?>

            </tbody>
          </table>

          <?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>

<?php } else { ?>

  <div class="text-center">No Benefeciary Found!</div>

<?php } ?>
<?php if( ! $inner_page ): ?>

              </div>
              </div>
            </div>
    </div>
</div>
<?php endif; ?>
<?php $this->load->view('footer'); ?>