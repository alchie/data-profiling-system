<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
<?php if( ! $inner_page ): ?>
<?php $this->load->view('benefeciaries/benefeciaries_navbar'); ?>

<div class="container">
    <div class="row">
            <div class="col-md-12">
              <div class="panel panel-default">
                <div class="panel-heading">

<div class="row">
<div class="col-md-6 col-sm-6">
                  <h3 class="panel-title">
                    <strong>Height & Weight</strong>
 <?php if( $current_month ) { ?>
 <a href="<?php echo site_url("benefeciaries/height_weight_all/{$current_year}/{$current_month}/{$limit}") . "?output=print&show=" . $this->input->get('show') . "&cgs=" . $current_cgs; ?>"><span class="glyphicon glyphicon-print"></span></a>
<?php } ?>
<br><small><em>(<a href="<?php echo site_url("benefeciaries/height_weight_all/{$current_year}/{$current_month}/{$benefeciaries_count}/0") . "?show=" . $this->input->get('show'). "&cgs=" . $current_cgs; ?>"><?php echo $benefeciaries_count; ?> name<?php echo ($benefeciaries_count>1)?"s":""; ?> found</a>)</em></small>
<?php if(( $limit <> 10 ) || ( $benefeciaries_count == 0 )) { ?>
<a href="<?php echo site_url("benefeciaries/height_weight_all/{$current_year}/{$current_month}/10/0"); ?>" class="badge" style="background-color: red;">Reset <span class="glyphicon glyphicon-remove"></span></a>
<?php } ?>
                  </h3>
</div>

<div class="col-md-6 col-sm-6">
<form method="get" action="<?php echo site_url( uri_string() ); ?>">
<div class="input-group input-group-sm">

<?php if( $current_month ) { ?>
  <span class="input-group-btn">
     <a class="btn btn-warning" href="<?php echo site_url("benefeciaries/import_height_weight/{$current_year}/{$current_month}") . "?next=" . uri_string(); ?>"><span class="glyphicon glyphicon-import"></span></a>
  </span>

<div class="input-group-btn">
<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <?php echo date('F', strtotime($current_month.'/1/'.date('Y'))); ?> <span class="caret"></span>
  </button>
<ul class="dropdown-menu">
  <li class=""><a href="<?php echo site_url("benefeciaries/height_weight_all/{$current_year}/0/{$limit}/{$start}"); ?>">Show All</a></li>
<?php for($i=1;$i<=12;$i++) { ?>
    <li class="<?php echo ($i==$current_month) ? 'active' : ''; ?>"><a href="<?php echo site_url("benefeciaries/height_weight_all/{$current_year}/{$i}/{$limit}/{$start}"); ?>"><?php echo date('F', strtotime($i.'/1/'.date('Y'))); ?></a></li>
<?php } ?>
</ul>

</div><!-- /btn-group -->

<?php } ?>
<div class="input-group-btn">
<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <?php echo $current_year; ?> <span class="caret"></span>
  </button>
<ul class="dropdown-menu">
<?php foreach($years as $year) { ?>
    <li class="<?php echo ($year->year==$current_year) ? 'active' : ''; ?>"><a href="<?php echo site_url("benefeciaries/height_weight_all/{$year->year}/{$current_month}/{$limit}/{$start}"); ?>"><?php echo $year->year; ?></a></li>
<?php } ?>
</ul>

</div><!-- /btn-group -->

<div class="input-group-btn">
<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Child Growth Standard <span class="caret"></span>
  </button>
<ul class="dropdown-menu">
<?php foreach(array(
  'weight_for_age' => 'Weight-for-age',
  'height_for_age' => 'Length/height-for-age',
  'weight_for_length_height' => 'Weight-for-length/height',
  'bmi_for_age' => 'Body mass index-for-age (BMI-for-age)',
  /*
  'hc_for_age' => 'Head circumference-for-age',
  'ac_for_age' => 'Arm circumference-for-age',
  'ssf_for_age' => 'Subscapular skinfold-for-age',
  'tsf_for_age' => 'Triceps skinfold-for-age',
  'motor_milestones' => 'Motor development milestones',
  'w_velocity' => 'Weight velocity',
  'l_velocity' => 'Length velocity',
  'hc_velocity' => 'Head circumference velocity',
  */
) as $key => $cgs) { ?>
    <li class="<?php echo ($key==$current_cgs) ? 'active' : ''; ?>"><a href="<?php echo site_url("benefeciaries/height_weight_all/{$current_year}/{$current_month}/{$limit}/{$start}") . "?show=" . $this->input->get('show') . "&cgs=" . $key; ?>"><?php echo $cgs; ?></a></li>
<?php } ?>
</ul>

</div><!-- /btn-group -->

  <input type="text" name="q" class="form-control" placeholder="Search for..." value="<?php echo $this->input->get('q'); ?>">
  <span class="input-group-btn">
    <button class="btn btn-default" type="submit">Search</button>
  </span>
</div><!-- /input-group -->
</form>

</div>

</div>

                </div>
                <div class="panel-body" id="ajaxBodyInnerPage">
<?php endif; ?>
<?php if( $benefeciaries ) { ?>

          <table class="table table-default table-hover">
            <thead>
              <tr>
                <th>Last Name</th>
                <th>First Name</th>
<?php 
$col_width="5%";
if( $current_month ) {
  $col_width="25%";
}
for($i=1;$i<=12;$i++) { 
if( ($current_month) && ($current_month<>$i) ) {
  continue;
} 
  ?>
                <th class="text-center" width="<?php echo $col_width; ?>">
<?php if( !$current_month ) { ?>
                  <a href="<?php echo site_url("benefeciaries/height_weight_all/{$current_year}/{$i}/{$limit}/{$start}"); ?>">
<?php } ?>
                  <?php echo date('F', strtotime($i.'/1/'.date('Y'))); ?>
<?php if( !$current_month ) { ?>
                  </a>
<?php } ?>
<?php if( ($current_month) && ($current_month==$i) ) { ?>
  <a href="<?php echo site_url("benefeciaries/height_weight_all/{$current_year}/{$current_month}/{$limit}") . "?show=without_hw"; ?>"><span class="glyphicon glyphicon-remove"></span></a>

  <a href="<?php echo site_url("benefeciaries/height_weight_all/{$current_year}/{$current_month}/{$limit}") . "?show=with_hw" ?>"><span class="fa fa-check"></span></a>

<?php } ?>
              </th>
<?php } ?>
<?php if( $current_month ) { ?>
  <th class="text-right">Status</th>
<?php } ?>
              </tr>
            </thead>
            <tbody>

            <?php foreach($benefeciaries as $benefeciary) { //print_r( $benefeciary ); ?>
              <tr id="benefeciary-<?php echo $benefeciary->name_id; ?>"  title="<?php echo $benefeciary->name_id; ?>">
                <td><?php echo $benefeciary->lastname; ?></td>
                <td><?php echo $benefeciary->firstname; ?></td>
<?php for($i=1;$i<=12;$i++) { 
if( ($current_month) && ($current_month<>$i) ) {
  continue;
}
$var = 'month_' . $i;
  ?>
                <td class="text-center">
<?php if( $benefeciary->$var ) { ?>
    <a class="ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Edit Height & Weight" href="#ajaxModal" data-url="<?php echo site_url("benefeciaries/edit_height_weight/{$benefeciary->$var}/ajax") . "?next=" . uri_string() . "&q=" . $this->input->get('q'); ?>"><span class="fa fa-check" style="color:green"></span></a>
<?php } else { ?>
    <a class="ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Add Height & Weight" href="#ajaxModal" data-url="<?php echo site_url("benefeciaries/add_height_weight/{$benefeciary->name_id}/ajax") . "?next=" . uri_string() . "&month={$i}&year={$current_year}" . "&q=" . $this->input->get('q'); ?>"><span class="fa fa-remove" style="color:red"></span></a>
<?php } ?>
                </td>
<?php } ?>
<?php if( $current_month ) { ?>
  <td class="text-right"><?php echo weight_for_age($benefeciary->weight, $benefeciary->birthday); ?></td>
<?php } ?>
              </tr>
            <?php } ?>

            </tbody>
          </table>

          <?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>

<?php } else { ?>

  <div class="text-center">No Benefeciary Found!</div>

<?php } ?>
<?php if( ! $inner_page ): ?>

              </div>
              </div>
            </div>
    </div>
</div>
<?php endif; ?>
<?php $this->load->view('footer'); ?>