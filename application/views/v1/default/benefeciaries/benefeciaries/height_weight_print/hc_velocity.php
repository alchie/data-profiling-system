<html><head><title><?php echo (isset($page_title))?$page_title:'VOUCHER'; ?></title></head>
<?php //print_r($check); ?>
<?php //print_r($expenses); ?>
<style>
<!--
  body {
    font-family: Arial, Helvetica, sans-serif;
    font-size: 12px;
  }
  table {
    font-size: 14px;
  }
  .bigger {
   font-size: 14px; 
  }
  .footer {
   font-size: 10px; 
  }
  table.bordered {
    border-top: 1px solid #000;
  }
  table.bordered td,
  table.bordered th 
  {
    border-bottom: 1px solid #000;
  }
  tr.bordered td {
   border-bottom: 1px solid #000; 
   border-top: 1px solid #000; 
  }
  .allcaps {
    text-transform: uppercase;
  }
  .green_bg {
    background-color: rgb(216,228,188);
  }
  .yellow_bg {
    background-color: rgb(255, 221, 0);;
  }

  .font85p {
    font-size: 85%;
  }

  .font90p {
    font-size: 90%;
  }

.print-topnav {
  position: fixed;
  width: 300px;
  top: 0;
  right: 0;
  margin-top: 0; /* Negative half of height. */
  margin-left: -250px; /* Negative half of width. */
  border: 1px solid #000;
  border-top:none;
  padding:3px;
  border-bottom-left-radius: 5px;
  border-bottom-right-radius: 5px;
  background-color: #CCC;
  font-size: 11px;
  z-index: 99;
}
.print-topnav a {
    text-decoration: none;
    color: #000;
}
.print-topnav a:hover {
  font-weight: bold;
}
.print-topnav a:active {
  font-weight: bold;
}
.text-center {
  text-align: center;
}
.text-right {
  text-align: right;
}
.text-left {
  text-align: left;
}
.active {
  font-weight: bold;
  color:blue!important;
}
.allcaps {
  text-transform: uppercase;
}

#check_print {
  width: 768px;
  height: 288px;
  position:relative;
}
#check_print .elm {
  position:absolute;
  color: black;
  font-size:12px;
  font-weight:bold;
  font-family:serif;
}
#check_print.serif .elm {
  font-family:serif;
}
#check_print.monospace .elm {
  font-family:monospace;
}
#check_print.sans-serif .elm {
  font-family:sans-serif;
}
#check_print.cursive .elm {
  font-family:cursive;
}
#check_print.fantasy .elm {
  font-family:fantasy;
}
#check_print .payee {
  top: 74px;
    left: 130px;
}
#check_print .payee.two_lines {
    top: 57px;
    width: 400px;
    text-align: center;
}
#check_print .amount {
    top: 70px;
    left: 580px;
}
#check_print .date {
  top: 40px;
    left: 600px;
}
#check_print .amount-words {
  top: 95px;
    left: 80px;
    font-size: 10px;
    width: 650px;
    text-align: center;
  font-weight:normal;
}
#check_print .signatory {
  top: 175px;
    left: 380px;
}
#check_print .signatory-2 {
  top: 175px;
    left: 550px;
}
.print {
  background: #FFF;
}
.edit_field {
  color:#000;
  text-decoration: none;
}
@media print {
    .hide-print {
      display: none;
    }
    .show-print {
      display: block;
    }
}

-->
</style>
<body>


<div class="print-topnav hide-print text-center allcaps">
  <a href="<?php echo site_url( uri_string() ) . "?output=print&center=all&show=" . $this->input->get('show') . "&cgs=" . $current_cgs; ?>">All Centers</a> &middot; 
  <a href="<?php echo site_url( uri_string() ) . "?show=" . $this->input->get('show') . "&cgs=" . $current_cgs; ?>">Back</a> 
</div>

<table width="100%" height="100%" cellpadding="0" cellspacing="0">
  <thead>

    <tr>
      <th colspan="4">
<table width="100%" cellpadding="3" cellspacing="0">

      <tr>
      <th class="allcaps">Archdiocese of Davao</th>
    </tr>

    <tr>
      <th><strong class="allcaps" style="font-size: medium; text-decoration: underline;">Archdiocesan Nourishment Center, Inc.</strong></th>
    </tr>
    <tr>
      <th style="font-weight: normal;"><small>Lot 2, Pag-asa St, Fatima, Brgy. 24-C, Davao City &middot; +63 (82) 285-1524<br>
https://www.ancdavao.com/ &middot; anc.archdavao@gmail.com</small>
      </th>
    </tr>

    <tr>
      <th>

        <br><strong style="font-size: small;font-family: sans; letter-spacing: 10px; text-transform: uppercase;">List of Recipients</strong><br><br>

      </th>
    </tr>

</table>

<table width="100%" cellpadding="3" cellspacing="0" class="bordered font85p" style="margin-top: 5px;">
 <tr>
    <th class="text-left" width="15%">Last name</th>
    <th class="text-left" width="15%">First name</th>
    <th class="text-left" width="10%">Gender</th>
    <th class="text-left" width="10%">Birthdate</th>
    <th class="text-center" width="10%">Age</th>
    <th class="text-right" width="10%">Height</th>
    <th class="text-right" width="10%">Weight</th>
    <th class="text-right" width="15%">BMI Status</th>
  </tr>
  <tr>
    <th colspan="8"></th>
  </tr>
</table>

      </th>
    </tr>

  </thead>
  <tbody style="vertical-align: top; height: 100%;" >
    <tr>
      <td colspan="4">
        
<table width="100%" cellpadding="3" cellspacing="0" class="bordered font85p" style="margin-top: 5px;">
<?php 
$n = 1;
foreach($benefeciaries as $benefeciary) { //print_r( $benefeciary ); 
  $age = current_age( $benefeciary->birthday );
  ?>
  <tr>
    <td class="text-left" width="15%"><?php echo $n; ?>. <?php echo $benefeciary->lastname; ?></td>
    <td class="text-left" width="15%"><?php echo $benefeciary->firstname; ?></td>
    <td class="text-left" width="10%"><?php echo strtoupper(substr($benefeciary->gender,0,1)); ?></td>
    <td class="text-left" width="10%"><?php echo date("m/d/Y", strtotime($benefeciary->birthday)); ?></td>
    <td class="text-center" width="10%"><?php echo $age->y; ?>Y<?php echo $age->m; ?>M<?php echo $age->m; ?>D </td>
    <td class="text-right" width="10%"><?php echo number_format($benefeciary->height,2); ?> cm</td>
    <td class="text-right" width="10%"><?php echo number_format($benefeciary->weight,2); ?> kg</td>
    <td class="text-right" width="15%"><?php echo bmiStatus(metricBMIFormula($benefeciary->height,$benefeciary->weight)); ?></td>
  </tr>
<?php $n++; } ?>
</table>


      </td>
    </tr>
  </tbody>
  <tfoot style="vertical-align: bottom;">

        <tr>
      <td style="padding-top: 10px;" width="35%" class="footer">Prepared by:</td>
      <td style="padding-top: 10px;" width="35%" class="footer text-right">Certified Correct By:</td>
    </tr>
    <tr>
      <td width="35%" class="footer"><br><br><strong class="allcaps">Chester Alan Tagudin</strong><br>Finance In-Charge</td>
      <td width="35%" class="footer text-right"><br><br><strong class="allcaps">Sr. Ma. Felina Inting, LGC</strong><br>Admin Head</td>
    </tr>

  </tfoot>
</table>


</body>
</html>