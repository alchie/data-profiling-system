<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('benefeciaries/benefeciaries_navbar'); ?>

<div class="container">
<div class="row">

  <div class="col-md-6 col-md-offset-3">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Edit Benefeciary Record</h3>
        </div>
        <form method="post">
        <div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>

<div class="row">
<?php if($groups) { ?>
  <div class="col-md-6">
          <div class="form-group">
            <label>Group</label>
            <select class="form-control" title="Select a Group" name="group_id">
              <?php if($groups) foreach($groups as $group) { ?>
                <option value="<?php echo $group->id; ?>" <?php echo ($benefeciary->group_id==$group->id) ? "SELECTED" : ""; ?>><?php echo $group->name; ?></option>
              <?php } ?>
            </select>
          </div>
  </div>
<?php } ?>
<?php if($areas) { ?>
  <div class="col-md-6">
          <div class="form-group">
            <label>Area</label>
            <select class="form-control" title="Select a Area" name="area_id">
              <?php if($areas) foreach($areas as $area) { ?>
                <option value="<?php echo $area->id; ?>" <?php echo ($benefeciary->area_id==$area->id) ? "SELECTED" : ""; ?>><?php echo $area->name; ?></option>
              <?php } ?>
            </select>
          </div>
  </div>
<?php } ?>
  <div class="col-md-6">
          <div class="form-group">
            <label>Date Accepted</label>
            <input name="date_accepted" type="text" class="form-control datepicker" value="<?php echo ($benefeciary->accepted) ? date('m/d/Y', strtotime($benefeciary->accepted)) : date('m/d/Y'); ?>">
          </div>
  </div>

  <div class="col-md-6">
<?php if( $benefeciary_status ) { ?>
          <div class="form-group">
            <label>Status</label>
            <select class="form-control" title="Select a Status" name="status">
              <?php foreach($benefeciary_status as $status) { ?>
                <option value="<?php echo $status->id; ?>" <?php echo ($benefeciary->status==$status->id) ? "SELECTED" : ""; ?>><?php echo $status->name; ?></option>
              <?php } ?>
            </select>
          </div>
<?php } ?>
          </div>
  <div class="col-md-6">
          <div class="form-group">
            <label>Benefeciary ID</label>
            <input name="benefeciary_id" type="text" class="form-control" value="<?php echo $benefeciary->benefeciary_id; ?>">
          </div>
  </div>

          
<div class="col-md-6">
          <div class="form-group">
            <?php foreach($services as $service) { ?>
            <label><input name="benefeciary_service[]" type="checkbox" value="<?php echo $service->id; ?>" <?php echo ($service->checked) ? 'CHECKED' : ''; ?>> <?php echo $service->name; ?></label><br>
          <?php } ?>
          </div>
</div>
</div>
<div class="form-group">
            <label>Notes</label>
            <textarea name="notes" class="form-control"><?php echo $benefeciary->notes; ?></textarea>
          </div>

          <div class="form-group">
            <label>Transfer <?php echo lang_term('companies_title_singular', 'Center'); ?></label>
            <select class="form-control" title="Select a Center" name="center_id">
              <?php if($centers) foreach($centers as $center) { ?>
                <option value="<?php echo $center->id; ?>" <?php echo ($benefeciary->center_id==$center->id) ? "SELECTED" : ""; ?>><?php echo $center->name; ?></option>
              <?php } ?>
            </select>
          </div>

<?php if( isset($output) && ($output!='ajax') ) : ?>

        </div>
        <div class="panel-footer">
          <button type="submit" class="btn btn-success">Submit</button>
          <a href="<?php echo site_url($current_uri); ?>" class="btn btn-warning">Back</a>
        </div>
        </form>
      </div>
    </div>
</div>
</div>
<?php $this->load->view('footer'); ?>
<?php endif; ?>