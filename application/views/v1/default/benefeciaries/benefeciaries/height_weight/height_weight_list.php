<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
<?php if( ! $inner_page ): ?>

<?php $this->load->view('lists/names/employed_navbar'); ?>

<div class="container">
    <div class="row">
            <div class="col-md-12">
              <div class="panel panel-default">
                <div class="panel-heading">

<div class="row">
<div class="col-md-9 col-sm-6">

                  <h3 class="panel-title">
                    <strong><?php echo $current_page; ?></strong>

                    <?php if( $this->input->get('q') ) { ?>
                    <span class="badge"><?php echo $this->input->get('q'); ?> <a href="<?php echo site_url(uri_string()); ?>"><span class="glyphicon glyphicon-remove"></span></a></span>
                    <?php } ?>

<br><small><em>(<?php echo $height_weights_count; ?> measurement<?php echo ($height_weights_count>1)?"s":""; ?> found)</em></small>

                  </h3>
</div>
<div class="col-md-3 col-sm-6">
<form method="get" action="<?php echo site_url( uri_string() ); ?>">
<div class="input-group input-group-sm">
  <input type="text" name="q" class="form-control" placeholder="Search for..." value="<?php echo $this->input->get('q'); ?>">
  <span class="input-group-btn">
    <button class="btn btn-default" type="submit">Search</button>
    
<?php if( hasAccess('benefeciaries', 'benefeciaries', 'add') ) { ?>
  <button type="button" class="btn btn-success ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Add Height & Weight" data-url="<?php echo site_url("benefeciaries/add_height_weight/{$name->id}/ajax") . "?next=" . (($this->input->get('next')) ? $this->input->get('next') : uri_string()); ?>" style="margin-right: 5px">Add Height & Weight</button>
<?php } ?>

  </span>
</div><!-- /input-group -->
</form>
</div>
</div>

                </div>
                <div class="panel-body" id="ajaxBodyInnerPage">
<?php endif; ?>
<?php if( $height_weights ) { ?>

          <table class="table table-default table-hover">
            <thead>
              <tr>
                <th>Date Taken</th>
                <th>Height (cm)</th>
                <th>Weight (kg)</th>
                <th>Age</th>
                <th>BMI</th>
                <th>Status</th>
                <?php if( hasAccess('benefeciaries', 'benefeciaries', 'edit') ) { ?>
                  <th width="10px" class="text-right">Action</th>
                <?php } ?>
              </tr>
            </thead>
            <tbody>

            <?php foreach($height_weights as $hw) { ?>
              <tr id="height_weight-<?php echo $hw->name_id; ?>">
                <td><?php echo date('F d, Y', strtotime($hw->date_hw)); ?></td>
                <td><?php echo $hw->height; ?></td>
                <td><?php echo $hw->weight; ?></td>
                <td><?php echo $hw->age; ?> years <?php echo $hw->age_m; ?> months <?php echo round($hw->age_d); ?> days</td>
                <td><?php echo metricBMIFormula( $hw->height, $hw->weight ); ?></td>
                <td><?php echo bmiStatus(metricBMIFormula( $hw->height, $hw->weight )); ?></td>
              <?php if( hasAccess('benefeciaries', 'benefeciaries', 'edit') ) { ?>
                <td class="text-right">
                        <a class="btn btn-warning btn-xs ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Edit Height and Weight" data-url="<?php echo payroll_url("benefeciaries/edit_height_weight/{$hw->id}/ajax") . "?next=" .uri_string(); ?>">Edit</a>
                </td>
              <?php } ?>
              </tr>
            <?php } ?>

            </tbody>
          </table>

          <?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>

<?php } else { ?>

  <div class="text-center">No Height & Weight Found!</div>

<?php } ?>
<?php if( ! $inner_page ): ?>

              </div>
              </div>
            </div>
    </div>
</div>
<?php endif; ?>
<?php $this->load->view('footer'); ?>