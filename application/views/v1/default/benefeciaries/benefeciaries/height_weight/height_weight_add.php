<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('lists/names/employed_navbar'); ?>

<div class="container">
<div class="row">

  <div class="col-md-6 col-md-offset-3">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Add Height & Weight</h3>
        </div>
        <form method="post">
        <div class="panel-body">

<?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>
<?php 
$custom_date = date('m/d/Y');
$custom_month = ($this->input->get('month')) ? $this->input->get('month') : date('m');
$custom_year = ($this->input->get('year')) ? $this->input->get('year') : date('Y');
if( $this->input->get('month') ) {
  $custom_date = date( 'm/d/Y', strtotime($custom_month."/1/".$custom_year) );
}
$current_date = ($this->input->post('date')) ? $this->input->post('date') : $custom_date;
?>
          <div class="form-group">
            <label>Date</label>
            <input name="date" type="text" class="form-control datepicker" value="<?php echo $current_date; ?>">
          </div>

          <div class="form-group">
            <label>Height (cm)</label>
            <input name="height" type="text" class="form-control" value="<?php echo $this->input->post('height'); ?>">
          </div>

          <div class="form-group">
            <label>Weight (kg)</label>
            <input name="weight" type="text" class="form-control" value="<?php echo $this->input->post('weight'); ?>">
          </div>

          <div class="form-group">
            <label>Notes</label>
            <textarea name="notes" class="form-control"><?php echo $this->input->post('notes'); ?></textarea>
          </div>

<?php if( isset($output) && ($output!='ajax') ) : ?>

        </div>
        <div class="panel-footer">
          <button type="submit" class="btn btn-success">Submit</button>
          <a href="<?php echo site_url($current_uri); ?>" class="btn btn-warning">Back</a>
        </div>
        </form>
      </div>
    </div>
</div>
</div>
<?php $this->load->view('footer'); ?>
<?php endif; ?>