<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
<?php if( ! $inner_page ): ?>

<?php $this->load->view('lists/names/employed_navbar'); ?>

<div class="container">
    <div class="row">
            <div class="col-md-12">
              <div class="panel panel-default">
                <div class="panel-heading">

<div class="row">
<div class="col-md-9 col-sm-6">

                  <h3 class="panel-title">
                    <strong><?php echo $current_page; ?></strong>

                    <?php if( $this->input->get('q') ) { ?>
                    <span class="badge"><?php echo $this->input->get('q'); ?> <a href="<?php echo site_url(uri_string()); ?>"><span class="glyphicon glyphicon-remove"></span></a></span>
                    <?php } ?>

<br><small><em>(<?php echo $activities_count; ?> activit<?php echo ($activities_count>1)?"ies":"y"; ?> found)</em></small>

                  </h3>
</div>
<div class="col-md-3 col-sm-6">
<form method="get" action="<?php echo site_url( uri_string() ); ?>">
<div class="input-group input-group-sm">
  <input type="text" name="q" class="form-control" placeholder="Search for..." value="<?php echo $this->input->get('q'); ?>">
  <span class="input-group-btn">
    <button class="btn btn-default" type="submit">Search</button>

  </span>
</div><!-- /input-group -->
</form>
</div>
</div>

                </div>
                <div class="panel-body" id="ajaxBodyInnerPage">
<?php endif; ?>
<?php if( $activities ) { ?>

          <table class="table table-default table-hover">
            <thead>
              <tr>
                <th>Date</th>
                <th>Activity Name</th>
                <th>Service</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>

            <?php foreach($activities as $activity) {  ?>
              <tr id="activity-<?php echo $activity->id; ?>">
                <td><?php echo date('F d, Y', strtotime($activity->activity_date)); ?></td>
                <td><?php echo $activity->name; ?></td>
                <td><?php echo $activity->service_name; ?></td>
                <td>
                <a class="btn btn-success btn-xs body_wrapper" href="<?php echo site_url("activities/attendance/{$activity->id}"); ?>">Attendace</a>
              </td>
              </tr>
            <?php } ?>

            </tbody>
          </table>

          <?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>

<?php } else { ?>

  <div class="text-center">No activity Found!</div>

<?php } ?>
<?php if( ! $inner_page ): ?>

              </div>
              </div>
            </div>
    </div>
</div>
<?php endif; ?>
<?php $this->load->view('footer'); ?>