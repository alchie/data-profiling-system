<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <div class="panel panel-default">
          <div class="panel-heading">Select Center</div>
          <div class="panel-body">
<?php endif; ?>
<?php if($centers) { ?>

<div class="list-group">
  <?php foreach($centers as $center) { ?>
  <a href="<?php echo site_url("welcome/select_company/{$center->id}"); ?>" class="list-group-item">
    <h4 class="list-group-item-heading"><?php echo $center->name; ?></h4>
    <p class="list-group-item-text"><?php echo $center->address; ?><br/><?php echo $center->phone; ?></p>
  </a>
  <?php } ?>
</div>
<?php } ?>

<?php if( isset($output) && ($output!='ajax') ) : ?>
  
          </div>
        </div>
    </div>
</div>

<?php $this->load->view('footer'); ?>

<?php endif; ?>