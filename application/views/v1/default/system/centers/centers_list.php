<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('header'); ?>
<?php if( ! $inner_page ): ?>

<?php $this->load->view('system/system_navbar'); ?>

<div class="container">
    <div class="row">
            <div class="col-md-12">
              <div class="panel panel-default">
                <div class="panel-heading">
<?php if( hasAccess('system', 'centers', 'add') ) { ?>
  <button type="button" class="btn btn-success btn-xs pull-right ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Add <?php echo lang_term('centers_title_singular', 'Center'); ?>" data-url="<?php echo site_url("system_centers/add/ajax") . "?next=" . uri_string(); ?>" style="margin-right: 5px">Add <?php echo lang_term('centers_title_singular', 'Center'); ?></button>
<?php } ?>
                  <h3 class="panel-title bold"><?php echo lang_term('centers_title_plural', 'Centers'); ?> <a href="<?php echo site_url("system_centers"); ?>?filter=trash"><span class="glyphicon glyphicon-trash"></span></a></h3>
                </div>
                <div class="panel-body" id="ajaxBodyInnerPage">
<?php endif; ?>
<?php if( $centers ) { ?>

          <table class="table table-default table-hover">
            <thead>
              <tr>
                <th><?php echo lang_term('centers_title_singular', 'Center'); ?> Name</th>
                <th>Address</th>
                <th>Phone</th>
                <?php if( hasAccess('system', 'centers', 'edit') ) { ?>
                  <th width="170px" class="text-right">Action</th>
                <?php } ?>
              </tr>
            </thead>
            <tbody>

            <?php foreach($centers as $company) { ?>
              <tr id="employee-group-<?php echo $company->id; ?>" class="<?php echo ($company->default==1) ? 'success' : ''; ?>">
                <td><?php echo $company->name; ?></td>
                <td><?php echo $company->address; ?></td>
                <td><?php echo $company->phone; ?></td>
              <?php if( hasAccess('system', 'centers', 'edit') ) { ?>
                <td class="text-right">
<?php if( $this->input->get('filter') == 'trash') { ?>

  <a class="btn btn-success btn-xs confirm" href="<?php echo site_url("system_centers/restore/{$company->id}"); ?>" data-target="#employee-group-<?php echo $company->id; ?>">Restore</a>

<?php } else { ?>
<div class="btn-group">
  <button type="button" class="btn btn-warning btn-xs ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Edit <?php echo lang_term('centers_title_singular', 'Center'); ?>" data-url="<?php echo site_url("system_centers/edit/{$company->id}/ajax") . "?next=" . uri_string(); ?>">Edit</button>
  <button type="button" class="btn btn-warning btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <span class="caret"></span>
    <span class="sr-only">Toggle Dropdown</span>
  </button>
  <ul class="dropdown-menu dropdown-menu-right">
    <li><a href="#" class="ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Payroll Columns" data-url="<?php echo site_url("system_centers/column_group/{$company->id}/ajax") . "?next=" . uri_string(); ?>">Payroll Columns</a></li>
    <li><a href="#" class="ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Print Group" data-url="<?php echo site_url("system_centers/print_group/{$company->id}/ajax") . "?next=" . uri_string(); ?>">Print Group</a></li>
    <li><a href="#" class="ajax-modal" data-toggle="modal" data-target="#ajaxModal" data-title="Print CSS" data-url="<?php echo site_url("system_centers/print_css/{$company->id}/ajax") . "?next=" . uri_string(); ?>">Print CSS</a></li>
  </ul>
</div>

                <a class="btn btn-danger btn-xs confirm_remove" href="<?php echo site_url("system_centers/deactivate/{$company->id}"); ?>" data-target="#employee-group-<?php echo $company->id; ?>">Deactivate</a>

                </td>

              <?php } ?>

              <?php } ?>
              </tr>
            <?php } ?>

            </tbody>
          </table>

          <?php echo ($pagination!='') ? '<center>' . $pagination . '</center>' : ''; ?>

<?php } else { ?>

  <div class="text-center">No Centers Found!</div>

<?php } ?>
<?php if( ! $inner_page ): ?>

              </div>
              </div>
            </div>
    </div>
</div>
<?php endif; ?>
<?php $this->load->view('footer'); ?>