<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 
/* draws a calendar */
function draw_calendar($month,$year, $activities, $birthdays){

  $aDates = array();
  if( $activities ) {
    foreach($activities as $activity) {
      $aDates[$activity->activity_date] = $activity;
    }
  }

  $bDates = array();
  if( $birthdays ) {
    foreach($birthdays as $birthday) {
      $bDates[date('Y-m-d', strtotime( $year."-".$month."-".$birthday->birth_day ))] = $birthday->bdays;
    }
  }

  /* draw table */
  $calendar = '<table cellpadding="0" cellspacing="0" class="calendar">';

  /* table headings */
  $headings = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
  $calendar.= '<tr class="calendar-row"><td class="calendar-day-head">'.implode('</td><td class="calendar-day-head">',$headings).'</td></tr>';

  /* days and weeks vars now ... */
  $running_day = date('w',mktime(0,0,0,$month,1,$year));
  $days_in_month = date('t',mktime(0,0,0,$month,1,$year));
  $days_in_this_week = 1;
  $day_counter = 0;
  $dates_array = array();

  /* row for week one */
  $calendar.= '<tr class="calendar-row">';

  /* print "blank" days until the first of the current week */
  for($x = 0; $x < $running_day; $x++):
    $calendar.= '<td class="calendar-day-np"> </td>';
    $days_in_this_week++;
  endfor;

  /* keep going with days.... */
  for($list_day = 1; $list_day <= $days_in_month; $list_day++):

    $list_date = date('Y-m-d', strtotime($year.'-'.$month.'-'.$list_day));

      $working_day = true;
      switch ($running_day) {
        case '0':
          $working_day = WORK_ON_SUN;
          break;
        case '1':
          $working_day = WORK_ON_MON;
          break;
        case '2':
          $working_day = WORK_ON_TUE;
          break;
        case '3':
          $working_day = WORK_ON_WED;
          break;
        case '4':
          $working_day = WORK_ON_THU;
          break;
        case '5':
          $working_day = WORK_ON_FRI;
          break;
        case '6':
          $working_day = WORK_ON_SAT;
          break;
      }

          $calendar.= '<td class="calendar-day text-right ';
          $calendar.= (($working_day)?'':' disabled');
          $calendar.= ((isset($aDates[$list_date]) || isset($bDates[$list_date]))? ' has-data':'');

          $calendar.= (($list_date==date('Y-m-d'))? ' current-day':'');

          $calendar.='">';
          

        $calendar.= '<a class="date_link body_wrapper" data-title="'.date('F d, Y', strtotime($list_date)).'" href="'.site_url("activities/index/{$list_date}") . "?next=" . uri_string() .'">';
        //$calendar.= '<input type="hidden" name="inclusive_date[]" value="'.$list_date.'">';
        //$calendar.= '<input type="checkbox" name="selected[]" value="'.$list_date.'"';
        //$calendar.= '>';
        //$calendar.=((isset($aDates[$list_date])) ? $aDates[$list_date]->notes . " (+".$aDates[$list_date]->premium."%)" :'');

        $calendar.= ((isset($bDates[$list_date])) ? '<i class="fa fa-birthday-cake" style="font-size:30px; padding-top:5px;"></i>': '' );
        $calendar.= ((isset($aDates[$list_date])) ? '<i class="fa fa-cutlery" style="font-size:30px; padding-top:5px;padding-left:5px;"></i>': '' );

        //$calendar.= ((isset($bDates[$list_date])) ? '<i class="fa fa-gittip" style="font-size:30px; padding-top:5px;padding-left:5px;"></i>': '' );

        $calendar.= '</a>';


      /* add in the day number */
      $calendar.= '<div class="day-number">';
       $calendar.= $list_day;
      $calendar.='</div>';

      /** QUERY THE DATABASE FOR AN ENTRY FOR THIS DAY !!  IF MATCHES FOUND, PRINT THEM !! **/
      $calendar.= str_repeat('<p> </p>',2);
        

    $calendar.= '</td>';
    if($running_day == 6):
      $calendar.= '</tr>';
      if(($day_counter+1) != $days_in_month):
        $calendar.= '<tr class="calendar-row">';
      endif;
      $running_day = -1;
      $days_in_this_week = 0;
    endif;
    $days_in_this_week++; $running_day++; $day_counter++;
  endfor;

  /* finish the rest of the days in the week */
  if($days_in_this_week < 8):
    for($x = 1; $x <= (8 - $days_in_this_week); $x++):
      $calendar.= '<td class="calendar-day-np"> </td>';
    endfor;
  endif;

  /* final row */
  $calendar.= '</tr>';

  /* end the table */
  $calendar.= '</table>';
  
  /* all done, return result */
  return $calendar;
}

?>

<?php if( isset($output) && ($output!='ajax') ) : ?>

<?php $this->load->view('header'); ?>

<?php $this->load->view('calendar/calendar_navbar'); ?>

<div class="container">
<div class="row">
<?php 
$next = ($this->input->get('next')) ? $this->input->get('next') : 'calendar';
?>
  <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          <a href="<?php echo site_url("calendar/index/".date('m', strtotime('+1 month', strtotime($current_year.'-'.$current_month.'-01')))."/".date('Y', strtotime('+1 month', strtotime($current_year.'-'.$current_month.'-01')))."/{$output}") . "?next={$next}"; ?>" class="pull-right body_wrapper"><?php echo date('F Y', strtotime('+1 month', strtotime($current_year.'-'.$current_month.'-01'))); ?> &rArr;</a>

          <a href="<?php echo site_url("calendar/index/".date('m', strtotime('-1 month', strtotime($current_year.'-'.$current_month.'-01')))."/".date('Y', strtotime('-1 month', strtotime($current_year.'-'.$current_month.'-01')))."/{$output}") . "?next={$next}"; ?>" class="pull-left body_wrapper">&lArr; <?php echo date('F Y', strtotime('-1 month', strtotime($current_year.'-'.$current_month.'-01'))); ?></a>

<h3 class="text-center panel-title" style="margin-top:0px;">
  
  <div class="btn-group">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <strong><?php echo date('F Y', strtotime($current_month."/1/".$current_year)); ?></strong> <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
<?php for($i=1;$i <= 12; $i++ ) { ?>
    <li class="<?php echo ($i==$current_month) ? "active" : ""; ?>"><a class="body_wrapper" href="<?php echo site_url("calendar/index/{$i}/{$current_year}"); ?>"><?php echo date('F Y', strtotime($i."/1/".$current_year)); ?></a></li>
<?php } ?>
  </ul>
</div>

</h3>

        </div>

        <div class="panel-body">
  <?php echo (validation_errors()) ? '<div class="alert alert-danger">' . validation_errors() . '</div>' : ''; ?>

<?php endif; ?>

<?php 
echo draw_calendar($current_month,$current_year,$activities, $birthdays);
?>


<?php if( isset($output) && ($output!='ajax') ) : ?>
        </div>

      </div>
    </div>
</div>
</div>

<?php $this->load->view('footer'); ?>
<?php endif; ?>