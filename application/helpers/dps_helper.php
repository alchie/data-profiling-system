<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if( ! function_exists('centimetersToInches') ) {
	function centimetersToInches($centimeters) {
        $inches = $centimeters / 2.54;
        return round( $inches, 5 );
	}
}

if( ! function_exists('centimetersToMeters') ) {
    function centimetersToMeters($centimeters){
        return $centimeters * 0.01;
    }
}

if( ! function_exists('metricBMIFormula') ) {
    function metricBMIFormula($height, $weight){
        if( $height && $weight) {
            $height = centimetersToMeters($height);
            $bmi = ( $weight / ( $height * $height ) ); 
            return round( $bmi, 5 );
        } else {
            return 0;
        }
    }
}

if( ! function_exists('bmiStatus') ) {
    function bmiStatus($bmi){
        switch($bmi) {
            case (($bmi > 0) && ($bmi < 18.5)):
                return 'Underweight';
            break;
            case (($bmi > 18.5) && ($bmi < 24.99999)):
                return 'Normal weight';
            break;
            case (($bmi > 25) && ($bmi < 29.99999)):
                return 'Overweight';
            break;
            case (($bmi > 30) && ($bmi < 34.99999)):
                return 'Obesity Class 1';
            break;
            case (($bmi > 35) && ($bmi < 39.99999)):
                return 'Obesity Class 2';
            break;
            case ($bmi <= 40):
                return 'Extreme Obesity Class 3';
            break;
        }
    }
}

if( ! function_exists('current_age') ) {
    function current_age($birthdate){
        $age = date_diff(date_create($birthdate), date_create('now'));
        return $age;
    }
}

if( ! function_exists('weight_for_age') ) {
    function weight_for_age($weight, $birthdate) {
        $now = time(); // or your date as well
        $your_date = strtotime( $birthdate );
        $datediff = $now - $your_date;
        $age_in_days = round($datediff / (60 * 60 * 24));
        $age_in_months = round(($age_in_days / 30.41666666666667),2);

        $weight_for_age = $weight * $age_in_months; 

        return ($weight_for_age * 100);
    }
}